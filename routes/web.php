<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['as'=>'index','uses'=>'UmumController@index']);
Route::get('/harga-{name}', ['as'=>'daftar-harga','uses'=>'UmumController@harga_pulsa']);
Route::post('/masuk', ['as'=>'masuk','uses'=>'UmumController@masuk']);
Route::post('/daftar', ['as'=>'daftar','uses'=>'UmumController@daftar']);
Route::post('/kirim-kode', ['as'=>'kirim-kode','uses'=>'UmumController@kirim_kode','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/kirim-ulang-kode', ['as'=>'kirim-ulang-kode','uses'=>'UmumController@kirim_ulang_kode','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/ganti-email', ['as'=>'ganti-email','uses'=>'UmumController@ganti_email','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/kontak', ['as'=>'kontak','uses'=>'UmumController@kontak']);
Route::get('/channels', ['as'=>'channels','uses'=>'UmumController@channels']);

Route::get('/share/{kode}', ['as'=>'share','uses'=>'UmumController@share']);
Route::get('/download/aplikasi', ['as'=>'download-aplikasi','uses'=>'UmumController@download','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/edit-password', ['as'=>'edit-password','uses'=>'UmumController@ganti_password','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/edit-password', ['as'=>'edit-password','uses'=>'UmumController@ganti_password','middleware' => ['auth' ,'checksinglesession']]);

Route::get('/keluar', ['as'=>'keluar','uses'=>'UmumController@keluar','middleware' => ['auth' ,'checksinglesession']]);;

Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/user', ['as'=>'user','uses'=>'UserController@user','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin', ['as'=>'admin','uses'=>'AdminController@admin','middleware' => ['auth' ,'checksinglesession']]);

//ADMIN HARGA PPOB
Route::get('/admin/data-harga-{id}', ['as'=>'admin-data-harga','uses'=>'AdminController@data_harga','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/admin/data-harga-{id}', ['as'=>'admin-data-harga','uses'=>'AdminController@data_harga','middleware' => ['auth' ,'checksinglesession']]);

//Belanja

//Transaksi pulsa
Route::get('/transaksi-pulsa', ['as'=>'transaksi-pulsa','uses'=>'UserController@transaksi_pulsa','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/provider', ['as'=>'provider','uses'=>'UserController@provider','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/operator', ['as'=>'operator','uses'=>'UserController@operator','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses', ['as'=>'proses','uses'=>'UserController@proses','middleware' => ['auth' ,'checksinglesession']]);
//Transaksi Token
Route::get('/transaksi-pln', ['as'=>'transaksi-pln','uses'=>'UserController@transaksi_pln','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/provider-pln', ['as'=>'provider-pln','uses'=>'UserController@provider_pln','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses-pln', ['as'=>'proses-pln','uses'=>'UserController@proses_pln','middleware' => ['auth' ,'checksinglesession']]);
//Transaksi Paket Data
Route::get('/transaksi-paket-data', ['as'=>'transaksi-paket-data','uses'=>'UserController@transaksi_paket_data','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/provider-paket-data', ['as'=>'provider-paket-data','uses'=>'UserController@provider_paket_data','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/operator-paket-data', ['as'=>'operator-paket-data','uses'=>'UserController@operator_paket_data','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses-paket-data', ['as'=>'proses-paket-data','uses'=>'UserController@proses_paket_data','middleware' => ['auth' ,'checksinglesession']]);
//Transaksi Voucher Game
Route::get('/transaksi-voucher-game', ['as'=>'transaksi-voucher-game','uses'=>'UserController@transaksi_voucher_game','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/provider-game', ['as'=>'provider-game','uses'=>'UserController@provider_game','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses-game', ['as'=>'proses-game','uses'=>'UserController@proses_game','middleware' => ['auth' ,'checksinglesession']]);
//Transaksi E-Wallet
Route::get('/transaksi-e-wallet', ['as'=>'transaksi-e-money','uses'=>'UserController@transaksi_e_money','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/provider-e-wallet', ['as'=>'provider-e-money','uses'=>'UserController@provider_e_money','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses-e-wallet', ['as'=>'proses-e-money','uses'=>'UserController@proses_e_money','middleware' => ['auth' ,'checksinglesession']]);
//Transaksi SMS
Route::get('/transaksi-sms', ['as'=>'transaksi-sms','uses'=>'UserController@transaksi_sms','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/provider-sms', ['as'=>'provider-sms','uses'=>'UserController@provider_sms','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses-sms', ['as'=>'proses-sms','uses'=>'UserController@proses_sms','middleware' => ['auth' ,'checksinglesession']]);
//Transaksi TELPON
Route::get('/transaksi-telpon', ['as'=>'transaksi-telpon','uses'=>'UserController@transaksi_telpon','middleware' => ['auth' ,'checksinglesession']]);
// Route::post('/provider-telpon', ['as'=>'provider-telpon','uses'=>'UserController@provider_telpon','middleware' => ['auth' ,'checksinglesession']]);
// Route::post('/proses-telpon', ['as'=>'proses-telpon','uses'=>'UserController@proses_telpon','middleware' => ['auth' ,'checksinglesession']]);

//Deposit
Route::get('/tambah-saldo', ['as'=>'tambah-saldo','uses'=>'UserController@tambah_saldo','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/mutasi-saldo', ['as'=>'mutasi-saldo','uses'=>'UserController@mutasi_saldo','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/data-topup', ['as'=>'data-topup','uses'=>'UserController@data_topup','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/sudah-transfer/{id}', ['as'=>'sudah-transfer','uses'=>'UserController@sudah_transfer','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/batalkan-deposit/{id}', ['as'=>'batalkan-deposit','uses'=>'UserController@batalkan_deposit','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/bank', ['as'=>'bank','uses'=>'UserController@bank','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/proses-deposit', ['as'=>'proses-deposit','uses'=>'UserController@proses_deposit','middleware' => ['auth' ,'checksinglesession']]);

Route::get('/laporan-pulsa', ['as'=>'laporan-pulsa','uses'=>'UserController@laporan_pulsa','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/laporan-voucher-pln', ['as'=>'laporan-voucher-pln','uses'=>'UserController@laporan_voucher_pln','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/laporan-paket-data', ['as'=>'laporan-paket-data','uses'=>'UserController@laporan_paket_data','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/laporan-paket-sms', ['as'=>'laporan-paket-sms','uses'=>'UserController@laporan_paket_sms','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/laporan-paket-telpon', ['as'=>'laporan-paket-telpon','uses'=>'UserController@laporan_paket_telpon','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/laporan-game', ['as'=>'laporan-game','uses'=>'UserController@laporan_game','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/laporan-e-wallet', ['as'=>'laporan-e-money','uses'=>'UserController@laporan_e_money','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/data-agen-kamu', ['as'=>'tambah-untung','uses'=>'UserController@tambah_untung','middleware' => ['auth' ,'checksinglesession']]);;
Route::post('/data-agen-kamu', ['as'=>'tambah-untung','uses'=>'UserController@tambah_untung','middleware' => ['auth' ,'checksinglesession']]);;

//ROUTE ADMIN
Route::get('/admin/permintaan-topup', ['as'=>'admin-permintaan-topup','uses'=>'AdminController@permintaan_topup','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/admin/proses-topup', ['as'=>'admin-proses-topup','uses'=>'AdminController@proses_topup','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/admin/cancel-topup', ['as'=>'admin-cancel-topup','uses'=>'AdminController@cancel_topup','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin/mutasi-saldo', ['as'=>'admin-mutasi-saldo','uses'=>'AdminController@mutasi_saldo','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin/topup-manual', ['as'=>'admin-topup-manual','uses'=>'AdminController@topup_manual','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/admin/topup-manual', ['as'=>'admin-topup-manual','uses'=>'AdminController@proses_topup_manual','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin-bank', ['as'=>'admin-bank','uses'=>'AdminController@bank','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin/laporan-ppob', ['as'=>'admin-laporan-ppob','uses'=>'AdminController@laporan_ppob','middleware' => ['auth' ,'checksinglesession']]);
//ADMIN SUPPLIER
Route::get('/admin/mutasi-saldo-supplier', ['as'=>'admin-mutasi-saldo-supplier','uses'=>'AdminController@mutasi_saldo_supplier','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin/data-topup-supplier', ['as'=>'admin-data-topup-supplier','uses'=>'AdminController@data_topup_supplier','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin/topup-supplier', ['as'=>'admin-topup-supplier','uses'=>'AdminController@topup_supplier','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/admin/topup-supplier', ['as'=>'admin-proses-topup-supplier','uses'=>'AdminController@topup_supplier','middleware' => ['auth' ,'checksinglesession']]);
Route::get('/admin/bank-supplier', ['as'=>'admin-bank-supplier','uses'=>'AdminController@bank_supplier','middleware' => ['auth' ,'checksinglesession']]);

Route::get('/admin-data-user', ['as'=>'admin-data-user','uses'=>'AdminController@data_user','middleware' => ['auth' ,'checksinglesession']]);
Route::post('/admin-data-user', ['as'=>'admin-data-user','uses'=>'AdminController@data_user','middleware' => ['auth' ,'checksinglesession']]);


Route::get('/update-sql-ppob', ['as'=>'update-sql-ppob','uses'=>'CallbackController@update_sql_ppob','middleware' => ['auth' ,'checksinglesession']]);



//TES
Route::get('/produk/{id}', ['as'=>'produk','uses'=>'UserController@produk']);
Route::get('/detail-produk/{id}', ['as'=>'detail-produk','uses'=>'UserController@detail_produk']);

Route::get('/toko/{id}', ['as'=>'toko','uses'=>'UserController@toko']);


Route::get('/toko-saya', ['as'=>'toko-saya','uses'=>'UserController@toko_saya']);
Route::get('/form-toko', ['as'=>'form-toko','uses'=>'UserController@form_toko']);
Route::post('/form-toko', ['as'=>'simpan-toko','uses'=>'UserController@form_toko']);
Route::get('/propinsi', ['as'=>'propinsi','uses'=>'UserController@propinsi']);
Route::post('/kota', ['as'=>'kota','uses'=>'UserController@kota']);
Route::post('/check-ongkir', ['as'=>'check-ongkir','uses'=>'UserController@cek_ongkir']);


Route::get('/toko-saya/{id}', ['as'=>'detail-toko-saya','uses'=>'UserController@detail_saya']);
Route::get('/detail-produk-saya/{id}', ['as'=>'detail-produk-saya','uses'=>'UserController@detail_produk_saya']);

Route::get('/form-produk/{id}', ['as'=>'form-produk','uses'=>'UserController@form_produk']);
Route::post('/form-produk', ['as'=>'simpan-produk','uses'=>'UserController@simpan_produk']);
Route::get('/kategori', ['as'=>'kategori','uses'=>'UmumController@kategori']);


Route::get('/all-propinsi', ['as'=>'all-propinsi','uses'=>'OngkirController@all_propinsi']);
Route::get('/propinsi/{propinsi_id}', ['as'=>'pilih-propinsi','uses'=>'OngkirController@kota']);
Route::get('/all-city', ['as'=>'all-city','uses'=>'OngkirController@city']);
Route::get('/cek-ongkir', ['as'=>'cek-ongkir','uses'=>'OngkirController@cek_ongkir']);

// Route::get('/kota/{propinsi_id}', ['as'=>'kota','uses'=>'OngkirController@kota']);

Route::get('/harga_downline/{id}', ['as'=>'harga_downline','uses'=>'UserController@harga_downline']);
Route::post('/harga_downline/{id}', ['as'=>'harga_downline','uses'=>'UserController@harga_downline']);
Route::get('/jadi-agen', ['as'=>'jadi-agen','uses'=>'UserController@jadi_agen']);
Route::get('/proses_jadi_agen', ['as'=>'proses_jadi_agen','uses'=>'UserController@proses_jadi_agen']);
