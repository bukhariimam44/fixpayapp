@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Topup Manual</h3>   
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12">	
						<div class="w3agile properties">
								<div class="properties-bottom">
										<div class="w3ls-text" id="topup_manual">
													
													<div class="form-group">
													<h6  style="color:#11909e;">Bank</h6>
													<select v-model="selected" class="form-control">
																<option v-for="bank in banks" v-bind:value="bank.id">
																				@{{ bank.nama }} - @{{ bank.no_rek }}, An. @{{ bank.atas_nama }}
																</option>
														</select>
													</div>
													<div class="form-group">
													<h6  style="color:#11909e;">Email</h6>
													<input type="text" v-model="email"  required="" class="form-control"/>
													</div>
													<div class="form-group">
													<h6 style="color:#11909e;">Nominal</h6>
													<money v-model="nominal" v-bind="money" class="form-control"></money>
													<!-- <input type="text" v-model="nominal"  required="" class="form-control"/> -->
													</div>
													<div class="form-group">
													<button class="btn btn-success" @click="prosesTopupManual()">Proses</button>
													</div>
										</div> 
								</div> 
						</div> 
					</div> 
				</div>
@endsection
@section('js')
<script src="https://rawgit.com/vuejs-tips/v-money/master/dist/v-money.js"></script>
<script>
var topup_manual = new Vue({
	el:'#topup_manual',
	data:{
		selected: '',
		email : '',
		nominal : '',
		money: {
				decimal: ',',
				thousands: '.',
				precision: 0,
				masked: false
		},
		banks: [],
		reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
		error : []
	},
	mounted() {
					this.load()
	},
	methods: {
		async load(){
						await axios.get(window.url_bank).then((response) =>{
										console.log('Berhasil'+JSON.stringify(response.data));
										this.banks = response.data.bank;
										this.selected = response.data.bank[0].id;
						},(response)=>{
										console.log('ERROR: '+response);
						});
		},
		async prosesTopupManual(){
				//Loading
				Swal.fire({
									title: 'Mohon menunggu...',
									text:'Topup Sedang Diproses..',
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
				//END LOADING
				
					if (this.email && this.nominal && this.selected) {
							console.log('lolos');
					
							let request = {bank_id : this.selected, email : this.email, nominal : this.nominal};
							request['token'] = document.querySelector('#token').getAttribute('value');
							await axios.post(window.url_topup_manual,request).then((response) =>{
											console.log('Berhasil proses'+JSON.stringify(response.data));
											if (response.data.code === 200) {
													swal({
															title: response.data.title,
															text: response.data.message,
															icon: "success",
															dangerMode: true,
													})
													.then((willDelete) => {
															if (willDelete) {
																	// swal("Terimakasih, Silahkan", {
																	// 		icon: "success",
																	// });
																	window.location.href = response.data.url;
															} else {
																	window.location.href = response.data.url;
															}
													});
											}else{
													swal("Transaksi Gagal!", response.data.message, "error");
													Swal.close()
											}
											
							},(response)=>{
											console.log('ERROR: '+response);
											swal("Gagal!", response, "error");
											Swal.close()
							});
							
					}					
					console.log('panjang email : '+this.email.length);
					this.errors = [];
					if (this.email.length < 7) {
							swal("Cek Kembali!", "Email minimal 7 karakter", "error");
							Swal.close()
					}else if(!this.reg.test(this.email)){
							swal("Cek Kembali!", "Email tidak falid", "error");
							Swal.close()
					}else if (this.nominal  < 10000) {
							swal("Cek Kembali!", "Nominal Minimal Rp 10.000 ", "error");
							Swal.close()
					}
		}
	},
});
</script>
@endsection