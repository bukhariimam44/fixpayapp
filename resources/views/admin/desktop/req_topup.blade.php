@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Req Topup</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">	
					<div class="w3agile properties">
					@include('flash::message')
					@if(count($datas) < 1) <p style="text-align:center;">Kosong</p> @endif
					<div>
								<div class="w3ls-text">
					
					<div class="table-responsive">
						<table class="table">
							<thead>
							<th>No.</th><th>No.Trx</th><th>Email</th><th>Bank</th><th>Transfer</th><th>Status</th><th>Aksi</th>
							</thead>
							<tbody>
							@foreach($datas as $key => $dta)
							<tr>
							<td>{{$key+1}}.</td><td>{{$dta->no_trx}} <br> {{date('d M Y H:i', strtotime($dta->created_at))}} </td><td>{{$dta->userId->email}}</td><td>{{$dta->bankId->nama}} <br> {{$dta->bankId->no_rek}} </td><td>Rp {{number_format($dta->transfer,0,',','.')}}</td><td>{{$dta->status}}</td><td> @if($dta->status == 'menunggu')
							<form action="{{route('admin-proses-topup')}}" method="post" id="proses{{$dta->id}}">
									@csrf
									<input type="hidden" name="ids" value="{{$dta->id}}">
									</form>
									<form action="{{route('admin-cancel-topup')}}" method="post" id="cancel{{$dta->id}}">
									@csrf
									<input type="hidden" name="ids" value="{{$dta->id}}">
									</form>
							<button class="btn btn-success" id="btnProses{{$dta->id}}" onclick="myFunctionProses{{$dta->id}}()">Proses</button> <button class="btn btn-danger" id="btnCancel{{$dta->id}}" onclick="myFunctionCancel{{$dta->id}}()">Batal</button> @endif</td>
							</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<!-- properties -->
								@foreach($datas as $key => $dt)
						<script type="text/javascript">
						function myFunctionProses{{$dt->id}}() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Topup Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("btnProses{{$dt->id}}").style.display="disabled";
							document.getElementById('proses{{$dt->id}}').submit();
						}
						function myFunctionCancel{{$dt->id}}() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Cancel Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("btnCancel{{$dt->id}}").style.display="disabled";
							document.getElementById('cancel{{$dt->id}}').submit();
						}
					</script>
					@endforeach
					</div>
								
								</div>
													
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
<script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection