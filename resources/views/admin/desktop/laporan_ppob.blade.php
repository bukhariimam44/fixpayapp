@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Laporan PPOB</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					
					<div class="w3agile properties">
					
					<!-- properties -->
					<div>
					
					
								<div class="w3ls-text">
								<div class="table-responsive">
								<table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Tgl & No.Trx</th>
                              <th>Description</th> 
                              <th>Nomor</th>
                              <th>Harga</th>
                              <th>Untung Agen <br>Untung Admin</th>
																														<th>Status</th>
                            </tr> 
                        </thead> 
                        <tbody> 
         
                            @foreach($datas as $key => $data)
                            <tr> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td>{{date('d M Y H:i', strtotime($data->created_at))}} <br>{{$data->no_trx}}</td> 
                                <td>{{$data->productId->description}}</td> 
                                <td>
																																@if($data->idcust == null || $data->idcust == '')
																																<p style="color:#11909e;"><b>{{$data->nomor}}</b>  </p>
																																@else
																																<p style="color:#11909e;"><b>HP : {{$data->nomor}}</b>  </p>
																																<p style="color:#11909e;"><b>ID : {{$data->idcust}}</b>  </p>
																																@endif
																																@if($data->status_ppob_id == 4)
																																<p><b>@if($data->productId->provider == 'PLN') Token @else SN @endif :</b> {{$data->sn_token}} </p>
																																@endif
																																</td> 
                                <td>Rp {{number_format($data->harga)}}</td> 
                                <td>Rp {{number_format($data->untung_agen)}} <br>Rp {{number_format($data->untung)}}</td> 
                                <td>
																																<button class="btn @if($data->status_ppob_id == 1) btn-warning @elseif($data->status_ppob_id == 4) btn-success @else btn-danger @endif">{{$data->statusPpob->name}}</button>
																																</td> 
                            </tr> 
                            @endforeach
                        </tbody> 
                    </table>
					</div>
							

								</div>
								
							</div>
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection