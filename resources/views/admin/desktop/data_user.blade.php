@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
				<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Data User </h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row" style="margin-top:40px;">
					<div class="col-md-12">
								<div class="table-responsive">
									<table class="table">
										<thead>
										<tr>
											<th>No</th>	<th>Nama</th><th>Username</th><th>Email</th><th>Saldo</th><th>Agen</th>
										</tr>
										</thead>
										<tbody>
										<?php $tot = 0;?>
											@foreach($datas as $key => $dt)
											<?php $tot += $dt->saldo;?>
											<tr>
												<td width="20px">{{$key+1}}.</td>
												<td>@if($dt->email_verified_at == null)<span style="color:red;">{{$dt->name}} </span> @else {{$dt->name}} @endif</td>
												<td>@if($dt->email_verified_at == null)<span style="color:red;"{{$dt->username}}</span> @else {{$dt->username}} @endif</td>
												<td>@if($dt->email_verified_at == null)<span style="color:red;">{{$dt->email}} </span> @else {{$dt->email}} @endif</td>
												<td>@if($dt->email_verified_at == null)<span style="color:red;">Rp {{number_format($dt->saldo,0,',','.')}} </span> @else Rp {{number_format($dt->saldo,0,',','.')}} @endif</td>
												<td><button class="btn @if($dt->agen == 'yes')btn-success @else btn-danger @endif">{{strtoupper($dt->agen)}}</button> @if($dt->agen == 'no' && $dt->email_verified_at != null) <button class="btn btn-primary" onclick="event.preventDefault();
												//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
                document.getElementById('daftaragen{{$dt->id}}').submit();">Daftarkan Agen</button> @endif</td>
											</tr>
											<form action="{{route('admin-data-user')}}" method="post" id="daftaragen{{$dt->id}}">
											@csrf
											<input type="hidden" name="ids" value="{{encrypt($dt->email)}}">
											<input type="hidden" name="action" value="update">
											</form>
											@endforeach
											<tr>
											<th colspan="4">Total</th><th>Rp {{number_format($tot,0,',','.')}}</th><th></th>
											</tr>
										</tbody>
										

									</table>
								</div>
					</div>
				</div>
@endsection
@section('js')
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
    $('#flash-overlay-modal').modal();
</script>
@endsection