@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Mutasi Saldo</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					
					<div class="w3agile properties">
					
					<!-- properties -->
					<div>
					<form action="{{route('admin-mutasi-saldo')}}" method="get">
					@csrf
					<div class="row">
					<div class="form-group col-md-3">
					<input type="text" name="email" placeholder="Email" class="form-control">
					<input type="hidden" name="action" value="cari">
					</div>
					<div class="form-group col-md-3">
					<button class="btn btn-primary form-control">TAMPILKAN</button>
					</div>
					</div>
					</form>
					
								<div class="w3ls-text">
								<div class="table-responsive">
						<table class="table">
							<thead>
							<th>No.</th><th>No.Trx</th><th>Debet</th><th>Kredit</th><th>Saldo</th><th>Keterangan</th>
							</thead>
							<tbody>
							@foreach($datas as $key => $dta)
							<tr>
							<td>{{$key+1}}.</td>
							<td>{{$dta->no_trx}} <br> {{date('d M Y H:i', strtotime($dta->created_at))}} </td>
							@if($dta->mutasi == 'Debet')
							<td>Rp {{number_format($dta->nominal,'0',',','.')}}</td>
							<td>0 </td>
							@else
							<td>0 </td>
							<td>Rp {{number_format($dta->nominal,'0',',','.')}}</td>
							@endif
							<td>Rp {{number_format($dta->saldo_akhir,'0',',','.')}}</td>
							<td>{{$dta->keterangan}}</td>
							</tr>
							@endforeach
							@if(count($datas) < 1) 
							<tr>
							<td colspan="6"><p style="text-align:center;">Kosong</p> </td>
							</tr>
							@endif
							</tbody>
						</table>
					</div>
							

								</div>
								
							</div>
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
@endsection