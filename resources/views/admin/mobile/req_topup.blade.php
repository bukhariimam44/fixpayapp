@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Req Topup</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">	
					<div class="w3agile properties">
					@include('flash::message')
					@if(count($datas) < 1) <p style="text-align:center;">Kosong</p> @endif
					@foreach($datas as $key => $dt)
					<!-- properties -->
					<div class="properties-bottom">
								<div class="w3ls-text">
									<h5><a>{{$dt->bankId->nama}} ({{$dt->bankId->no_rek}}) </a></h5>
									<h6 style="color:#11909e;">Atas Nama : {{$dt->bankId->atas_nama}}</h6>
									<br>
									<h5><a>Transfer Rp {{number_format($dt->transfer,'0',',','.')}}</a></h5>
									<br>
									<h6 style="font-size:14;color:black;">Email : <a style="font-size:14;color:red;">{{$dt->userId->email}}</a></h6>
									<h6 style="font-size:14;color:black;">No Transaksi : {{$dt->no_trx}}</h6>
									<h6 style="font-size:14;color:black;">Tgl Transaksi : {{date('d M Y H:i', strtotime($dt->created_at))}}</h6>
									<h6 style="font-size:14;color:black;">Status : <a style="font-size:14;color: @if($dt->status_deposit_id == 1 || $dt->status_deposit_id == 4) #eea236 @elseif($dt->status_deposit_id ==2) #11909e @else red @endif;"> <strong> {{strtoupper($dt->statusDepositId->status)}}</strong></a></h6>
									<br>
									@if($dt->status_deposit_id == 4)
									<form action="{{route('admin-proses-topup')}}" method="post" id="proses{{$dt->id}}">
									@csrf
									<input type="hidden" name="ids" value="{{$dt->id}}">
									</form>
									<form action="{{route('admin-cancel-topup')}}" method="post" id="cancel{{$dt->id}}">
									@csrf
									<input type="hidden" name="ids" value="{{$dt->id}}">
									</form>
									<button class="btn btn-success" id="btnProses{{$dt->id}}" onclick="myFunctionProses{{$dt->id}}()">Proses</button> <button class="btn btn-danger" id="btnCancel{{$dt->id}}" onclick="myFunctionCancel{{$dt->id}}()">Batal</button>
									@endif
								</div>
								
							</div>
						<script type="text/javascript">
						function myFunctionProses{{$dt->id}}() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Topup Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("btnProses{{$dt->id}}").style.display="disabled";
							document.getElementById('proses{{$dt->id}}').submit();
						}
						function myFunctionCancel{{$dt->id}}() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Cancel Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("btnCancel{{$dt->id}}").style.display="disabled";
							document.getElementById('cancel{{$dt->id}}').submit();
						}
					</script>
					@endforeach
									
													
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection