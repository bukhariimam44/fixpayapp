@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Mutasi Saldo Supplier</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					<div class="w3agile properties">
				
					@if(count($datas) < 1) <p style="text-align:center;">Kosong</p> @endif
					@foreach($datas as $key => $dt)
					<!-- properties -->
					<div class="properties-bottom">
								<div class="w3ls-text">
									<h5><a href="#">{{$dt['keterangan']}}</a></h5>
									<h6 style="color:red;">Type : {{$dt['mutasi']}}</h6>
									<h6 style="font-size:14;color:black;">Nominal : <a style="font-size:14;color:#11909e;">Rp {{number_format($dt['nominal'],'0',',','.')}}</a></h6>

									<h6 style="font-size:14;color:black;">No Transaksi : <a style="font-size:14;color:#11909e;"> {{$dt['no_trx']}}</a></h6>
									<h6 style="font-size:14;color:black;">Tgl Transaksi : <a style="font-size:14;color:#11909e;"> {{date('d M Y H:i', strtotime($dt['created_at']))}}</a></h6>
									<br>
									<h5>  <button class="btn btn-success" >SALDO @if($key ==0) AKHIR @endif: Rp {{number_format($dt['saldo_akhir'],'0',',','.')}}</button></h5>
								</div>
								
							</div>
					@endforeach
													
													
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
@endsection