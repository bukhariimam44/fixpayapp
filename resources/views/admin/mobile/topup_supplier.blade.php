@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll"  id="tambah_saldo_supplier">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Topup Saldo Supplier</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;">
					<div class="col-md-12">
							<div class="form-group">
								<label for="">Bank Transfer Tujuan</label>
								<select v-model="selected" class="form-control">
											<option v-for="bank in banks" v-bind:value="bank.id">
															@{{ bank.nama }}
											</option>
									</select>
							</div>
							<div class="form-group">
									<label for="">Nominal</label>
									<money v-model="nominal" v-bind="money" class="form-control"></money>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Penjelasan Form :</h4><br>
						<ol>
							<li>Pilih Bank Transfer Tujuan.</li>
							<li>Ketikan Nominal yang ingin anda depositkan. Nominal harus kelipatan 1000</li>
							<li>Setelah diisi semua, klik 'PROSES'. Perhatikan pesan yang muncul.
Anda HARUS mentransfer sesuai nominal yang kami sebutkan, agar sistem dapat mengenali dana transfer Anda. <br>
Maksimal transfer dana adalah 1x24jam dari tanggal request. Jika melebihi batas waktu dan belum transfer, Maka Anda harus request deposit ulang.
Dana akan masuk ke saldo Anda maksimal 15 menit setelah dana kami terima.</li>
						</ol>
				</div>
			</div>

<!-- //banner --> 
<div class="row">
					<div class="col-md-12">
							<div class="w3agile properties">
								<div class="properties-bottom" v-for="(transaksi,index) in transaksis" key="transaksi.id">
											<div class="w3ls-text">
													<h5 style="color:#12909e;">@{{transaksi.keterangan}} </h5>
													<br>
													<h5><a href="#">Transfer Rp @{{formatPrice(transaksi.nominal)}}</a></h5>
													<br>
													<h6 style="font-size:14;color:black;">No Transaksi : @{{transaksi.no_trx}}</h6>
													<h6 style="font-size:14;color:black;">Tgl Transaksi : @{{frontEndDateFormat(transaksi.created_at)}}</h6>
													<br>
													<button v-if="transaksi.status === 'berhasil' " class="btn btn-success">@{{transaksi.status}}</button>	<button v-if="transaksi.status === 'batal' " class="btn btn-danger">@{{transaksi.status}}</button> <button v-if="transaksi.status === 'menunggu' " class="btn btn-warning">@{{transaksi.status}}</button>
											</div>
								</div>
								<div class="clearfix"> </div>
							</div> 
					</div>
				</div>

</div>

@endsection
@section('js')
<script src="https://rawgit.com/vuejs-tips/v-money/master/dist/v-money.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
    var data_product = new Vue({
        el:'#tambah_saldo_supplier',
        data:{
												selected: '',
            banks: [],
												nominal:'',
												transaksis:[],
												money: {
														decimal: ',',
														thousands: '.',
														precision: 0,
														masked: false
												}
        },
        mounted() {
            this.load()
								},
        methods:{
            formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
												frontEndDateFormat: function(date) {
													return moment(date, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY H:mm');
												},
            async load(){
													let url_bang_supplier = "<?php echo route('admin-bank-supplier');?>";
                await axios.get(url_bang_supplier).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
																				this.banks = response.data.bank;
                    this.selected = response.data.bank[0].id;
																				this.transaksis = response.data.transaksi;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
												//Loading
												Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.selected && this.nominal >= 500000) {
															let url_proses_topup_supplier = "<?php echo route('admin-proses-topup-supplier');?>";
                let request = {bank_id : this.selected,nominal : this.nominal};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(url_proses_topup_supplier,request).then((response) =>{
                    console.log('Berhasil proses'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
																					this.load()
                      swal({
                        title: "INFO",
																								text: response.data.data,
                        icon: "warning",
                        Buttons: ['OK'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          // swal("Silahkan Transfer, \n Terimakasih... ", {
                          //   icon: "success",
																										// });
																										Swal.close()
																										// window.location.href = "<?php //echo route('admin-topup-supplier'); ?>";
                        } else {
																									Swal.close()
                          // window.location.href = "<?php //echo route('admin-topup-supplier'); ?>";
                        }
                      });
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Gagal!", "Bank harus dipilih!", "error");
																Swal.close()
              }
              if (this.nominal  < 500000) {
                swal("Gagal!", "Nominal Minimal Rp 500,000 ", "error");
																Swal.close()
              }
            }

        }
    });
</script>
@endsection