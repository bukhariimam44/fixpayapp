@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Harga {{$product}}</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					@include('flash::message')
					<button class="pull-right btn btn-success" onclick="event.preventDefault();
                document.getElementById('update-harga').submit();">Update</button>
					<div class="w3agile properties">
					<form action="{{route('admin-data-harga',$ids)}}" method="post" id="update-harga">
											@csrf
											<input type="hidden" name="action" value="update">
											@foreach($datas as $key => $data)
											<input type="hidden" name="code[]" value="{{$data->code}}">
											<div class="properties-bottom">
														<div class="w3ls-text">
														<h6 style="font-size:14;color:black;">Provider : <a style="font-size:14;color:#11909e;">{{$data->provider}}</a></h6>
															<h6 style="font-size:14;color:black;">Description : <a style="font-size:14;color:#11909e;">{{$data->description}}</a></h6>
															<h6 style="font-size:14;color:black;">Harga Supplier : <a style="font-size:14;color:#11909e;"> Rp {{number_format($data->price)}}</a></h6>
															<h6 style="font-size:14;color:black;">Markup Agen : <input type="number" name="markup_agen[]" id="markup_agen[]" value="{{$data->markup_agen}}" required=""/></h6>
															<h6 style="font-size:14;color:black;">Markup Admin : <input type="number" name="markup[]" id="markup[]" value="{{$data->markup}}" required=""/></h6>
															<h6 style="font-size:14;color:black;">Harga Untuk Agen : <a style="font-size:14;color:#11909e;"> Rp {{number_format($data->markup_agen+$data->price)}}</a></h6>
															<h6 style="font-size:14;color:black;">Harga Untuk NonAgen : <a style="font-size:14;color:#11909e;"> Rp {{number_format($data->markup+$data->price)}}</a></h6>
														</div>
											</div>
											@endforeach
										</form> 
										<div class="clearfix"> </div>
								</div> 

					<!-- <div class="w3agile properties">
										<div>
								<div class="w3ls-text">
								<div class="table-responsive">
								<table class="table table-striped"> 
                      <thead> 
                          <tr> 
                              <th>No.</th> 
                              <th>Provider</th>
                              <th>Code</th> 
                              <th>Description</th> 
                              <th>Harga Supplier</th>
                              <th>Markup Agen</th>
																														<th>Markup Admin</th>
                              <th>Harga Jual</th>
                            </tr> 
                        </thead> 
                        <tbody> 
                        <form action="{{route('admin-data-harga',$ids)}}" method="post" id="update-harga">
                        @csrf
                          <input type="hidden" name="action" value="update">
                            @foreach($datas as $key => $data)
                            <input type="hidden" name="code[]" value="{{$data->code}}">
                            <tr> 
                                <th scope="row">{{$key+1}}.</th> 
                                <td>{{$data->provider}}</td> 
                                <td>{{$data->code}}</td> 
                                <td>{{$data->description}}</td> 
                                <td>{{number_format($data->price)}}</td> 
                                <td width="130px">
                                    <input type="text" name="markup_agen[]" value="{{number_format($data->markup_agen)}}" class="form-control">
                                </td> 
																																<td width="130px">
                                    <input type="text" name="markup[]" value="{{number_format($data->markup)}}" class="form-control">
                                </td> 
                                <td>{{number_format($data->markup+$data->price)}}</td> 
                            </tr> 
                            @endforeach
                        </form> 
                        </tbody> 
                    </table>
					</div>
							

								</div>
								
							</div>
										<div class="clearfix"> </div>
								</div>  -->
					</div>
				</div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection