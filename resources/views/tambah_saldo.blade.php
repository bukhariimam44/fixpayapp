@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Tambah Saldo</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;" id="tambah_saldo">
				<div class="col-md-12">
							<div class="form-group">
								<label for="">Bank Transfer Tujuan</label>
								<select v-model="selected" class="form-control">
											<option v-for="bank in banks" v-bind:value="bank.id">
															@{{ bank.nama }} - @{{ bank.no_rek }}, An. @{{ bank.atas_nama }}
											</option>
									</select>
							</div>
							<div class="form-group">
									<label for="">Nominal</label>
									<input type="number" v-model="nominal" name="nominal" class="form-control">
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Panduan :</h4><br>
						<ol>
							<li>Pilih Bank Transfer Tujuan.</li>
							<li>Masukan Nominal (Minimal Rp 10,000).</li>
							<li>Klik Proses</li>
							<li>Jumlah yang ditransfer akan muncul setelah klik proses (Nominal yang dimasukan + 3 angka kode unik)</li>
							<li>Lakukan Transfer sesuai yang di tampilkan, agar sistem dapat mengenali uang yg di transfer.</li>
							<li>Kurang dari 3 menit Saldo akan otomatis bertambah sesuai dengan jumlah yang ditransfer.</li>
						</ol>
						<p> <strong style="color:red;">INGAT !</strong>  Jangan melakukan transfer sebelum perintah transfernya muncul (Setelah klik PROSES) dan Jumlah yang ditransfer harus sesuai yang ditampilan di aplikasi.</p>
				</div>
			</div>
</div>
@endsection
@section('js')
<script>
    var data_product = new Vue({
        el:'#tambah_saldo',
        data:{
												selected: '',
            banks: [],
												nominal:'',
        },
        mounted() {
            this.load()
								},
        methods:{
            formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                await axios.get(window.url_bank).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
																				this.banks = response.data.bank;
                    this.selected = response.data.bank[0].id;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
												//Loading
												Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.selected && this.nominal >= 10000) {
                let request = {bank_id : this.selected,nominal : this.nominal};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses_deposit,request).then((response) =>{
                    console.log('Berhasil proses'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
																					this.load()
                      swal({
                        title: "INFO",
																								text: "Nominal yang harus ditransfer Rp "+response.data.nominal+" ke Nomor rekening : "+response.data.bank.no_rek+" ("+response.data.bank.nama+"), atas nama : "+response.data.bank.atas_nama,
                        icon: "warning",
                        Buttons: ['OK'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          // swal("Silahkan Transfer, \n Terimakasih... ", {
                          //   icon: "success",
																										// });
																										window.location.href = "<?php echo route('data-topup'); ?>";
                        } else {
                          window.location.href = "<?php echo route('data-topup'); ?>";
                        }
                      });
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Bank harus dipilih!", "error");
																Swal.close()
              }
              if (this.nominal  < 10000) {
                swal("Cek Kembali!", "Nominal Minimal Rp 10,000 ", "error");
																Swal.close()
              }
            }

        }
    });
</script>
@endsection