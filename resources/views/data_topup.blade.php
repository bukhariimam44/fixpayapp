@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
				<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Data Topup</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					<div class="w3agile properties">
					@if(count($datas) < 1) <p style="text-align:center;">Kosong</p> @endif
					@foreach($datas as $key => $dt)
					<!-- properties -->
					<div class="properties-bottom">
								<div class="w3ls-text">
									<h5><a href="#">{{$dt->bankId->nama}} ({{$dt->bankId->no_rek}}) </a></h5>
									<h6 style="color:#11909e;">Atas Nama : {{$dt->bankId->atas_nama}}</h6>
									<br>
									<h5><a href="#">Transfer Rp {{number_format($dt->transfer,'0',',','.')}}</a></h5>
									<h6 style="font-size:14;color:black;">Kode Unik : <a style="font-size:14;color:red;">{{substr($dt->transfer,-3)}}</a></h6>
									<br>
									<h6 style="font-size:14;color:black;">No Transaksi : {{$dt->no_trx}}</h6>
									<h6 style="font-size:14;color:black;">Tgl Transaksi : {{date('d M Y H:i', strtotime($dt->created_at))}}</h6>
									<!-- <p><b>No Transaksi</b> {{$dt->no_trx}} </p>
									<p><b>Tgl Transaksi :</b> {{date('d M Y H:i', strtotime($dt->created_at))}} </p> -->
<br>
									<button class="btn @if($dt->status == 'menunggu') btn-warning @elseif($dt->status == 'berhasil') btn-success @else btn-danger @endif">{{$dt->status}}</button>
								</div>
								
							</div>
					@endforeach
													
													
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
@endsection