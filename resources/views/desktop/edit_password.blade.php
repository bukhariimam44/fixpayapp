@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Ganti Password</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row" id="edit_password">
					<div class="col-md-12">
					<div class="w3agile properties">

											<div class="properties-bottom">
														<div class="w3ls-text">
												
															<h6>Password Lama</h6>
															<input type="password" name="ps_lm" v-model="ps_lm" class="form-control">
															<h6>Password Baru</h6>
															<input type="password" name="ps_br" v-model="ps_br" class="form-control">
	<br>
															<button class="btn btn-success" @click="simpan()">Simpan</button>
														</div>
														
													</div>

													<div class="clearfix"> </div>
												</div> 
					</div>
				</div>
@endsection
@section('js')
<script>
var edit_password = new Vue({
	el:'#edit_password',
	data() {
		return {
			ps_lm:'',
			ps_br:'',
		}
	},
	methods: {
		async simpan(){
			Swal.fire({
								title: 'Mohon menunggu...',
								text:'Sedang diproses...',
								allowEscapeKey: false,
								allowOutsideClick: false,
								background: '#FFFFFF',
								showConfirmButton: false,
								onOpen: ()=>{
												Swal.showLoading();
								}
				}).then((dismiss) => {
					// Swal.showLoading();
					}
			);
					//END LOADING
					this.errors = [];
			if (this.ps_lm.length < 6) {
					swal("Opps!", "Password Lama Minimal 6 karakter", "error");
					Swal.close()
			}
			else if (this.ps_br.length < 6) {
					swal("Opps!", "Password Baru Minimal 6 Digit", "error");
					Swal.close()
			}
			else{
				let request = {password_lama : this.ps_lm, password: this.ps_br,action:'edit'};
				request['token'] = document.querySelector('#token').getAttribute('value');
				await axios.post(url_simpan_password,request).then((response) =>{
								console.log('Res Login'+JSON.stringify(response.data));
								if (response.data.code === 200) {
									this.ps_lm = '';
									this.ps_br = '';
									swal({
											title: response.data.title,
											text: response.data.message,
											icon: "warning",
											buttons: ['Tidak','Ya'],
											dangerMode: true,
									})
									.then((willDelete) => {
											if (willDelete) {
													swal("Terimakasih, Sampai Jumpa Lagi", {
															icon: "success",
													});
													window.location.href = "<?php echo route('keluar'); ?>";
											} else {
												Swal.close()
											}
									});
								}else{
										swal(response.data.title, response.data.message, "error");
										Swal.close()
								}
				},(response)=>{
								// console.log('ERROR: '+response);
								swal("Gagal!", response, "error");
								Swal.close()
				}); 
			}
		}
	},
});
</script>
@endsection