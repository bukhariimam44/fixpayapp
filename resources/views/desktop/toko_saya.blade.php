@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Toko Saya</h3>   
					</div> 
				</div>
					<!-- properties --> 
					<div class="w3agile properties"> 
					<div class="properties-w3lsrow"> 
@foreach($tokos as $key => $toko)
						<div class="properties-bottom">
							<div class="properties-img">
								<img src="{{asset('images/tokos/'.$toko->foto)}}" alt="">
								<div class="view-caption">
								<a href="{{route('detail-toko-saya',$toko->id)}}">	<h4><span class="glyphicon glyphicon-home"></span>  {{$toko->nama}}</h4>  </a>
								</div>
								
							</div>
							<div class="w3ls-text">
								<!-- <h5><a href="single.html">{{$toko->nama}}</a></h5> <br> -->
								<p><b>Jumlah Produk :</b> 12 </p> <br>
								<p><b>Terjual :</b> 2 </p> <br>
							</div>
						</div>
@endforeach
						<div class="clearfix"> </div>
					</div>
				</div>
				<!-- //properties --> 
</div>
			
@endsection
@section('js') 
<script>
	var kontak = new Vue({
		el:'#kontak',
		data: {
			name:'',
			email:'',
			judul:'',
			pesan:'',
			reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
			error:[]
		},
		methods: {
			async kirimPesan(){
				//Loading
				Swal.fire({
									title: 'Mohon menunggu...',
									text:'Pesan Sedang Dikirim..',
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
				//END LOADING
				//FALIDASI
				this.errors = [];
					if (this.name.length < 3) {
							swal("Cek Kembali!", "Nama minimal 3 karakter", "error");
							Swal.close();
					}else if(this.email.length < 7){
							swal("Cek Kembali!", "Email minimal 7 karakter", "error");
							Swal.close();
					}else if(!this.reg.test(this.email)){
							swal("Cek Kembali!", "Email tidak falid", "error");
							Swal.close();
					}else if(this.judul.length < 3){
							swal("Cek Kembali!", "Judul minimal 3 karakter", "error");
							Swal.close();
					}else if(this.pesan.length < 3){
							swal("Cek Kembali!", "Pesan minimal 3 karakter", "error");
							Swal.close();
					}
					//EDN FALIDASI
				
				if (this.name && this.email && this.judul && this.pesan) {
					swal({
							title: 'Berhasil',
							text: "Pesan Berhasil Terkirim",
							icon: "success",
							dangerMode: true,
					})
					.then((willDelete) => {
							if (willDelete) {
									// swal("Terimakasih, Silahkan", {
									// 		icon: "success",
									// });
									this.name = '';
									this.email = '';
									this.judul = '';
									this.pesan = '';
									Swal.close()
							} else {
								this.name = '';
									this.email = '';
									this.judul = '';
									this.pesan = '';
									Swal.close()
							}
					});
				}
				
			}
		},
	});
</script>
@endsection