@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Mau Komisi</h3>   
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12">
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
														<h6 style="font-size:14;color:black;">Mau mendapatkan Komisi yang lebih banyak ?</h6>
														<h6 style="font-size:14;color:black;">Caranya GAMPANG !</h6>
														<h6 style="font-size:14;color:black;">Bagikan Link dibawah, maka kamu akan berpeluang mendapatkan Komisi otomatis 100 ribu sampai 1 juta perbulan. </h6>
														<hr>
														<h6 style="font-size:14;color:#11909e;" id="link">{{route('share',Auth::user()->kode)}}</h6><br>
														<button class="btn btn-success" onclick="copyToClipboard('#link')">Copy Link</button>
												</div>
										</div>
							</div>
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
												<h6 style="font-size:14;color:black;">Semakin banyak orang yang mendaftar dari Link yang kamu bagikan, maka semakin banyak komisi yang akan didapatkan. </h6>
														<h6 style="font-size:14;color:black;">Data Orang yang mendaftar Dari Link yang kamu bagikan :</h6>
														@foreach($datas as $key => $dt)
														<h6 style="font-size:14;color:#11909e;">{{$key+1}}. {{$dt->name}}</h6>
														@endforeach
														@if(count($datas) < 1)
														<h6 style="font-size:14;color:#11909e;">Belum Ada</h6>
														@endif
												</div>
										</div>
							</div>
					</div>
			</div>
</div>
@endsection
@section('js')
<script>
function copyToClipboard(elementId) {
	var $temp = $("<input>");
	$("body").append($temp);
  $temp.val($(elementId).text()).select();
  document.execCommand("copy");
  $temp.remove();
		swal('Berhasil', "Berhasil Copy", "success");
}
</script>
@endsection