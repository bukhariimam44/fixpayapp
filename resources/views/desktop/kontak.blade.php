@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Kontak</h3>   
					</div> 
				</div>
				<div class="w3agile contact" id="kontak"> 
					<h3 class="w3ls-title">Hubungi Kami</h3>
					<div class="contact-form"> 
							<input type="text" name="Name" v-model="name" placeholder="Nama" required="">
							<input type="text" name="Email" v-model="email" placeholder="Email" required="">
							<input type="text" name="judul" v-model="judul" placeholder="Judul" required="">
							<textarea name="pesan" v-model="pesan" placeholder="Pesan" required=""></textarea> <br><br>
							<button type="button" class="btn btn-success" @click="kirimPesan()">KIRIM</button>
					</div>
					@if(config('app.name') == 'FIXPAY')
					<div class="map"> 
					<hr>
						<h3 class="w3ls-title">MAPS</h3>
						<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.9503398796587!2d-73.9940307!3d40.719109700000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1441710758555" allowfullscreen></iframe> -->
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2912630649316!2d106.74408661426882!3d-6.225274662700637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f0b60f4782ff%3A0x4f96b591014993eb!2sJl.%20H.%20Misar%2052%2C%20RT.6%2FRW.11%2C%20Petukangan%20Utara%2C%20Kec.%20Pesanggrahan%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2012260!5e0!3m2!1sen!2sid!4v1594092081545!5m2!1sen!2sid" allowfullscreen></iframe>
					</div>
					<div class="contact-form"> 
						<h3 class="w3ls-title">Kontak Info</h3>
						<p><b>Alamat :</b> Petukangan - Jakarta Selatan </p>
						<p><b>HP / WA :</b> +62 851-5503-0013</p>
						<!-- <p><b>Fax :</b> (1234) 888 8884</p> -->
						<p><b>Email :</b> <a href="mailto:fixpayapp@gmail.com">fixpayapp@gmail.com</a></p>
					</div>
					@else
					<div class="map"> 
					<hr>
						<h3 class="w3ls-title">MAPS</h3>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.9503398796587!2d-73.9940307!3d40.719109700000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1441710758555" allowfullscreen></iframe>
							<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.2912630649316!2d106.74408661426882!3d-6.225274662700637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f0b60f4782ff%3A0x4f96b591014993eb!2sJl.%20H.%20Misar%2052%2C%20RT.6%2FRW.11%2C%20Petukangan%20Utara%2C%20Kec.%20Pesanggrahan%2C%20Kota%20Jakarta%20Selatan%2C%20Daerah%20Khusus%20Ibukota%20Jakarta%2012260!5e0!3m2!1sen!2sid!4v1594092081545!5m2!1sen!2sid" allowfullscreen></iframe> -->
					</div>
					<div class="contact-form"> 
						<h3 class="w3ls-title">Kontak Info</h3>
						<p><b>Alamat :</b> Ciputat - Tangerang </p>
						<p><b>HP / WA :</b> +62 858-8168-9129</p>
						<!-- <p><b>Fax :</b> (1234) 888 8884</p> -->
						<p><b>Email :</b> <a href="mailto:warungimbuhpay@gmail.com">warungimbuhpay@gmail.com</a></p>
					</div>
					@endif
				</div>
</div>
			
@endsection
@section('js') 
<script>
	var kontak = new Vue({
		el:'#kontak',
		data: {
			name:'',
			email:'',
			judul:'',
			pesan:'',
			reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
			error:[]
		},
		methods: {
			async kirimPesan(){
				//Loading
				Swal.fire({
									title: 'Mohon menunggu...',
									text:'Pesan Sedang Dikirim..',
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
				//END LOADING
				//FALIDASI
				this.errors = [];
					if (this.name.length < 3) {
							swal("Cek Kembali!", "Nama minimal 3 karakter", "error");
							Swal.close();
					}else if(this.email.length < 7){
							swal("Cek Kembali!", "Email minimal 7 karakter", "error");
							Swal.close();
					}else if(!this.reg.test(this.email)){
							swal("Cek Kembali!", "Email tidak falid", "error");
							Swal.close();
					}else if(this.judul.length < 3){
							swal("Cek Kembali!", "Judul minimal 3 karakter", "error");
							Swal.close();
					}else if(this.pesan.length < 3){
							swal("Cek Kembali!", "Pesan minimal 3 karakter", "error");
							Swal.close();
					}
					//EDN FALIDASI
				
				if (this.name && this.email && this.judul && this.pesan) {
					swal({
							title: 'Berhasil',
							text: "Pesan Berhasil Terkirim",
							icon: "success",
							dangerMode: true,
					})
					.then((willDelete) => {
							if (willDelete) {
									// swal("Terimakasih, Silahkan", {
									// 		icon: "success",
									// });
									this.name = '';
									this.email = '';
									this.judul = '';
									this.pesan = '';
									Swal.close()
							} else {
								this.name = '';
									this.email = '';
									this.judul = '';
									this.pesan = '';
									Swal.close()
							}
					});
				}
				
			}
		},
	});
</script>
@endsection