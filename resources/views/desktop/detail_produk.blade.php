@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<meta name="asal" id="asal" value="{{$produks->tokoId->kota_id}}">
<style>
    html,
    body {
      position: relative;
      height: 100%;
    }

    body {
      background: #eee;
      font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
      font-size: 14px;
      color: #000;
      margin: 0;
      padding: 0;
    }

    .swiper-container {
      width: 100%;
      height: 100%;
    }

    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
  </style>
		<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
@endsection
@section('content') 
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Detail Barang</h3>   
					</div> 
				</div>
					<!-- properties --> 
					<div class="w3agile properties"> 
					<!-- <h3 class="w3ls-title"></h3>  -->
					<!-- <p class="w3-text">Lorem ipsum dolor sit amet consectetur adip iscing elit. Nam vestibulum ipsum quis purus varius efficitur nunc eget purus ac risus facilisis.</p> -->

					<div class="properties-w3lsrow"> 

						<div class="properties-bottom">

								<div class="w3ls-text">
									<!-- Swiper -->
											<div class="swiper-container">
													<div class="swiper-wrapper">
															<div class="swiper-slide"><img src="{{asset('images/produks/'.$produks->gambar_depan)}}" alt=""></div>
															<div class="swiper-slide"><img src="{{asset('images/produks/'.$produks->gambar_samping)}}" alt=""></div>
															<div class="swiper-slide"><img src="{{asset('images/produks/'.$produks->gambar_belakang)}}" alt=""></div>
													</div>
													<!-- Add Pagination -->
													<div class="swiper-pagination"></div>
											</div>
											<!-- Swiper -->
								
								</div>
							
							<div class="w3ls-text"  id="detail">
							<h5><a href="#" style="color:red;">Rp {{number_format($produks->harga,0,',','.')}}</a> <br> <a href="#" style="color:grey;font-size:14px;"><strike>Rp {{number_format($produks->harga+($produks->harga*25/100),0,',','.')}}</strike> -25%</a></h5> <br><br>
								<h5 style="font-size:25px;color:#11909e;">{{$produks->nama_barang}}</h5> <br>
								<p><b>Dikirim dari :</b> <br> {{$produks->tokoId->alamat_pengiriman}}, <br> {{$produks->tokoId->kotaId->kota}} - {{$produks->tokoId->propinsiId->propinsi}}</p> <br><br>
								<div class="w3ls-text" v-show="ongkir">
								<center> <h5>Cek Ongkir</h5> </center>
								<br>
								
									<div class="row">
										<div class="form-group col-sm-3 col-md-3 col-xs-6" style="width:50%;">
											<label for="">Berat</label>
											<input type="text" class="form-control" value="{{$produks->berat}} Gram" disabled>
										</div>
										<div class="form-group col-sm-3 col-md-3 col-xs-6" style="width:50%;">
											<label for="">Jumlah</label>
											<input type="number" v-model="jumlah" class="form-control">
										</div>
									</div>

									<div class="form-group">
										<label for="">Propinsi Penerima</label>
										<select v-model="select_propinsi" @change="loadKota()" class="form-control">
													<option v-for="propinsi in propinsis" v-bind:value="propinsi.id">
																	@{{ propinsi.propinsi }}
													</option>
											</select>
									</div>
									<div class="form-group">
										<label for="">Kota Penerima</label>
										<select v-model="select_kota" class="form-control">
													<option v-for="kota in kotas" v-bind:value="kota.id">
																	@{{ kota.kota }} (Kode Pos : @{{ kota.kode_pos }})
													</option>
											</select>
									</div>
									<div class="form-group">
										<label for="">Kurir</label>
										<select v-model="kurir" class="form-control">
													<option v-for="kurire in kurires" v-bind:value="kurire.value">
																	@{{ kurire.text }}
													</option>
											</select>
									</div>
									<hr>
									<div class="form-group">
										<ul style="margin-left:15px;">
											<li v-for="(ongkos_kirimes, index) in ongkos_kirim"> @{{ ongkos_kirimes.service }} : Rp @{{ formatPrice(ongkos_kirimes.cost[0].value) }}, (@{{ ongkos_kirimes.cost[0].etd }} Hari)</li>
										</ul>
									</div>
									<div class="form-group">
										<button class="btn btn-primary form-control" @click="checkOngkir()">Cek Ongkir</button>
									</div>
									</div>
									<!-- <div class="w3ls-text" v-show="tombol"> -->
											<a href="{{route('toko',encrypt($produks->toko_id))}}" class="btn btn-warning"><i class="glyphicon glyphicon-home"></i> Toko</a> <button class="btn btn-success"><i class="glyphicon glyphicon-shopping-cart"></i> Beli</button> <button class="btn btn-danger" @click="cekOngkir()"><span v-if="ongkir"><i class="glyphicon glyphicon-remove-circle"></i> Tutup Ongkir</span><span v-else><i class="glyphicon glyphicon-send"></i> Cek Ongkir</span></button>
									<!-- </div> -->
								</div>

								
						</div>
						

						<div class="clearfix"> </div>
					</div>
				</div>
			
@endsection
@section('js') 
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
  </script>
		<script>
		var detail = new Vue({
			el:'#detail',
			data: {
				ongkir:false,
				select_propinsi: null,
				propinsis: [],
				select_kota:null,
				kotas:[],
				berat:<?php echo $produks->berat;?>,
				jumlah:1,
				kurir:'jne',
				kurires:[
					{
						text:'JNE',value:'jne'
					},
					{
						text:'TIKI',value:'tiki'
					},
					{
						text:'POS',value:'pos'
					}
				],
				ongkos_kirim:[]
			},
			mounted() {
				this.loadPronpinsi()
			},
			methods: {
				cekOngkir(){
					this.ongkir == false ? this.ongkir = true: this.ongkir = false;
				},
				formatPrice(value) {
								// let val = (value/1).toFixed(0).replace('.', ',')
								return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
				},
				async loadPronpinsi(){
						
							let url_propinsi = '<?php echo route('propinsi');?>';
								await axios.get(url_propinsi).then((response) =>{
												console.log('Propinsi : '+JSON.stringify(response.data));
												this.propinsis = response.data;
												this.select_propinsi = response.data[0].id;
												this.loadKota()
								},(response)=>{
												console.log('ERROR: '+response);
								});
				},
				async loadKota(){
						//Loading
						Swal.fire({
												title: 'Mohon menunggu...',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
					let url_kota = '<?php echo route('kota');?>';
								let request = {propinsi : this.select_propinsi};
								request['token'] = document.querySelector('#token').getAttribute('value');
								await axios.post(url_kota,request).then((response) =>{
												console.log('Propinsi : '+JSON.stringify(response.data));
												this.kotas = response.data;
												this.select_kota = response.data[0].id;
												Swal.close()
								},(response)=>{
												console.log('ERROR: '+response);
												Swal.close()
								});
				},
				async checkOngkir(){
					// Loading
					Swal.fire({
												title: 'Mohon menunggu...',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
					let url_ongkir = '<?php echo route('check-ongkir');?>';
								let request = {tujuan : this.select_kota,kurir:this.kurir,berat:this.berat*this.jumlah};
								request['asal'] = document.querySelector('#asal').getAttribute('value');
								request['token'] = document.querySelector('#token').getAttribute('value');
								await axios.post(url_ongkir,request).then((response) =>{
												console.log('Respon cek ongkir : '+JSON.stringify(response.data));
												
												if (response.data.code === 200) {
													this.ongkos_kirim = response.data.data.rajaongkir.results[0].costs;
													console.log('Respon cek ongkir kurir: '+JSON.stringify(response.data.data.rajaongkir.results[0].costs));
													// Swal.fire({
													// 		title: 'Are you sure?',
													// 		text: "You won't be able to revert this!",
													// 		icon: 'warning',
													// 		showCancelButton: true,
													// 		confirmButtonColor: '#3085d6',
													// 		cancelButtonColor: '#d33',
													// 		confirmButtonText: 'Yes, delete it!'
													// }).then((result) => {
													// 		if (result.value) {
													// 				Swal.fire(
													// 						'Deleted!',
													// 						'Your file has been deleted.',
													// 						'success'
													// 				)
													// 		}
													// })
												}
												Swal.close()
								},(response)=>{
												console.log('ERROR: '+response);
												Swal.close()
								});
				}
			},
		});
		</script>
@endsection