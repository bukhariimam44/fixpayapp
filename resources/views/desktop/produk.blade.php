@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>{{$categories->kategori}} </h3>   
					</div> 
				</div>
					<!-- properties --> 
					<div class="w3agile properties"> 
					<!-- <h3 class="w3ls-title">Produk</h3>  -->
					<!-- <p class="w3-text">Barang - barang yang di jual di Toko  </p> <br> -->
					<div class="properties-w3lsrow"> 
					@foreach($produks as $key => $produk)
					<a href="{{route('detail-produk',encrypt($produk->id))}}">
						<div class="properties-bottom">
							<div class="w3ls-text">
								<img src="{{asset('images/produks/'.$produk->gambar_depan)}}" alt="">
							</div>
							<div class="w3ls-text">
								<h5><a href="{{route('detail-produk',encrypt($produk->id))}}" style="font-size:25px;color:#11909e;">{{$produk->nama_barang}}</a></h5> <br><br>
								<h5><a href="{{route('detail-produk',encrypt($produk->id))}}" style="color:red;">Rp {{number_format($produk->harga,0,',','.')}}</a> <a href="{{route('detail-produk',$produk->id)}}" style="color:grey;font-size:14px;"><strike>Rp {{number_format($produk->harga+($produk->harga*25/100),0,',','.')}}</strike></a></h5> <br>
								<!-- <p><b>Harga :</b> Rp {{number_format($produk->harga,0,',','.')}} </p> <br> -->
								<p>Kota {{$produk->tokoId->kotaId->kota}} </p> <br>
							</div>
						</div>
						</a>
					@endforeach
						<div class="clearfix"> </div>
					</div>
				</div>
			
@endsection
@section('js') 

@endsection