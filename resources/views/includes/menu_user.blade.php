								@if(config('app.name') == 'FIXPAY')
									@if(Auth::user()->id == 1)
									<li><a href="#" class="menu shop"><i class="glyphicon glyphicon-shopping-cart"></i>Belanja<span class="glyphicon glyphicon-menu-down"></span></a>
										<ul class="nav-sub shoping">
											@foreach(App\Category::get() as $menu)
											<li><a href="{{route('produk',encrypt($menu->id))}}"><i class=""></i>{{$menu->kategori}}</a></li>   
											@endforeach                                                                                     																													
										</ul>			
									<div class="clearfix"> </div>
									<script>
										$( "li a.shop" ).click(function() {
										$( "ul.shoping" ).slideToggle( 300, function() {
										});
										});
									</script> 
								</li>
									@endif
								@else
									@if(Auth::user()->id == 41 || Auth::user()->id == 1)
									<li><a href="#" class="menu shop"><i class="glyphicon glyphicon-shopping-cart"></i>Belanja<span class="glyphicon glyphicon-menu-down"></span></a>
										<ul class="nav-sub shoping">
											@foreach(App\Category::get() as $menu)
											<li><a href="{{route('produk',encrypt($menu->id))}}"><i class=""></i>{{$menu->kategori}}</a></li>   
											@endforeach                                             																														
										</ul>			
									<div class="clearfix"> </div>
									<script>
										$( "li a.shop" ).click(function() {
										$( "ul.shoping" ).slideToggle( 300, function() {
										});
										});
									</script> 
								</li>
									@endif
								@endif
								<li><a href="#" class="menu atu {{setActive(['transaksi-pulsa','transaksi-pln','transaksi-paket-data','transaksi-voucher-game','transaksi-e-money','transaksi-sms','transaksi-telpon'])}}"><i class="glyphicon glyphicon-send"></i>Transaksi<span class="glyphicon glyphicon-menu-down"></span></a>
									<ul class="nav-sub satu">
									<li><a href="{{route('transaksi-pulsa')}}"><i class="glyphicon glyphicon-phone"></i>Pulsa</a></li>                                             
									<li><a href="{{route('transaksi-pln')}}"><i class="glyphicon glyphicon-flash"></i>Voucher PLN</a></li> 
									<li><a href="{{route('transaksi-paket-data')}}"><i class="glyphicon glyphicon-modal-window"></i>Paket Data</a></li>    
									<li><a href="{{route('transaksi-sms')}}"><i class="glyphicon glyphicon-sound-dolby"></i>Paket SMS</a></li>    
									<li><a href="{{route('transaksi-telpon')}}"><i class="glyphicon glyphicon-earphone"></i>Paket Telpon</a></li>    
									<li><a href="{{route('transaksi-voucher-game')}}"><i class="glyphicon glyphicon-knight"></i>Voucher Game</a></li>      
									<li><a href="{{route('transaksi-e-money')}}"><i class="glyphicon glyphicon-asterisk"></i>E-Wallet</a></li>                                                                         
									</ul>			
									<div class="clearfix"> </div>
									<script>
										$( "li a.atu" ).click(function() {
										$( "ul.satu" ).slideToggle( 300, function() {
										});
										});
									</script> 
								</li>
								<li><a href="#" class="menu depo {{setActive(['tambah-saldo','data-topup','mutasi-saldo'])}}"><i class="glyphicon glyphicon-credit-card"></i>Deposit<span class="glyphicon glyphicon-menu-down"></span></a>
									<ul class="nav-sub dipi">
									<li><a href="{{route('tambah-saldo')}}"><i class="glyphicon glyphicon-plus"></i>Tambah Saldo </a></li>   
									<li><a href="{{route('data-topup')}}"><i class="glyphicon glyphicon-th-list"></i>Data Deposit </a></li>                                          
									<li><a href="{{route('mutasi-saldo')}}"><i class="glyphicon glyphicon-transfer"></i>Mutasi Saldo </a></li>                                            
									</ul>			
									<div class="clearfix"> </div>
									<script>
										$( "li a.depo" ).click(function() {
										$( "ul.dipi" ).slideToggle( 300, function() {
										});
										});
									</script> 
								</li>
								<li><a href="#" class="menu laporan {{setActive(['laporan-pulsa','laporan-voucher-pln','laporan-paket-data','laporan-paket-sms','laporan-paket-telpon','laporan-game','laporan-e-money'])}}"><i class="glyphicon glyphicon-floppy-saved"></i>Laporan<span class="glyphicon glyphicon-menu-down"></span></a>
									<ul class="nav-sub lapor">
									<li><a href="{{route('laporan-pulsa')}}"><i class="glyphicon glyphicon-phone"></i>Pulsa</a></li>                                             
									<li><a href="{{route('laporan-voucher-pln')}}"><i class="glyphicon glyphicon-flash"></i>Voucher PLN</a></li> 
									<li><a href="{{route('laporan-paket-data')}}"><i class="glyphicon glyphicon-modal-window"></i>Paket Data</a></li>    
									<li><a href="{{route('laporan-paket-sms')}}"><i class="glyphicon glyphicon-sound-dolby"></i>Paket SMS</a></li>    
									<li><a href="{{route('laporan-paket-telpon')}}"><i class="glyphicon glyphicon-earphone"></i>Paket Telpon</a></li>                                             
									<li><a href="{{route('laporan-game')}}"><i class="glyphicon glyphicon-knight"></i>Voucher Game</a></li>  
									<li><a href="{{route('laporan-e-money')}}"><i class="glyphicon glyphicon-asterisk"></i>E-Wallet</a></li>                                             
									</ul>			
									<div class="clearfix"> </div>
									<script>
										$( "li a.laporan" ).click(function() {
										$( "ul.lapor" ).slideToggle( 300, function() {
										});
										});
									</script> 
								</li>
								{{--@if(config('app.name') == 'FIXPAY')
									@if(Auth::user()->id == 1)
											<li><a href="{{route('toko-saya')}}" class="{{setActive(['toko-saya'])}}"><i class="glyphicon glyphicon-home"></i> Toko Saya </a></li>
									@endif
								@else
									@if(Auth::user()->id == 41 || Auth::user()->id == 1)
											<li><a href="{{route('toko-saya')}}" class="{{setActive(['toko-saya'])}}"><i class="glyphicon glyphicon-home"></i> Toko Saya </a></li>
									@endif
								@endif--}}
								<li><a href="#" class="menu ua {{setActive(['daftar-harga'])}}"><i class="glyphicon glyphicon-usd"></i>Harga<span class="glyphicon glyphicon-menu-down"></span></a>
									<ul class="nav-sub dua">
									<li><a href="{{route('daftar-harga','pulsa')}}"><i class="glyphicon glyphicon-phone"></i>Pulsa</a></li>                                             
									<li><a href="{{route('daftar-harga','Voucher PLN')}}"><i class="glyphicon glyphicon-flash"></i>Voucher PLN</a></li> 
									<li><a href="{{route('daftar-harga','Paket Data')}}"><i class="glyphicon glyphicon-modal-window"></i>Paket Data</a></li>
									<li><a href="{{route('daftar-harga','Voucher Game')}}"><i class="glyphicon glyphicon-knight"></i>Voucher Game</a></li>                                             
									<li><a href="{{route('daftar-harga','E-Wallet')}}"><i class="glyphicon glyphicon-asterisk"></i>E-Wallet</a></li>
									</ul>
									<div class="clearfix"> </div>
									<script>
										$( "li a.ua" ).click(function() {
										$( "ul.dua" ).slideToggle( 300, function() {
										});
										});
									</script> 
								</li>
								@if(Auth::user()->agen == 'yes')
								<li id="jadi_agen"><a href="{{route('tambah-untung')}}" class="{{setActive(['tambah-untung'])}}"><i class="glyphicon glyphicon-user"></i> Data Agen Kamu</a></li>
								<li><a href="#" class="menu ad_harga {{setActive(['harga_downline'])}}"><i class="glyphicon glyphicon-credit-card"></i>Harga Agen<span class="glyphicon glyphicon-menu-down"></span></a>
									<ul class="nav-sub ada_harga">
									<li><a href="{{route('harga_downline','pulsa')}}">Pulsa</a></li>  
									<li><a href="{{route('harga_downline','pln')}}">Token Listrik</a></li>
									<li><a href="{{route('harga_downline','game')}}">Voucher Game</a></li>
									<li><a href="{{route('harga_downline','paket-internet')}}">Paket Internet</a></li>
									<li><a href="{{route('harga_downline','paket-sms')}}">Paket SMS</a></li>
									<li><a href="{{route('harga_downline','paket-telpon')}}">Paket Telpon</a></li>
									<li><a href="{{route('harga_downline','saldo-OVO')}}">Saldo OVO</a></li>
									<li><a href="{{route('harga_downline','saldo-Gojek')}}">Saldo Gojek</a></li>
									<li><a href="{{route('harga_downline','saldo-Gojek-Driver')}}">Saldo Gojek Driver</a></li>
									<li><a href="{{route('harga_downline','saldo-Grab')}}">Saldo Grab</a></li>
									<li><a href="{{route('harga_downline','saldo-Grab-Driver')}}">Saldo Grab Driver</a></li>
									<li><a href="{{route('harga_downline','saldo-Linkaja')}}">Saldo Link Aja</a></li>
									<li><a href="{{route('harga_downline','saldo-Shopeepay')}}">Saldo ShopeePay</a></li>
									<li><a href="{{route('harga_downline','wifi-id')}}">Wifi ID</a></li>
									<li><a href="{{route('harga_downline','google-play')}}">Google Play</a></li>
									</ul>			
									<div class="clearfix"> </div>
									<script>
										$( "li a.ad_harga" ).click(function() {
											$( "ul.ada_harga" ).slideToggle( 300, function() {
											});
										});
									</script> 
									</li>
									@else
											<?php $trx = \App\HistoryTransaksi::where('user_id',Auth::user()->id)->get();?>
											@if(count($trx) > 50)
											<div id="jadi_agen">
											<li @click="daftarAgen()"><a><i class="glyphicon glyphicon-user"></i> Jadi Agen </a></li>
											</div>
											@endif
									@endif
