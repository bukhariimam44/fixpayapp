<li><a href="#" class="menu ad_depo {{setActive(['admin-permintaan-topup','admin-mutasi-saldo','admin-topup-manual'])}}"><i class="glyphicon glyphicon-credit-card"></i>Deposit<span class="glyphicon glyphicon-menu-down"></span></a>
<ul class="nav-sub ad_dipi">
<li><a href="{{route('admin-permintaan-topup')}}"><i class="glyphicon glyphicon-th-list"></i>Req Topup </a></li>                                            
<li><a href="{{route('admin-mutasi-saldo')}}"><i class="glyphicon glyphicon-book"></i>Mutasi Saldo </a></li>                                            
<li><a href="{{route('admin-topup-manual')}}"><i class="glyphicon glyphicon-book"></i>Topup Manual </a></li>                                            
</ul>			
<div class="clearfix"> </div>
<script>
	$( "li a.ad_depo" ).click(function() {
		$( "ul.ad_dipi" ).slideToggle( 300, function() {
		});
	});
</script> 
</li>
<li><a href="#" class="menu ad_harga {{setActive(['admin-data-harga'])}}"><i class="glyphicon glyphicon-credit-card"></i>Update Markup<span class="glyphicon glyphicon-menu-down"></span></a>
<ul class="nav-sub ada_harga">
<li><a href="{{route('admin-data-harga','pulsa')}}">Pulsa</a></li>  
<li><a href="{{route('admin-data-harga','pln')}}">Token Listrik</a></li>
<li><a href="{{route('admin-data-harga','game')}}">Voucher Game</a></li>
<li><a href="{{route('admin-data-harga','paket-internet')}}">Paket Internet</a></li>
<li><a href="{{route('admin-data-harga','paket-sms')}}">Paket SMS</a></li>
<li><a href="{{route('admin-data-harga','paket-telpon')}}">Paket Telpon</a></li>
<li><a href="{{route('admin-data-harga','saldo-OVO')}}">Saldo OVO</a></li>
<li><a href="{{route('admin-data-harga','saldo-Gojek')}}">Saldo Gojek</a></li>
<li><a href="{{route('admin-data-harga','saldo-Gojek-Driver')}}">Saldo Gojek Driver</a></li>
<li><a href="{{route('admin-data-harga','saldo-Grab')}}">Saldo Grab</a></li>
<li><a href="{{route('admin-data-harga','saldo-Grab-Driver')}}">Saldo Grab Driver</a></li>
<li><a href="{{route('admin-data-harga','saldo-Linkaja')}}">Saldo Link Aja</a></li>
<li><a href="{{route('admin-data-harga','saldo-Shopeepay')}}">Saldo ShopeePay</a></li>
<li><a href="{{route('admin-data-harga','wifi-id')}}">Wifi ID</a></li>
<li><a href="{{route('admin-data-harga','google-play')}}">Google Play</a></li>
</ul>			
<div class="clearfix"> </div>
<script>
	$( "li a.ad_harga" ).click(function() {
		$( "ul.ada_harga" ).slideToggle( 300, function() {
		});
	});
</script> 
</li>
<li><a href="{{route('admin-laporan-ppob')}}"><i class="glyphicon glyphicon-floppy-saved"></i> Laporan Ppob </a></li>
<li><a href="#" class="menu su_depo {{setActive(['admin-permintaan-topup','admin-mutasi-saldo','admin-topup-manual'])}}"><i class="glyphicon glyphicon-credit-card"></i>Supplier<span class="glyphicon glyphicon-menu-down"></span></a>
<ul class="nav-sub su_dipi">
<li><a href="{{route('admin-topup-supplier')}}"><i class="glyphicon glyphicon-plus"></i> Topup Supplier </a></li>                                            
<li><a href="{{route('admin-mutasi-saldo-supplier')}}" class="{{setActive(['admin-mutasi-saldo-supplier'])}}"><i class="glyphicon glyphicon-transfer"></i>Mutasi Saldo </a></li>                                            
<!-- <li><a href="{{route('admin-topup-manual')}}"><i class="glyphicon glyphicon-book"></i>Data Transaksi </a></li>                                             -->
</ul>			
<div class="clearfix"> </div>
<script>
	$( "li a.su_depo" ).click(function() {
		$( "ul.su_dipi" ).slideToggle( 300, function() {
		});
	});
</script> 
</li>
<li><a href="{{route('admin-data-user')}}" class="{{setActive(['admin-data-user'])}}"><i class="glyphicon glyphicon-user"></i> Data User </a></li>
