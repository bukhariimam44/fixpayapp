@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Transaksi Paket Data</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;" id="transaksi_pulsa">
				<div class="col-md-12">
							<div class="form-group">
								<label for="">Provider</label>
								<select v-model="selected" class="form-control" @change="load()">
												<option v-for="option in options" v-bind:value="option.value">
																@{{ option.text }}
												</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Nominal</label>
								<select v-model="selected_2" class="form-control" @change="check()">
												<option v-for="nominal in nominals" v-bind:value="nominal.code">
																@{{ nominal.description }} = Rp @{{ formatPrice(nominal.price+nominal.markup) }}
												</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Nomor HP</label>
								<input type="number" v-model="no_hp" name="no_hp" class="form-control">
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Panduan :</h4><br>
						<ol>
							<li>Pastikan Saldo kamu cukup.</li>
							<li>Pilih Provider sesuai Kartu kamu.</li>
							<li>Pilih Nominal</li>
							<li>Masukan Nomor HP</li>
							<li>Klik Proses</li>
							<li>Paket Data akan otomatis masuk kurang dari 1 Menit</li>
						</ol>
				</div>
			</div>
</div>
@endsection
@section('js')
<script>
    var transaksi_pulsa = new Vue({
								el:'#transaksi_pulsa',
        data:{
            selected: 'TELKOMSEL',
            options: [
                { text: 'Telkomsel', value: 'TELKOMSEL' },
                { text: 'Indosat', value: 'INDOSAT' },
                { text: 'XL', value: 'XL' },
                { text: 'Smartfren', value: 'SMARTFREN' },
                { text: 'Tree', value: 'TRI' },
                { text: 'Axis', value: 'AXIS' }
            ],
            selected_2 :'',
            nominals :[],
            no_hp:'',
        },
        mounted() {
            this.load()
        },
        methods:{
            formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                let request = {provider : this.selected};
                request['token'] = document.querySelector('#token').getAttribute('value');
                // console.log('LOG :'+JSON.stringify(request));
                // console.log('LOG URL:'+window.url_operator);
                await axios.post(window.url_provider,request).then((response) =>{
                    // console.log('Berhasil'+JSON.stringify(response.data));
                    this.nominals = response.data.data;
                    this.selected_2 = response.data.data[0].code;
                    // this.check()
                },(response)=>{
                    // console.log('ERROR: '+response);
                });
            },
            async check(){
                let request = {code : this.selected_2};
                request['token'] = document.querySelector('#token').getAttribute('value');
                // console.log('LOG :'+JSON.stringify(request));
                // console.log('LOG URL:'+window.url_operator);
                await axios.post(window.url_operator,request).then((response) =>{
                    // console.log('Berhasil check'+JSON.stringify(response.data));
                },(response)=>{
                    // console.log('ERROR: '+response);
                });
            },
           async checkForm() {
												//Loading
														Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.selected && this.selected_2 && this.no_hp) {
                let request = {code : this.selected_2,no_hp : this.no_hp};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    // console.log('Berhasil checkform'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
                      swal({
                        title: "Transaksi Berhasil",
                        text: "Apakah ingin transaksi lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
																										Swal.close()
                        } else {
                          window.location.href = "<?php echo route('laporan-paket-data'); ?>";
                        }
                      });
                    }else{
																					
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    // console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Provider harus dipilih!", "error");
																Swal.close()
              }
              if (!this.selected_2) {
                swal("Cek Kembali!", "Nominal harus dipilih!", "error");
																Swal.close()
              }
              if (this.no_hp.length < 10) {
                swal("Cek Kembali!", "Nomor HP Minimal 10 angka!", "error");
																Swal.close()
              }
            }

        }
    });
</script>
@endsection