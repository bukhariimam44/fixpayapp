@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Lupa Password</h3>   
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12">
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
													@if (session('status'))
																	<div class="alert alert-success" role="alert">
																					{{ session('status') }}
																	</div>
													@endif
													<form action="{{ route('password.email') }}" method="post" id="kirim">
													@csrf
														<h5 style="font-size:14;color:black;">Masukan Email</h5>
														<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
														<br>
														<button class="btn btn-success" id="btnKirim" onclick="kirimLink()">Kirim Tautan Setel Ulang Kata Sandi</button>
													</form>
												</div>
										</div>
							</div>
					</div>
			</div>
</div>
@endsection
@section('js')
<script>
function kirimLink() {
	//Loading
	Swal.fire({
								title: 'Mohon menunggu...',
								text:'Sedang Diproses..',
								allowEscapeKey: false,
								allowOutsideClick: false,
								background: '#FFFFFF',
								showConfirmButton: false,
								onOpen: ()=>{
												Swal.showLoading();
								}
				}).then((dismiss) => {
					// Swal.showLoading();
					}
			);
			//END LOADING
			document.getElementById("btnKirim").style.display="disabled";
			document.getElementById('kirim').submit();
}
</script>
@endsection