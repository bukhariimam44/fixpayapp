@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Reset Password</h3>   
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12">
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
													@if (session('status'))
																	<div class="alert alert-success" role="alert">
																					{{ session('status') }}
																	</div>
													@endif
													<form action="{{ route('password.update') }}" method="post" id="resetp">
													<input type="hidden" name="token" value="{{ $token }}">
													@csrf
													<div class="form-group">
														<h5 style="font-size:14;color:black;">Masukan Email</h5>
														<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
														@error('email')
																		<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																		</span>
														@enderror
														</div>
														<div class="form-group">
														<h5 style="font-size:14;color:black;">Masukan Password Baru</h5>
														<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="new-password" autofocus>
														@error('password')
																		<span class="invalid-feedback" role="alert">
																						<strong>{{ $message }}</strong>
																		</span>
														@enderror
														</div>
														<div class="form-group">
														<h5 style="font-size:14;color:black;">Ulangi Password Baru</h5>
														<input id="password-confirm" type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" required autocomplete="new-password" autofocus>
														</div>
														</form>
														<br>
														<button class="btn btn-success" id="resetPassword" onclick="resetPassword()">Reset Password</button>
													
												</div>
										</div>
							</div>
					</div>
			</div>
</div>
@endsection
@section('js')
<script>
function resetPassword() {
	//Loading
	Swal.fire({
								title: 'Mohon menunggu...',
								text:'Sedang Diproses..',
								allowEscapeKey: false,
								allowOutsideClick: false,
								background: '#FFFFFF',
								showConfirmButton: false,
								onOpen: ()=>{
												Swal.showLoading();
								}
				}).then((dismiss) => {
					// Swal.showLoading();
					}
			);
			//END LOADING
			document.getElementById("resetPassword").style.display="disabled";
			document.getElementById('resetp').submit();
}
</script>
@endsection