@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<meta name="appname" id="appname" value="{{config('app.name')}}">
<?php $agent = new \Jenssegers\Agent\Agent();
									$browser = $agent->platform();
									$sss = \Cookie::get('kode_fixpay');
									?>
									@if(isset($sss))
									<meta name="website" id="website" value="true">
									@else
									<meta name="website" id="website" value="false">
									@endif
@endsection
@section('content') 
<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Verifikasi Email</h3>   
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12">
					<div class="w3agile properties">
					<div class="properties-bottom">
                <!-- <div class="card-header">{{ __('Verifikasi Email Anda') }}.</div> -->
                <div class="w3ls-text" id="verifikasi_email">
																				<h6 style="color:#11909e;">Masukan Kode Verifikasi yang {{config('app.name')}} kirim ke Email kamu.</h6>
																			<div class="row">
																			<div class="form-group col-md-12">
																			<input type="text" v-model="kode" placeholder="Kode ..." class="form-control">
																			</div>
																			<div class="form-group col-md-12">
																			<button class="btn btn-success" @click="kirimKode()">Proses Verifikasi</button>
																			</div>
																			</div>
																			<hr>
																			<h6 style="color:#11909e;">Jika kamu tidak menerima email dari {{config('app.name')}}, Klik tombol Tidak Menerima Email</h6>

																				<form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
																								@csrf
																				</form>
																								<br>
																								<button class="btn btn-warning" @click="kirimKodeBaru()">Tidak Menerima Email</button>
																				
																				<hr>
																				<h6 style="color:#11909e;">Jika Email kamu salah atau ingin mengganti email, masukan Email baru di bawah.</h6>
																			<div class="row">
																			<div class="form-group col-md-12">
																			<input type="text"  placeholder="Email ..." v-model="email" class="form-control">
																			</div>
																			<div class="form-group col-md-12">
																			<button class="btn btn-primary" @click="simpanEmailBaru()">Simpan Email Baru</button>
																			</div>
																			</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
	var kirim_kode = new Vue({
		el:'#verifikasi_email',
		data:{
			kode:'',
			email:'',
			error:[],
			appname:document.querySelector('#appname').getAttribute('value'),
			website:document.querySelector('#website').getAttribute('value'),
		},
		mounted() {
			if (this.website == 'true') {
				this.load();
			}
		},
		methods: {
			load(){
				swal({
							title: "INFO !",
							text: "Install Aplikasi "+this.appname+" untuk kenyamanan dan kemudahan dalam bertransaksi.",
							icon: "warning",
							buttons: ['Instal Sekarang','Nanti'],
							dangerMode: true,
							allowEscapeKey: false,
							allowOutsideClick: false,
					})
					.then((willDelete) => {
							if (willDelete) {
									swal("Baik Terimakasih, Klik menu Install Aplikasi jika ingin menggunakan Aplikasi "+this.appname+".", {
											icon: "success",
									});
									Swal.close()
							} else {
								if (this.appname == 'FIXPAY') {
									window.location.href = "<?php echo route('download-aplikasi');?>";
								}else if(this.appname == 'WARUNG IMBUH PAY'){
									window.location.href = "https://play.google.com/store/apps/details?id=com.wip.warungimbuhpay";
								}else{
									// swal("Terimakasih, Silahkan", {
									// 		icon: "success",
									// });
									Swal.close()
								}
							}
					});
			},
			async kirimKode(){
				//Loading
				Swal.fire({
									title: 'Mohon menunggu...',
									text:'Verifikasi Sedang Diproses..',
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
				//END LOADING
				if (this.kode) {
						let url_kirim_kode = "<?php echo route('kirim-kode');?>";
						let request = {kode : this.kode};
						request['token'] = document.querySelector('#token').getAttribute('value');
						// console.log('LOG :'+JSON.stringify(request));
						await axios.post(url_kirim_kode,request).then((response) =>{
										// console.log('Berhasil Verifikasi '+JSON.stringify(response.data));
										if (response.data.code === 200) {
											swal(response.data.title, response.data.message, "success");
											Swal.close()
											window.location.href = "<?php echo route('index'); ?>";
										}else{
											swal(response.data.title, response.data.message, "error");
											Swal.close()
										}
						},(response)=>{
										swal('Gagal Verifikasi', 'Silahkan ulangi kembali', "error");
										Swal.close()
						});
				}
				this.errors = [];
				if (this.kode.length < 6) {
						swal("Cek Kembali!", "Kode Minimal 6 karakter", "error");
						Swal.close()
				}

			},
			async kirimKodeBaru(){
					//Loading
					Swal.fire({
									title: 'Mohon menunggu...',
									text:'Permintaan Sedang Diproses..',
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
				//END LOADING
				
				let url_kirim_ulang_kode = "<?php echo route('kirim-ulang-kode');?>";
				// request['token'] = document.querySelector('#token').getAttribute('value');
				let request = {token : document.querySelector('#token').getAttribute('value')};
				// console.log('LOG :'+JSON.stringify(request));
				await axios.post(url_kirim_ulang_kode,request).then((response) =>{
								// console.log('Berhasil Verifikasi '+JSON.stringify(response.data));
								if (response.data.code === 200) {
									swal(response.data.title, response.data.message, "success");
									Swal.close()
								}else{
									swal(response.data.title, response.data.message, "error");
									Swal.close()
								}
				},(response)=>{
								swal('Gagal Verifikasi', 'Silahkan ulangi kembali', "error");
								Swal.close()
				});
			},
			async simpanEmailBaru(){
					//Loading
					Swal.fire({
									title: 'Mohon menunggu...',
									text:'Permintaan Sedang Diproses..',
									allowEscapeKey: false,
									allowOutsideClick: false,
									background: '#FFFFFF',
									showConfirmButton: false,
									onOpen: ()=>{
													Swal.showLoading();
									}
					}).then((dismiss) => {
						// Swal.showLoading();
						}
				);
				//END LOADING
				if (this.email) {
						let url_ganti_email = "<?php echo route('ganti-email');?>";
						let request = {email : this.email};
						request['token'] = document.querySelector('#token').getAttribute('value');
						await axios.post(url_ganti_email,request).then((response) =>{
							// console.log('Respon ganti email '+JSON.stringify(response.data));
										if (response.data.code === 200) {
											swal(response.data.title, response.data.message, "success");
											Swal.close()
											window.location.href = "<?php echo url('/email/verify'); ?>";
										}else{
											swal(response.data.title, response.data.message, "error");
											Swal.close()
										}
						},(response)=>{
										swal('Gagal Verifikasi', 'Silahkan ulangi kembali', "error");
										Swal.close()
						});
				}
				this.errors = [];
				if (this.email.length < 7) {
						swal("Cek Kembali!", "Email Minimal 7 karakter", "error");
						Swal.close()
				}
			}
		},
	});
</script>
@endsection