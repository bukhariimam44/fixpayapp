<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>{{config('app.name')}}</title> 
<!-- For-Mobile-Apps-and-Meta-Tags -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="{{config('app.name')}}" />
<meta name="_token" id="token" value="{{csrf_token()}}">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //For-Mobile-Apps-and-Meta-Tags -->
<!-- Custom Theme files -->
<link href="{{asset('css/bootstrap.css')}}" type="text/css" rel="stylesheet" media="all">
<link href="{{asset('css/style.css')}}" type="text/css" rel="stylesheet" media="all"> 
<link rel="stylesheet" href="{{asset('css/ken-burns.css')}}" type="text/css" media="all" /> 
<!-- //Custom Theme files -->
<!-- js -->
<script src="{{asset('js/jquery-2.2.3.min.js')}}"></script> 
<!-- //js -->
<!-- pop-up-box -->
<script src="{{asset('js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
	    <script>
			$(document).ready(function() {
				$('.popup-top-anim').magnificPopup({
					type: 'inline',
					fixedContentPos: false,
					fixedBgPos: true,
					overflowY: 'auto',
					closeBtnInside: true,
					preloader: false,
					midClick: true,
					removalDelay: 300,
					mainClass: 'my-mfp-zoom-in'
				});																							
			}); 
		</script>
<!--//pop-up-box -->
<!-- web-fonts -->  
<link href='//fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- //web-fonts -->
@yield('css')
</head>
<body class="bg">
	<div class="agile-main"> 
		<div class="menu-wrap" id="style-1"> 
			<nav class="top-nav">
				<ul class="icon-list">
					<li class="menu-title">
					@if(config('app.name') == 'FIXPAY') 
					<img src="{{asset('images/logo.png')}}" width="120px" alt=""> 
					@elseif(config('app.name') == 'WARUNG IMBUH PAY')
					<img src="{{asset('images/wip_logo.png')}}" width="120px" alt=""> 
					@elseif(config('app.name') == 'WELIMENA')
					<img src="{{asset('images/welimena.png')}}" width="120px" alt=""> 
					@endif
					
					</li>
					<li><a class="{{setActive(['index'])}}" href="{{route('index')}}"><i class="glyphicon glyphicon-dashboard"></i>Beranda </a></li>
					@if(Auth::check())
								@if(Auth::user()->type == 'user')
									@include('includes.menu_user')
								@elseif(Auth::user()->type == 'admin')
								@include('includes.menu_admin')
								@endif

						@else
						<li><a href="#small-dialog" class="sign-in popup-top-anim"><i class="glyphicon glyphicon-log-in"></i> Login </a></li>
						<li><a href="#small-dialog1" class="sign-in popup-top-anim"><i class="glyphicon glyphicon-user"></i> Daftar </a></li>
						@endif
					@if(config('app.name') == 'WARUNG IMBUH PAY')
					<li><a href="{{route('channels')}}"><i class="glyphicon glyphicon-facetime-video"></i>Tutorial & Channel </a></li>
					@endif
					<li><a href="{{route('kontak')}}"><i class="glyphicon glyphicon-envelope"></i> Kontak </a></li>
					@if(Auth::check())
					<?php $agent = new \Jenssegers\Agent\Agent();
									$browser = $agent->platform();
									$sss = \Cookie::get('kode_fixpay');
									?>
									@if(isset($sss))
										@if($browser == 'AndroidOS')
											@if(config('app.name')=='FIXPAY' || config('app.name')=='WELIMENA' )
											<li><a href="{{route('download-aplikasi')}}" target="_blank"><i class="glyphicon glyphicon-download"></i> Install Aplikasi </a></li>
											@elseif(config('app.name') == 'WARUNG IMBUH PAY')
											<li><a href="https://play.google.com/store/apps/details?id=com.wip.warungimbuhpay" target="_blank"><i class="glyphicon glyphicon-download"></i> Install Aplikasi </a></li>
											@endif
										@endif
									@endif
					@endif
				</ul>
			</nav>
			<button class="close-button" id="close-button">C</button>
		</div> 
		<div class="content-wrap">
			<div class="header" style="position: fixed;z-index: 1000;width:100%;"> 
				<div class="menu-icon">   
					<button class="menu-button" id="open-button">O</button>
				</div>
				<div class="logo">
					<h2><a href="#">
					@if(config('app.name') == 'WARUNG IMBUH PAY') 
					WIP 
					@else
					{{config('app.name')}} 
					@endif</a></h2>
					
				</div>
				<div class="login">
					@if(Auth::check())
					<a href="#small-dialog-akun" class="sign-in popup-top-anim"><span class="glyphicon glyphicon-user"></span></a> 
					<!-- modal -->
					<div id="small-dialog-akun" class="mfp-hide">
						
						<div class="login-modal">

							<div class="booking-info">
							@if(config('app.name') == 'FIXPAY')
							<h3><img src="{{asset('images/logo.png')}}" width="150px" alt=""></h3>
							@elseif(config('app.name') == 'WARUNG IMBUH PAY')
							<h3><img src="{{asset('images/wip_logo.png')}}" width="150px" alt=""></h3>
							@elseif(config('app.name') == 'WELIMENA')
							<h3><img src="{{asset('images/welimena.png')}}" width="150px" alt=""></h3>
							@endif
							<br><br><br>
							</div>
							<div class="login-form">
								
								<div class="form-group">
									<div class="col-md-12">
										<label for="">Nama</label><br>
									{{Auth::user()->name}}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="">Email</label><br>
									{{Auth::user()->email}}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<label for="">Saldo</label><br>
									Rp {{number_format(Auth::user()->saldo)}}
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12">
										<button class="btn btn-danger" id="btnLogout" onclick="myFunctionKeluar()">Keluar</button> <a href="{{route('edit-password')}}" class="btn btn-warning">Edit Password</a>
									</div>
									<form action="{{route('logout')}}" method="post" id="keluar">
									@csrf
									</form>
								</div>
							</div> 
						</div>
					</div>
					
					<script type="text/javascript">
					
						function myFunctionKeluar() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Logout Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("btnLogout").style.display="disabled";
							document.getElementById('keluar').submit();
						}
					</script>
					<!-- //modal --> 
					@else
					<a href="#small-dialog" class="sign-in popup-top-anim"><span class="glyphicon glyphicon-user"></span></a> 
					@endif
					<!-- modal -->
					<link href="{{ captcha_layout_stylesheet_url() }}" type="text/css" rel="stylesheet">
					<div id="small-dialog" class="mfp-hide">
						<div class="login-modal">
							<div class="booking-info">
							@if(config('app.name') == 'FIXPAY')
							<h3><img src="{{asset('images/logo.png')}}" width="150px" alt=""></h3>
							@elseif(config('app.name') == 'WARUNG IMBUH PAY')
							<h3><img src="{{asset('images/wip_logo.png')}}" width="150px" alt=""></h3>
							@elseif(config('app.name') == 'WELIMENA')
							<h3><img src="{{asset('images/welimena.png')}}" width="150px" alt=""></h3>
							@endif
							</div>
							<div class="login-form">
								<form action="{{route('masuk')}}" method="post" id="post_login">
								@csrf
									<div class="styled-input">
										<input type="text" name="username" required=""/>
										<label>Username</label>
										<span></span>
									</div>
									<div class="styled-input">
										<input type="password" name="password" required=""> 
										<label>Kata Sandi</label>
										<span></span>
									</div> 
									<div class="styled-input">
									{!! captcha_image_html('ExampleCaptcha') !!}
									</div>
									<div class="styled-input">
									<input type="text" id="CaptchaCode" name="CaptchaCode" required="">
										<label>Kode Captcha</label>
										<span></span>
									</div> 
									<div class="wthree-text"> 
										<ul> 
											<li class="pull-left">
												<input type="checkbox" id="brand" name="remember" value="1">
												<label for="brand"><span></span> Buat tetap login</label>  
											</li>
											<li> 
												<a href="{{route('password.request')}}">Lupa Kata Sandi?</a> </li>
										</ul>
										<div class="clear"> </div>
									</div> 
									<input type="button" id="btnLogin" onclick="myFunctionLogin()" class="btn btn-success form-control" value="Login">	
								</form>
								<p>Belum punya akun ? <a href="#small-dialog1" class="sign-in popup-top-anim"> DAFTAR</a></p>
							</div> 
						</div>
					</div>
					<script type="text/javascript">
					
						function myFunctionLogin() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Login Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("btnLogin").style.display="disabled";
							document.getElementById('post_login').submit();
						}
					</script>
					<script>
					$(document).ready(function(){
									$("a[title ~= 'BotDetect']").removeAttr("style");
									$("a[title ~= 'BotDetect']").removeAttr("href");
									$("a[title ~= 'BotDetect']").css('visibility', 'hidden');

					});
					</script>
					<!-- //modal --> 

					<!-- modal-two -->
					<div id="small-dialog1" class="mfp-hide">
						<div class="login-modal">  
							<div class="booking-info">
							@if(config('app.name') == 'FIXPAY')
							<h3><img src="{{asset('images/logo.png')}}" width="150px" alt=""></h3>
							@elseif(config('app.name') == 'WARUNG IMBUH PAY')
							<h3><img src="{{asset('images/wip_logo.png')}}" width="150px" alt=""></h3>
							@elseif(config('app.name') == 'WELIMENA')
							<h3><img src="{{asset('images/welimena.png')}}" width="150px" alt=""></h3>
							@endif
							</div>
							<div class="login-form signup-form" id="daftar">
									<div class="styled-input">
										<input type="text" v-model="nama" required=""/>
										<label>Nama Lengkap</label>
										<span></span>
									</div>
									<div class="styled-input">
										<input type="text" v-model="username" @keydown.space.prevent required=""/>
										<label>Username</label>
										<span></span>
									</div>
									<div class="styled-input">
										<input type="text" v-model="email" @keydown.space.prevent name="email" required=""/>
										<label>Email</label>
										<span></span>
									</div>
									<div class="styled-input">
										<input type="password" v-model="password" name="Password" @keydown.space.prevent required=""> 
										<label>Kata Sandi</label>
										<span></span>
									</div>  
									<div class="wthree-text"> 
										<input type="checkbox" id="brand1" v-model="setuju">
										<label for="brand1"><span></span>Saya menerima ketentuan penggunaan.</label> 
									</div>
									<input type="submit" @click="daftar()" value="DAFTAR">		
							</div> 
						</div>
					</div>
					<!-- //modal-two --> 
				</div> 
				<div class="clearfix"> </div>
				@if(config('app.name') == 'WARUNG IMBUH PAY')
				<marquee behavior="" direction="" style="color:#FFFFFF;font-size:7px;">Bantu donasikan Buat kepedulian anak yatim transaksi dengan WIP (Warung Imbuh Pay)</marquee>
				@endif
			
			</div>

			<div class="content" style="margin-top:60px;">
			@include('flash::message')

			@yield('content')
				<!-- footer -->
				<div class="w3agile footer"> 
					<div class="footer-text">
							@if(config('app.name') == 'WARUNG IMBUH PAY')
							<p>&copy; 2020 WIP . All Rights Reserved By Imbuh Ruwadi</p>
							@else 
							<p>&copy; 2020 {{config('app.name')}} . All Rights Reserved</p>
							@endif
						
					</div>
				</div>
		</div>
	</div> 
	</div> 
	<!-- menu-js -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
{{--<script>
			var login = new Vue({
				el:'#login',
				data: {
					username:'',
					password:'',
					remember:false,
					error:[]
				},
				methods: {
					async login(){
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Login...',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
													//END LOADING
													//FALIDASI
							this.errors = [];
							if (this.username.length < 6) {
									swal("Cek Kembali!", "Username Minimal 6 karakter", "error");
									Swal.close()
							}
							else if (this.password.length < 6) {
									swal("Cek Kembali!", "Kata Sandi Minimal 6 Digit", "error");
									Swal.close()
							}
							//END FALIDASI
							if (this.username && this.password) {
								let url_login = "<?php echo route('masuk');?>";
								let request = {username : this.username, password: this.password,remember:this.remember};
								request['token'] = document.querySelector('#token').getAttribute('value');
								await axios.post(url_login,request).then((response) =>{
												console.log('Res Login'+JSON.stringify(response.data));
												if (response.data.code === 200) {
													swal(response.data.title, response.data.message, "success");
													Swal.close()
													window.location.href = "<?php echo route('index'); ?>";
												}else{
														swal(response.data.title, response.data.message, "error");
														Swal.close()
												}
								},(response)=>{
												// console.log('ERROR: '+response);
												swal("Gagal!", response, "error");
												Swal.close()
								}); 
							}
							
					}
				},
			});
</script>--}}
<script>
    var daftar = new Vue({
								el:'#daftar',
        data:{
									nama:'',
									username:'',
									email:'',
									password:'',
									setuju:'',
									reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
									error:[]
								},
								methods: {
									async daftar(){
										//Loading
										Swal.fire({
																		title: 'Mohon menunggu...',
																		text:'Pendaftaran Sedang Diproses..',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
													this.errors = [];
													if (this.nama.length < 3) {
															swal("Cek Kembali!", "Nama Minimal 3 karakter", "error");
															Swal.close()
													}
													else if (this.username.length < 6) {
															swal("Cek Kembali!", "Username Minimal 6 karakter", "error");
															Swal.close()
													}
													else if (this.email.length < 7) {
															swal("Cek Kembali!", "Email Minimal 7 karakter", "error");
															Swal.close()
													}
												 else if(!this.reg.test(this.email)){
															swal("Cek Kembali!", "Email tidak falid", "error");
															Swal.close()
													}
													else if (this.password.length < 6) {
															swal("Cek Kembali!", "Kata Sandi Minimal 6 Digit", "error");
															Swal.close()
													}
													else if (!this.setuju) {
															swal("Cek Kembali!", "Checklish Ketentuan Pengguna", "error");
															Swal.close()
													}else{
													if (this.username && this.nama && this.email && this.password  && this.setuju) {
														let url_daftar = "<?php echo route('daftar');?>";
														let request = {name : this.nama, username : this.username, email : this.email, password: this.password};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(url_daftar,request).then((response) =>{
                    console.log('Res daftar'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
																					swal(response.data.title, response.data.message, "success");
																					Swal.close()
																					window.location.href = "<?php echo url('/email/verify'); ?>";
                    }else{
                      swal(response.data.title, response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    // console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                }); 
													}
												}
									}
								},
				});
	</script>
	<script src="{{asset('js/classie.js')}}"></script>
	<script src="{{asset('js/main.js')}}"></script>
	<!-- //menu-js -->
	<!-- nice scroll-js -->
	<!-- <script src="{{asset('js/jquery.nicescroll.min.js')}}"></script>  -->
	<!-- <script>
		$(document).ready(function() {
			var nice = $("html").niceScroll();  // The document page (body)
			$("#div1").html($("#div1").html()+' '+nice.version);
			$("#boxscroll").niceScroll({cursorborder:"",cursorcolor:"#00F",boxzoom:true}); // First scrollable DIV
		});
	</script> -->
	<!-- //nice scroll-js -->
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{asset('js/bootstrap.js')}}"></script>
				@yield('js')
				<script>
									var jadi_agen = new Vue({
										el:"#jadi_agen",
										data:{
											url_jadi_agen:"<?php echo route('jadi-agen');?>",
											url_proses_agen:"<?php echo route('proses_jadi_agen');?>",
										},
										methods: {
											async daftarAgen(){
												//Loading
												Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
												await axios.get(this.url_jadi_agen).then((response) =>{
																console.log('Berhasil'+JSON.stringify(response.data));
																if (response.data.code === 200) {
																	Swal.fire({
																			title: response.data.title,
																			text: response.data.message,
																			allowEscapeKey: false,
																			allowOutsideClick: false,
																			icon: 'warning',
																			showCancelButton: true,
																			confirmButtonColor: '#3085d6',
																			cancelButtonColor: '#d33',
																			confirmButtonText: response.data.textBtnYa,
																			cancelButtonText: response.data.textBtnTidak
																	}).then((result) => {
																			if (result.value) {
																					this.lanjutkanDaftar();
																			}
																	})
																}
												},(response)=>{
																console.log('ERROR: '+response);
												});
											},
											async lanjutkanDaftar(){
												//Loading
												Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
												await axios.get(this.url_proses_agen).then((response) =>{
																console.log('Berhasil'+JSON.stringify(response.data));
																if (response.data.code === 401) {
																	Swal.fire({
																			title: response.data.title,
																			text: response.data.message,
																			allowEscapeKey: false,
																			allowOutsideClick: false,
																			icon: 'warning',
																			showCancelButton: true,
																			confirmButtonColor: '#3085d6',
																			cancelButtonColor: '#d33',
																			confirmButtonText: response.data.textBtnYa,
																			cancelButtonText: response.data.textBtnTidak
																	}).then((result) => {
																			if (result.value) {
																				window.location.href = "<?php echo route('tambah-saldo'); ?>";
																			}
																	})
																}else if(response.data.code === 200){
																	Swal.fire({
																			title: response.data.title,
																			text: response.data.message,
																			allowEscapeKey: false,
																			allowOutsideClick: false,
																			icon: 'success',
																			showCancelButton: false,
																			confirmButtonColor: '#3085d6',
																			cancelButtonColor: '#d33',
																			confirmButtonText: response.data.textBtnYa,
																			cancelButtonText: response.data.textBtnTidak
																	}).then((result) => {
																			if (result.value) {
																				window.location.href = "<?php echo route('tambah-untung'); ?>";
																			}
																	})
																}else{
																	swal("Transaksi Gagal!", response.data.message, "error");
																	Swal.close()
																}
												},(response)=>{
																console.log('ERROR: '+response);
												});
											}
										},
									});
									</script>
</body>
</html>