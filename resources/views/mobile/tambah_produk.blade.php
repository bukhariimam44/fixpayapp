@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
   .uploading-image{
     display:flex;
					width:200px;
					height:200px;
   }
 </style>
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Tambah Produk</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;" id="tambah_produk">
				<div class="col-md-12">
							<div class="form-group">
								<label for="">Kategori</label>
								<select v-model="selected" class="form-control">
											<option v-for="kategori in kategoris" v-bind:value="kategori.id">
															@{{ kategori.kategori }}
											</option>
									</select>
							</div>
							<div class="form-group">
									<label for="">Nama Barang</label>
									<input v-model="nama" class="form-control"></input>
							</div>
							<div class="form-group">
									<label for="">Berat Barang (Gram)</label>
									<money v-model="berat" v-bind="beratan" class="form-control"></money>
							</div>
							<div class="form-group">
									<label for="">Stok Barang</label>
									<money v-model="stok" v-bind="stokan" class="form-control"></money>
							</div>
							<div class="form-group">
									<label for="">Harga</label>
									<money v-model="harga" v-bind="money" class="form-control"></money>
							</div>
							<div class="form-group">
									<label for="">Gambar Depan (sama sisi)</label>
									<img v-if="gambardepan" :src="gambardepan" class="uploading-image" />
      			<input type="file" accept="image/jpeg" @change=uploadImageDepan class="form-control">
							</div>
							<div class="form-group">
									<label for="">Gambar Samping (sama sisi)</label>
									<img v-if="gambarsamping" :src="gambarsamping" class="uploading-image" />
      			<input type="file" accept="image/jpeg" @change=uploadImageSamping class="form-control">
							</div>
							<div class="form-group">
									<label for="">Gambar Belakang (sama sisi)</label>
									<img v-if="gambarbelakang" :src="gambarbelakang" class="uploading-image" />
      			<input type="file" accept="image/jpeg" @change=uploadImageBelakang class="form-control">
							</div>
							<div class="form-group">
									<label for="">Penjelasan</label>
									<textarea v-model="penjelasan" class="form-control" placeholder="Penjelasan lengkap tentang produk yang dijual."></textarea>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Panduan :</h4><br>
						<ol>
							<li>Pilih Kategori Barang</li>
						</ol>
						<!-- <p> <strong style="color:red;">INGAT !</strong>  Jangan melakukan transfer sebelum perintah transfernya muncul (Setelah klik PROSES) dan Jumlah yang ditransfer harus sesuai yang ditampilan di aplikasi.</p> -->
				</div>
			</div>
</div>
@endsection
@section('js')
<script src="https://rawgit.com/vuejs-tips/v-money/master/dist/v-money.js"></script>

<script>
    var data_product = new Vue({
        el:'#tambah_produk',
        data:{
												selected: '',
            kategoris: [],
												nama:'',
												berat:'',
												stok:'',
												harga:'',
												penjelasan:null,
												money: {
														decimal: ',',
														thousands: '.',
														suffix: ' Rupiah',
														precision: 0,
														masked: false
												},
												beratan: {
														decimal: ',',
														thousands: '.',
														suffix: ' Gram',
														precision: 0,
														masked: false
												},
												stokan: {
														decimal: ',',
														thousands: '.',
														suffix: ' Pcs',
														precision: 0,
														masked: false
												},
												gambardepan:null,
												gambarsamping:null,
												gambarbelakang:null,
        },
        mounted() {
            this.load()
								},
        methods:{
												uploadImageDepan(e){
                const image = e.target.files[0];
                const reader = new FileReader();
                reader.readAsDataURL(image);
                reader.onload = e =>{
                    this.gambardepan = e.target.result;
                    // console.log(this.gambardepan);
                };
            },
												uploadImageSamping(e){
                const image = e.target.files[0];
                const reader = new FileReader();
                reader.readAsDataURL(image);
                reader.onload = e =>{
                    this.gambarsamping = e.target.result;
                    // console.log(this.gambarsamping);
                };
            },
												uploadImageBelakang(e){
                const image = e.target.files[0];
                const reader = new FileReader();
                reader.readAsDataURL(image);
                reader.onload = e =>{
                    this.gambarbelakang = e.target.result;
                    // console.log(this.gambarbelakang);
                };
            },
            formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                await axios.get(window.url_kategori).then((response) =>{
                    // console.log('Berhasil'+JSON.stringify(response.data));
																				this.kategoris = response.data;
                    this.selected = response.data[0].id;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
												//Loading
												Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.selected && this.nama && this.berat && this.stok && this.harga && this.penjelasan && this.gambardepan && this.gambarsamping && this.gambarbelakang) {
                let request = {kategori_id : this.selected, nama : this.nama, berat : this.berat, stok : this.stok, harga : this.harga, penjelasan : this.penjelasan, gambardepan : this.gambardepan, gambarsamping : this.gambarsamping, gambarbelakang : this.gambarbelakang};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    
                    if (response.data.code === 200) {
																					console.log('Berhasil proses 200'+JSON.stringify(response.data));
																					this.load()
																					swal({
                        title: response.data.message,
                        text: "Apakah ingin tambah produk lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
																										Swal.close()
                        } else {
                          window.location.href = "<?php echo route('detail-toko-saya',$sss = \Cookie::get('toko')); ?>";
                        }
                      });
                    }else{
																					console.log('Berhasil proses 400'+JSON.stringify(response.data));
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Gagal !", "Kategory harus dipilih!", "error");
																Swal.close()
              }else if(!this.nama){
															swal("Gagal !", "Nama harus diisi", "error");
															Swal.close()
														}else if(this.berat < 1){
															swal("Gagal !", "Berat harus diisi", "error");
															Swal.close()
														}else if(this.stok < 1){
															swal("Gagal !", "Stok harus diisi", "error");
															Swal.close()
              }else if (this.harga < 1000) {
                swal("Gagal !", "Harga minimal Rp 1000 ", "error");
																Swal.close()
														}else if(!this.gambardepan){
															swal("Gagal !", "Gambar Depan harus diisi", "error");
															Swal.close()
														}else if(!this.gambarsamping){
															swal("Gagal !", "Gambar Samping harus diisi", "error");
															Swal.close()
														}else if(!this.gambarbelakang){
															swal("Gagal !", "Gambar Belakang harus diisi", "error");
															Swal.close()
														}else if(!this.penjelasan){
																swal("Gagal !", "Penjelasan harus diisi", "error");
																Swal.close()
              }
            }

        }
    });
</script>

@endsection