@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Toko Saya</h3>   
					</div> 
				</div>
					<!-- properties --> 
					<div class="w3agile properties"> 
					<a href="{{route('form-toko')}}" class="btn btn-success">Tambah Toko</a>
					<div class="properties-w3lsrow"> 
@foreach($tokos as $key => $toko)
						<div class="properties-bottom">
							<div class="properties-img">
								<img src="{{asset('images/tokos/'.$toko->foto)}}" alt="">
								<div class="view-caption">
								<a href="{{route('detail-toko-saya',$toko->id)}}">	<h4><span class="glyphicon glyphicon-home"></span>  {{$toko->nama}}</h4>  </a>
								</div>
								
							</div>
							<div class="w3ls-text">
								<!-- <h5><a href="single.html">{{$toko->nama}}</a></h5> <br> -->
								<p><b>Jumlah Produk :</b> {{$toko->produkId->sum('stok')}} </p> <br>
								<p><b>Lokasi :</b> {{$toko->propinsiId->propinsi}} </p> <br>
							</div>
						</div>
@endforeach
						<div class="clearfix"> </div>
					</div>
				</div>
				<!-- //properties --> 
</div>
			
@endsection
@section('js') 
@endsection