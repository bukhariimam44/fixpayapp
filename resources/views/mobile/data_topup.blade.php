@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
				<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Data Topup</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					@include('flash::message')
					<div class="w3agile properties">
					@if(count($datas) < 1) <p style="text-align:center;">Kosong</p> @endif
					@foreach($datas as $key => $dt)
					<!-- properties -->
					<div class="properties-bottom">
								<div class="w3ls-text">
									<h5><a href="#">{{$dt->bankId->nama}} ({{$dt->bankId->no_rek}}) </a></h5>
									<h6 style="color:#11909e;">Atas Nama : {{$dt->bankId->atas_nama}}</h6>
									<br>
									<h5><a href="#">Transfer Rp {{number_format($dt->transfer,'0',',','.')}}</a></h5>
									<h6 style="font-size:14;color:black;">Kode Unik : <a style="font-size:14;color:red;">{{substr($dt->transfer,-3)}}</a></h6>
									<br>
									<h6 style="font-size:14;color:black;">No Transaksi : {{$dt->no_trx}}</h6>
									<h6 style="font-size:14;color:black;">Tgl Transaksi : {{date('d M Y H:i', strtotime($dt->created_at))}}</h6>
									<hr>
									<h6 style="font-size:14;color:black;">Status : <a style="font-size:14;color: @if($dt->status_deposit_id == 1 || $dt->status_deposit_id == 4) #eea236 @elseif($dt->status_deposit_id ==2) #11909e @else red @endif;"> <strong> {{strtoupper($dt->statusDepositId->status)}}</strong></a></h6>
									<!-- <p><b>No Transaksi</b> {{$dt->no_trx}} </p>
									<p><b>Tgl Transaksi :</b> {{date('d M Y H:i', strtotime($dt->created_at))}} </p> -->
<br>
									<!-- <button class="btn @if($dt->status_deposit_id == 1) btn-warning @elseif($dt->status_deposit_id == 2) btn-success @else btn-danger @endif">{{$dt->statusDepositId->status}}</button>  -->
									@if($dt->status_deposit_id == 1)
									<button onclick="myFunctionProses{{$dt->id}}()" class="btn btn-success">SUDAH TRANSFER</button> 
									<button onclick="myFunctionBatal{{$dt->id}}()" class="btn btn-danger">BATALKAN</button>
									@endif
								</div>
								
							</div>
							<script type="text/javascript">
						function myFunctionProses{{$dt->id}}() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Sedang Dalam proses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
																window.location.href = "<?php echo route('sudah-transfer',encrypt($dt->id)); ?>";
												}
								}).then((dismiss) => {
											window.location.href = "<?php echo route('sudah-transfer',encrypt($dt->id)); ?>";
									}
							);
						}

						function myFunctionBatal{{$dt->id}}() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Pembatalan Sedang diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
																window.location.href = "<?php echo route('batalkan-deposit',encrypt($dt->id)); ?>";
												}
								}).then((dismiss) => {
											window.location.href = "<?php echo route('batalkan-deposit',encrypt($dt->id)); ?>";
									}
							);
						}
							</script>
					@endforeach
													
													
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection