@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Laporan Voucher PLN</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					<div class="w3agile properties">
					@if(count($datas) < 1) <p style="text-align:center;">Kosong</p> @endif
											@foreach($datas as $key => $dt)
											<!-- properties -->
											<div class="properties-bottom">
														<div class="w3ls-text">
														@if(Auth::user()->uplineId->agen == 'yes' && (int)(date('Ymd', strtotime($dt->created_at))) > 20200810)
															<h5><a href="#">{{$dt->productAgneId->description}} </a></h5>
															@else
															<h5><a href="#">{{$dt->productId->description}} </a></h5>
															@endif
															<h6>Rp {{number_format($dt->harga)}}</h6>
															@if($dt->idcust == null || $dt->idcust == '')
															<p style="color:#11909e;"><b>{{$dt->nomor}}</b>  </p>
															@else
															<p style="color:#11909e;"><b>HP : {{$dt->nomor}}</b>  </p>
															<p style="color:#11909e;"><b>No Meteran : {{$dt->idcust}}</b>  </p>
															@endif
															<h6 style="font-size:14;color:black;">No Transaksi : {{$dt->no_trx}}</h6>
															<h6 style="font-size:14;color:black;">Tgl Transaksi : {{date('d M Y H:i:s', strtotime($dt->created_at))}}</h6>
															
															@if($dt->status_ppob_id == 4)
															<h6 style="font-size:14;color:black;">Token : {{$dt->sn_token}}</h6>
															@endif
															<br>
															<button class="btn @if($dt->status_ppob_id == 1) btn-warning @elseif($dt->status_ppob_id == 4) btn-success @else btn-danger @endif">{{$dt->statusPpob->name}}</button>
														</div>
													</div>
											@endforeach
													
													
													<div class="clearfix"> </div>
												</div> 
					</div>
				</div>
@endsection
@section('js')
@endsection