@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Data Agen Kamu</h3>   
					</div> 
				</div>
				<div class="row">
					<div class="col-md-12">
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
														<h6 style="font-size:14;color:black;">Bagikan Link dibawah untuk mendapatkan agen lebih banyak.</h6>
														<h6 style="font-size:14;color:black;">Setiap yang mendaftar dari Link yang kamu bagikan, maka otomatis akan jadi agen kamu.</h6>

														<hr>
														<h6 style="font-size:14;color:#11909e;" id="link">{{route('share',Auth::user()->kode)}}</h6><br>
														<button class="btn btn-success" onclick="copyToClipboard('#link')">Copy Link</button>
												</div>
										</div>
							</div>
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
												<h6 style="font-size:14;color:black;text-align:center;">DAFTARKAN AGEN BARU</h6>
												<p>Setiap transaksi agen kamu, kamu akan mendapat tambahan saldo otomatis sesuai yang kamu input di Harga Agen (Untung).</p>
														<form action="{{route('tambah-untung')}}" method="post" id="tambah_agen">
														<input type="hidden" name="action" value="add">
														@csrf
														<div class="form-group">
														<h6 style="font-size:14;color:black;">Nama Lengkap</h6>
														<input type="text" name="name" class="form-control" required="">
														</div>
														<div class="form-group">
														<h6 style="font-size:14;color:black;">Email Aktif</h6>
														<input type="email" name="email" class="form-control" required="">
														</div>
														</form>
														<div class="form-group">
														<button class="form-control btn btn-success" id="daftarKan" onclick="daftarKan()">DAFTARKAN AGEN BARU</button>
														<p style="color:red;font-size:10px;">INFO : <br>Username dan Password akan langsung dikirim ke Email.</p>
														</div>
														<script type="text/javascript">
					
						function daftarKan() {
							//Loading
							Swal.fire({
												title: 'Mohon menunggu...',
												text:'Pendaftaran Sedang Diproses..',
												allowEscapeKey: false,
												allowOutsideClick: false,
												background: '#FFFFFF',
												showConfirmButton: false,
												onOpen: ()=>{
																Swal.showLoading();
												}
								}).then((dismiss) => {
									// Swal.showLoading();
									}
							);
							//END LOADING
							document.getElementById("daftarKan").style.display="disabled";
							document.getElementById('tambah_agen').submit();
						}
					</script>
					<!-- //modal --> 
												</div>
										</div>
							</div>
							<div class="w3agile properties">
									<div>
												<div class="w3ls-text">
												<!-- <h6 style="font-size:14;color:black;">Semakin banyak orang yang mendaftar dari Link yang kamu bagikan, maka semakin banyak keuntungan yang akan didapatkan dari transaksi agen kamu. </h6> -->
														<h6 style="font-size:14;color:black;text-align:center;">DATA AGEN KAMU YG TERDAFTAR :</h6>
														<p>Dari transaksi mereka, Saldo kamu akan otomatis bertambah sesuai jumlah kamu input di Harga Agen (Untung).</p>
														@foreach($datas as $key => $dt)
														<h6 style="font-size:14;color:#11909e;">{{$key+1}}. {{$dt->name}}</h6>
														@endforeach
														@if(count($datas) < 1)
														<h6 style="font-size:14;color:#11909e;">Belum Ada</h6>
														@endif
												</div>
										</div>
							</div>
					</div>
			</div>
</div>
@endsection
@section('js')
<script>
function copyToClipboard(elementId) {
	var $temp = $("<input>");
	$("body").append($temp);
  $temp.val($(elementId).text()).select();
  document.execCommand("copy");
  $temp.remove();
		swal('Berhasil', "Berhasil Copy", "success");
}
</script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script>
    $('#flash-overlay-modal').modal();
</script>
@endsection