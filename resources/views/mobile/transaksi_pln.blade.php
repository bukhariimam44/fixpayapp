@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Transaksi Voucher Listrik</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;" id="transaksi_pln_pra">
				<div class="col-md-12">
							<div class="form-group">
								<label for="">Nominal Voucher</label>
								<select v-model="selected" class="form-control" @change="check()">
													<option v-for="option in options" v-bind:value="option.code">
																	@{{ option.description }} = Rp @{{ formatPrice(parseInt(option.price)+parseInt(option.markup)) }}
													</option>
									</select>
							</div>
							<div class="form-group">
									<label for="">Nomor HP</label>
									<input type="number" v-model="no_hp" name="no_hp" class="form-control">
							</div>
							<div class="form-group">
									<label for="">Nomor Meteran</label>
									<money v-model="no_meteran" v-bind="money" class="form-control"></money>
									<!-- <input type="number" v-model="no_meteran" name="no_meteran" class="form-control"> -->
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Panduan :</h4><br>
						<ol>
							<li>Pastikan Saldo kamu cukup.</li>
							<li>Pilih Nominal Voucher.</li>
							<!-- <li>Masukan Nomor HP</li> -->
							<li>Masukan Nomor Meteran</li>
							<li>Klik Proses</li>
						</ol>
				</div>
			</div>
</div>
@endsection
@section('js')
<script src="https://rawgit.com/vuejs-tips/v-money/master/dist/v-money.js"></script>

<script>
    var data_product = new Vue({
        el:'#transaksi_pln_pra',
        data:{
												prov:'PLN',
												selected: '',
            options: [],
            no_hp :'',
            no_meteran:'',
												money: {
														decimal: ',',
														thousands: ' ',
														precision: 0,
														masked: false
												},
            errors: [],
        },
        mounted() {
            this.load()
        },
        methods:{
            formatPrice(value) {
													const count = parseInt(value);
                return count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async load(){
                let request = {provider : this.prov};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_provider,request).then((response) =>{
																				// console.log('Berhasil'+JSON.stringify(response.data.data));
																				this.options = response.data.data
																				this.selected = response.data.data[0].code
                    // this.check()
                },(response)=>{
                    // console.log('ERROR: '+response);
                });
												},
												// async check(){
												// 				let request = {code : this.selected};
												// 				console.log('SELEC = '+this.selected);
            //     request['token'] = document.querySelector('#token').getAttribute('value');
            //     await axios.post(window.url_komisi,request).then((response) =>{
												// 								console.log('Berhasil'+JSON.stringify(response.data.data));
            //     },(response)=>{
            //         console.log('ERROR: '+response);
            //     });
            // },
           async checkForm() {
												//Loading
												Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.selected && this.no_hp && this.no_meteran) {
                let request = {code : this.selected,no_hp : this.no_hp, no_meteran : this.no_meteran};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    // console.log('Berhasil'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
                      swal({
                        title: "Transaksi Berhasil",
                        text: "Apakah ingin transaksi lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
																										Swal.close()
                        } else {
                          window.location.href = "<?php echo route('laporan-voucher-pln'); ?>";
                        }
                      });
																				}else if(response.data.code === 401){
																					Swal.fire({
																							title: response.data.title,
																							text: response.data.message,
																							allowEscapeKey: false,
																							allowOutsideClick: false,
																							icon: 'warning',
																							showCancelButton: true,
																							confirmButtonColor: '#3085d6',
																							cancelButtonColor: '#d33',
																							confirmButtonText: response.data.textBtnYa,
																							cancelButtonText: response.data.textBtnTidak
																					}).then((result) => {
																							if (result.value) {
																									// Swal.fire(
																									// 		'Terimakasih!',
																									// 		'Your file has been deleted.',
																									// 		'success'
																									// )
																									window.location.href = "<?php echo route('tambah-saldo'); ?>";
																							}
																					})
                    }else{
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    // console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Provider harus dipilih!", "error");
																Swal.close()
              }
              if (this.no_hp.length < 10) {
                swal("Cek Kembali!", "Nomor Hp Minimal 10 angka!", "error");
																Swal.close()
              }
														if (this.no_meteran.length < 11) {
                swal("Cek Kembali!", "Nomor Meteran Minimal 11 angka!", "error");
																Swal.close()
              }
            }

        }
    });
</script>
@endsection