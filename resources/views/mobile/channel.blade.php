@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
				<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Tutorial & Channels</h3>   
					</div> 
				</div>
				<!-- //banner --> 
			<!-- properties --> 
			<div class="w3agile properties"> 
					@foreach($yout as $key => $dt)
					<div class="properties-bottom">
						<div>
						<iframe width="100%" height="200px" src="https://www.youtube.com/embed/{{$dt->id_youtube}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<!-- <img src="images/3.jpg" alt=""> -->
							<!-- <div class="view-caption">
								<h4><span class="glyphicon glyphicon-map-marker"></span> Broome St, Canada, USA </h4>  
							</div> -->
							<!-- <div class="w3ls-buy">
								<a href="single.html">Buy</a> 
							</div> -->
						</div>
						<div class="w3ls-text">
							<!-- <h5><a href="">{{$dt->judul}}</a></h5> -->
							<!-- <h6>{{$dt->judul}}</h6> -->
							<p>{{$dt->judul}} </p>
							<!-- <p><b>Bed Rooms :</b> 2 </p> -->
						</div>
					</div> 
					@endforeach
					<div class="clearfix"> </div>
				</div>
				<!-- //properties --> 
@endsection
@section('js')
@endsection