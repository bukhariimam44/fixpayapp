@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
   .uploading-image{
     display:flex;
					width:200px;
					height:200px;
   }
 </style>
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Tambah Toko</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;" id="tambah_toko">
				<div class="col-md-12">
						<div class="form-group">
									<label for="">Nama Toko</label>
									<input v-model="nama" class="form-control"></input>
							</div>
							<div class="form-group">
									<label for="">Nomor Telpon</label>
									<input v-model="nohp" class="form-control"></input>
							</div>
							<div class="form-group">
								<label for="">Propinsi Pengiriman</label>
								<select v-model="select_propinsi" @change="loadKota()" class="form-control">
											<option v-for="propinsi in propinsis" v-bind:value="propinsi.id">
															@{{ propinsi.propinsi }}
											</option>
									</select>
							</div>
							<div class="form-group">
								<label for="">Kota Pengiriman</label>
								<select v-model="select_kota" class="form-control">
											<option v-for="kota in kotas" v-bind:value="kota.id">
															@{{ kota.kota }} (@{{ kota.kode_pos }})
											</option>
									</select>
							</div>

							<div class="form-group">
									<label for="">Foto Toko</label>
									<img v-if="foto_toko" :src="foto_toko" class="uploading-image" />
      			<input type="file" accept="image/jpeg" @change=uploadImage class="form-control">
							</div>

							<div class="form-group">
									<label for="">Alamat Pengiriman</label>
									<textarea v-model="alamat" class="form-control" placeholder="Alamat lengkap pengirim (Harus sama dengan alamat toko)"></textarea>
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Panduan :</h4><br>
						<ol>
							<li>Pilih Kategori Barang</li>
						</ol>
						<!-- <p> <strong style="color:red;">INGAT !</strong>  Jangan melakukan transfer sebelum perintah transfernya muncul (Setelah klik PROSES) dan Jumlah yang ditransfer harus sesuai yang ditampilan di aplikasi.</p> -->
				</div>
			</div>
</div>
@endsection
@section('js')
<script src="https://rawgit.com/vuejs-tips/v-money/master/dist/v-money.js"></script>

<script>
    var tambah_toko = new Vue({
        el:'#tambah_toko',
        data:{
												select_propinsi: null,
            propinsis: [],
												select_kota:null,
												kotas:[],
												nama:null,
												nohp:null,
												alamat:null,
												foto_toko:null,
        },
        mounted() {
            this.loadPronpinsi()
								},
        methods:{
												uploadImage(e){
                const image = e.target.files[0];
                const reader = new FileReader();
                reader.readAsDataURL(image);
                reader.onload = e =>{
                    this.foto_toko = e.target.result;
                    // console.log(this.gambardepan);
                };
            },
		
            formatPrice(value) {
                return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            },
            async loadPronpinsi(){
                await axios.get(window.url_propinsi).then((response) =>{
                    console.log('Propinsi : '+JSON.stringify(response.data));
																				this.propinsis = response.data;
                    this.select_propinsi = response.data[0].id;
																				this.loadKota()
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
												async loadKota(){
																let request = {propinsi : this.select_propinsi};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_kota,request).then((response) =>{
                    console.log('Propinsi : '+JSON.stringify(response.data));
																				this.kotas = response.data;
                    this.select_kota = response.data[0].id;
                },(response)=>{
                    console.log('ERROR: '+response);
                });
            },
           async checkForm() {
												//Loading
													Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.select_propinsi && this.select_kota && this.nama && this.nohp && this.alamat && this.foto_toko ) {
                let request = {propinsi : this.select_propinsi, kota : this.select_kota, nama : this.nama, nohp : this.nohp, alamat : this.alamat, foto_toko : this.foto_toko};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    
                    if (response.data.code === 200) {
																					console.log('Berhasil proses 200'+JSON.stringify(response.data));
																					swal({
                        title: response.data.message,
                        text: "Apakah ingin tambah toko lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
																										Swal.close()
                        } else {
                          window.location.href = "<?php echo route('toko-saya'); ?>";
                        }
                      });
                    }else{
																					console.log('Berhasil proses 400'+JSON.stringify(response.data));
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.select_propinsi) {
                swal("Gagal !", "Propinsi Pengiriman harus dipilih!", "error");
																Swal.close()
              }else if(!this.select_kota){
															swal("Gagal !", "Kota Pengiriman harus dipilih!", "error");
															Swal.close()
														}else if(!this.nama){
															swal("Gagal !", "Nama Toko harus diisi", "error");
															Swal.close()
														}else if(!this.nohp){
															swal("Gagal !", "Nomor Telp harus diisi", "error");
															Swal.close()
														}else if(!this.foto_toko){
															swal("Gagal !", "Foto Toko harus diisi", "error");
															Swal.close()
														}else if(!this.alamat){
															swal("Gagal !", "Alamat harus diisi", "error");
															Swal.close()
														}
            }

        }
    });
</script>

@endsection