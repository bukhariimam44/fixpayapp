@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div class="scroll">
			<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Transaksi Game</h3>   
					</div> 
				</div>
				<div style="padding:25px 5px 5px 5px;" id="transaksi_pulsa">
				<div class="col-md-12">
							<div class="form-group">
								<label for="">Provider</label>
								<select v-model="selected" class="form-control" @change="load()">
												<option v-for="option in options" v-bind:value="option.value">
																@{{ option.text }}
												</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Nominal</label>
								<select v-model="selected_2" class="form-control" @change="check()">
												<option v-for="nominal in nominals" v-bind:value="nominal.code">
																@{{ nominal.description }} = Rp @{{ formatPrice(parseInt(nominal.price)+parseInt(nominal.markup)) }}
												</option>
								</select>
							</div>
							<div class="form-group">
								<label for="">Nomor HP</label>
								<input type="text" v-model="no_hp" name="no_hp" class="form-control">
							</div>
							<div class="form-group">
								<label for="">Nomor ID GAME</label>
								<input type="text" v-model="idcust" name="idcust" class="form-control">
							</div>
							<div class="form-group">
								<button type="button" class="btn btn-success form-control"  @click="checkForm()">PROSES</button>
							</div>
						<br>
						<h4>Panduan :</h4><br>
						<ol>
							<li>Pastikan Saldo kamu cukup.</li>
							<li>Pilih Provider.</li>
							<li>Pilih Nominal</li>
							<li>Masukan Nomor ID GAME</li>
							<li>Klik Proses</li>
						</ol>
				</div>
			</div>
</div>
@endsection
@section('js')
<script>
    var transaksi_pulsa = new Vue({
								el:'#transaksi_pulsa',
        data:{
            
            options: [
                // { text: 'Game facebook - Boyaa Poker', value: 'BP' },
                // { text: 'Cabal Online', value: 'CBL' },
                // { text: 'e-PINS', value: 'CRY' },
																// { text: 'Digicash', value: 'DGC' },gangguan
																// { text: 'FBCARD', value: 'FBG' },
																{ text: 'FREE FIRE', value: 'FF' },
																{ text: 'Gemscool', value: 'GMS' },
																// { text: 'LYTO', value: 'LYT' },gangguan
																{ text: 'MOGCAZ', value: 'MGC' },
																// { text: 'Megaxus', value: 'MGX' },ganguan
																{ text: 'Mobile Legend', value: 'ML' },
																{ text: 'MOGPLAY', value: 'MOG' },
																{ text: 'MOLPoints', value: 'MOL' },
																{ text: 'PUBG Mobile', value: 'PU' },
																{ text: 'STEAM', value: 'STE' },
																// { text: 'Travian', value: 'TRV' },gangguan
																{ text: 'UNIPIN', value: 'UNI' },
																// { text: 'Wavegame', value: 'WVP' },gangguan
            ],
												selected: 'FF',
            selected_2 :'',
            nominals :[],
            no_hp:'',
												idcust:''
        },
        mounted() {
            this.load()
        },
        methods:{
            formatPrice(value) {
													const count = parseInt(value);
                return count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            },
            async load(){
                let request = {provider : this.selected,provider_sub:'GAME'};
                request['token'] = document.querySelector('#token').getAttribute('value');
                console.log('LOG :'+JSON.stringify(request));
                // console.log('LOG URL:'+window.url_operator);
                await axios.post(window.url_provider,request).then((response) =>{
                    console.log('Berhasil'+JSON.stringify(response.data));
                    this.nominals = response.data.data;
                    this.selected_2 = response.data.data[0].code;
                    // this.check()
                },(response)=>{
                    // console.log('ERROR: '+response);
                });
            },
            async check(){
                let request = {code : this.selected_2};
                request['token'] = document.querySelector('#token').getAttribute('value');
                // console.log('LOG :'+JSON.stringify(request));
                // console.log('LOG URL:'+window.url_operator);
                await axios.post(window.url_operator,request).then((response) =>{
                    // console.log('Berhasil check'+JSON.stringify(response.data));
                },(response)=>{
                    // console.log('ERROR: '+response);
                });
            },
           async checkForm() {
												//Loading
														Swal.fire({
																		title: 'Mohon menunggu...',
																		allowEscapeKey: false,
																		allowOutsideClick: false,
																		background: '#FFFFFF',
																		showConfirmButton: false,
																		onOpen: ()=>{
																						Swal.showLoading();
																		}
														}).then((dismiss) => {
															// Swal.showLoading();
															}
													);
													//END LOADING
              if (this.selected && this.selected_2 && this.no_hp && this.idcust) {
                let request = {code : this.selected_2,no_hp : this.no_hp,idcust:this.idcust};
                request['token'] = document.querySelector('#token').getAttribute('value');
                await axios.post(window.url_proses,request).then((response) =>{
                    // console.log('Berhasil checkform'+JSON.stringify(response.data));
                    if (response.data.code === 200) {
                      swal({
                        title: "Transaksi Berhasil",
                        text: "Apakah ingin transaksi lagi ?",
                        icon: "warning",
                        buttons: ['Tidak','Ya'],
                        dangerMode: true,
                      })
                      .then((willDelete) => {
                        if (willDelete) {
                          swal("Terimakasih, Silahkan", {
                            icon: "success",
                          });
																										Swal.close()
                        } else {
                          window.location.href = "<?php echo route('laporan-game'); ?>";
                        }
                      });
																				}else if(response.data.code === 401){
																					Swal.fire({
																							title: response.data.title,
																							text: response.data.message,
																							allowEscapeKey: false,
																							allowOutsideClick: false,
																							icon: 'warning',
																							showCancelButton: true,
																							confirmButtonColor: '#3085d6',
																							cancelButtonColor: '#d33',
																							confirmButtonText: response.data.textBtnYa,
																							cancelButtonText: response.data.textBtnTidak
																					}).then((result) => {
																							if (result.value) {
																									// Swal.fire(
																									// 		'Terimakasih!',
																									// 		'Your file has been deleted.',
																									// 		'success'
																									// )
																									window.location.href = "<?php echo route('tambah-saldo'); ?>";
																							}
																					})
                    }else{
																					
                      swal("Transaksi Gagal!", response.data.message, "error");
																						Swal.close()
                    }
                    
                },(response)=>{
                    // console.log('ERROR: '+response);
                    swal("Gagal!", response, "error");
																				Swal.close()
                });
                
              }
              
              this.errors = [];
              if (!this.selected) {
                swal("Cek Kembali!", "Provider harus dipilih!", "error");
																Swal.close()
              }else
              if (!this.selected_2) {
                swal("Cek Kembali!", "Nominal harus dipilih!", "error");
																Swal.close()
              }else
              if (this.no_hp.length < 5) {
                swal("Cek Kembali!", "Nomor HP Minimal 10 angka!", "error");
																Swal.close()
              }else
														if (this.idcust.length < 3) {
                swal("Cek Kembali!", "Nomor ID GAME Minimal 3 angka!", "error");
																Swal.close()
              }
            }

        }
    });
</script>
@endsection