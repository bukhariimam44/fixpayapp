@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<style>
    html,
    body {
      position: relative;
      height: 100%;
    }

    body {
      background: #eee;
      font-family: Helvetica Neue, Helvetica, Arial, sans-serif;
      font-size: 14px;
      color: #000;
      margin: 0;
      padding: 0;
    }

    .swiper-container {
      width: 100%;
      height: 100%;
    }

    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;

      /* Center slide text vertically */
      display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }
  </style>
		<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
@endsection
@section('content') 
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Detail Barang</h3>   
					</div> 
				</div>
					<!-- properties --> 
					<div class="w3agile properties"> 
					<h3 class="w3ls-title">{{$produks->nama_barang}}</h3> 
					<!-- <p class="w3-text">Lorem ipsum dolor sit amet consectetur adip iscing elit. Nam vestibulum ipsum quis purus varius efficitur nunc eget purus ac risus facilisis.</p> -->

					<div class="properties-w3lsrow"> 

						<div class="properties-bottom">

								<div class="w3ls-text">
									<!-- Swiper -->
											<div class="swiper-container">
													<div class="swiper-wrapper">
															<div class="swiper-slide"><img src="{{asset('images/produks/'.$produks->gambar_depan)}}" alt=""></div>
															<div class="swiper-slide"><img src="{{asset('images/produks/'.$produks->gambar_samping)}}" alt=""></div>
															<div class="swiper-slide"><img src="{{asset('images/produks/'.$produks->gambar_belakang)}}" alt=""></div>
													</div>
													<!-- Add Pagination -->
													<div class="swiper-pagination"></div>
											</div>
											<!-- Swiper -->
								
								</div>
							
							<div class="w3ls-text">
								<h5>Kode : <a href="#"> {{$produks->kode_barang}}</a></h5> <br>
								<p><b>Kategori :</b> {{$produks->kategoriId->kategori}}</p> <br>
								<p><b>Harga Produk :</b> Rp {{number_format($produks->harga,0,',','.')}}</p> <br>
								<p><b>Berat :</b> {{$produks->berat}} Gram</p> <br>
								<p><b>Stok :</b> {{$produks->stok}}</p> <br>
								<p><b>Alamat Pengiriman :</b>  {{$produks->tokoId->alamat_pengiriman}}, {{$produks->tokoId->kotaId->kota}} - {{$produks->tokoId->propinsiId->propinsi}}</p> <br>
							</div>
						</div>


						<div class="clearfix"> </div>
					</div>
				</div>
			
@endsection
@section('js') 
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
      spaceBetween: 30,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
  </script>
@endsection