@extends('layouts.app_umum')
@section('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endsection
@section('content') 
<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Toko {{$tokos->nama}}</h3>   
					</div> 
				</div>
					<!-- properties --> 
					<div class="w3agile properties"> 
					<h3 class="w3ls-title">Produk</h3> 
					<p class="w3-text">Barang - barang yang di jual di Toko {{$tokos->nama}} : </p> <br>
					<a href="{{route('form-produk',$tokos->id)}}" class="btn btn-success">Tambah Produk</a>
					<div class="properties-w3lsrow"> 
@foreach($produks as $key => $produk)
						<div class="properties-bottom">
							<div class="w3ls-text">
								<img src="{{asset('images/produks/'.$produk->gambar_depan)}}" alt="">
							</div>
							<div class="w3ls-text">
								<h5><a href="{{route('detail-produk-saya',$produk->id)}}">{{$produk->nama_barang}}</a></h5> <br>
								<p><b>Harga Produk :</b> Rp {{number_format($produk->harga,0,',','.')}} </p> <br>
								<p><b>Stok :</b> {{$produk->stok}} </p> <br>
								<p><b>Berat :</b> {{$produk->berat}} Gram</p> <br>
							</div>
						</div>
@endforeach
						<div class="clearfix"> </div>
					</div>
				</div>
			
@endsection
@section('js') 

@endsection