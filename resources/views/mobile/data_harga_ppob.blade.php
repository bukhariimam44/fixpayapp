@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
			<div	div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Harga Agen{{$product}}</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row">
					<div class="col-md-12">
					@include('flash::message')
					<button class="pull-right btn btn-success" onclick="event.preventDefault();
                document.getElementById('update-harga').submit();">Update Untung</button>
								<div class="w3agile properties">
										<form action="{{route('harga_downline',$ids)}}" method="post" id="update-harga">
											@csrf
											<input type="hidden" name="action" value="update">
											@foreach($hargas as $key => $data)
											<input type="hidden" name="code[]" value="{{$data->code}}">
											<div class="properties-bottom">
														<div class="w3ls-text">
														<h6 style="font-size:14;color:black;">Provider : <a style="font-size:14;color:#11909e;">{{$data->provider}}</a></h6>
															<h6 style="font-size:14;color:black;">Description : <a style="font-size:14;color:#11909e;">{{$data->description}}</a></h6>
															<h6 style="font-size:14;color:black;">Harga Supplier : <a style="font-size:14;color:#11909e;"> Rp {{number_format($data->price)}}</a></h6>
															<h6 style="font-size:14;color:black;">Untung : <input type="number" name="markup[]" id="markup[]" value="{{$data->markup}}" required="" max="300"/> Max Rp 500</h6>
															<h6 style="font-size:14;color:black;">Harga + Untung : <a style="font-size:14;color:#11909e;"> Rp {{number_format($data->markup+$data->price)}}</a></h6>
														</div>
											</div>
											@endforeach
										</form> 
										<div class="clearfix"> </div>
								</div> 
					</div>
				</div>
@endsection
@section('js')
<script>
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@endsection