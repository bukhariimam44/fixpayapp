@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
				<div class="banner about-banner"> 
					<div class="banner-img4">  
						<h3>Harga {{$name}}</h3>   
					</div> 
				</div>
				<!-- //banner --> 
				<div class="row" style="margin-top:40px;">
					<div class="col-md-12">
								<div class="table-responsive">
									<table class="table">
										<thead>
										<tr>
											<th>No</th>	@if($name == 'Paket Data')<th>Provider</th> @endif<th>Produk</th>	<th>Harga</th>
										</tr>
										</thead>
										<tbody>
											@foreach($datas as $key => $dt)
											<tr>
												<td width="20px">{{$key+1}}.</td>@if($name == 'Paket Data')<td>{{$dt->provider}}</td>@endif<td>{{$dt->description}}</td><td>Rp {{number_format($dt->price+$dt->markup)}}</td>
											</tr>
											@endforeach
										</tbody>
										

									</table>
								</div>
					</div>
				</div>
@endsection
@section('js')
@endsection