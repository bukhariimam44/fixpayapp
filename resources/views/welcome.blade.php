@extends('layouts.app_umum')
@section('css')
@endsection
@section('content') 
<!-- banner -->
<div class="banner">
					<div id="kb" class="carousel kb_elastic animate_text kb_wrapper" data-ride="carousel" data-interval="6000" data-pause="hover">
						<!-- Wrapper-for-Slides -->
						<div class="carousel-inner" role="listbox"> 
							<!-- First-Slide -->
							<div class="item active">
								<div class="banner-img"> 
									<div class="carousel-caption kb_caption">
										<!-- <h3 data-animation="animated flipInX">Real estate</h3>   -->
									</div>
								</div>
							</div> 
							<!-- Second-Slide -->
							<div class="item">
								<div class="banner-img banner-img1"> 
									<div class="carousel-caption kb_caption kb_caption_right">
										<!-- <h3 data-animation="animated flipInX">Dream Home</h3>  -->
									</div>
								</div>
							</div> 
							<!-- Third-Slide -->
							<div class="item">
								<div class="banner-img banner-img2"> 
									<div class="carousel-caption kb_caption kb_caption_center">
										<!-- <h3 data-animation="animated flipInX">Latest Design</h3>  -->
									</div>
								</div>
							</div> 
							<!-- Third-Slide -->
							<div class="item">
								<div class="banner-img banner-img3"> 
									<div class="carousel-caption kb_caption kb_caption_center">
										<!-- <h3 data-animation="animated flipInX">Latest Design</h3>  -->
									</div>
								</div>
							</div> 
						</div> 
						<!-- Left-Button -->
						<a class="left carousel-control kb_control_left" href="#kb" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a> 
						<!-- Right-Button -->
						<a class="right carousel-control kb_control_right" href="#kb" role="button" data-slide="next">
							<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a> 
					</div>
					<script src="js/custom.js"></script>
				</div>
				<!-- //banner -->
					<!-- welcome -->
					<div class="welcome"> 
					<h3 class="w3ls-title">Selamat Datang ! @if(Auth::check()) <br> {{Auth::user()->name}} @endif</h3> 
					<p class="w3title-text">FixPay adalah Aplikasi Transaksi Pulsa Termurah SeIndonesia yang pendaftarannya GRATIS dan Cara transaksi yang sangat mudah. </p>
					<p class="w3title-text">Jadilah pengguna Aplikasi FixPay ke <strong> {{(int)count(App\User::get())+123}}</strong> yang merasakan keuntungannya. </p>

					<br><br>	
				</div> 
			
@endsection
@section('js')
@endsection