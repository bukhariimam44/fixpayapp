<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
	protected $fillable = [
			'id','toko_id','categories_id','nama_barang','kode_barang','gambar_depan','gambar_samping','gambar_belakang','user_id','stok','harga','berat','aktif','penjelasan'
	];
	public function gambarId(){
		return $this->hasOne('App\GambarProduk','produk_id');
	}
	public function gambarAll(){
		return $this->hasMany('App\GambarProduk','produk_id');
	}
	public function tokoId(){
		return $this->belongsTo('App\Toko','toko_id');
	}
	public function kategoriId(){
		return $this->belongsTo('App\Category','categories_id');
	}
}
