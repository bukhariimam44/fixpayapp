<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataHargaPpobAgent extends Model
{
	protected $fillable = [
		'id','user_upline','provider','provider_sub','operator','operator_sub','code', 'description','price','markup','status'
];
}
