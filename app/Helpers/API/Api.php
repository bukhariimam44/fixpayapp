<?php

namespace App\Helpers\API;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;

abstract class Api
{
    protected $method;
    protected $parameters;
    protected $data;
    protected $overWriteOptions=[];
    protected $url;

    public function __construct()
    {

    }

    public function get(){
        return $this->data;
    }

    protected function getData(){
        try {
          $client = new Client();
          $res = $client->request($this->method,  $this->url, $this->overWriteOptions);
        } catch (ClientException $e) {
          $response = json_decode($e->getResponse()->getBody()->getContents(), true);
          $this->data = $response;
          return $this;
        }

        if($res->getStatusCode() == 200) {
          $response = json_decode($res->getBody()->getContents(), true);
          $this->data = $response;
          return $this;
        }
        // Timeout / Gangguan Teknis
        $res_failed = [
    			'status' => [
    				'code' => 400,
    				'confirm' => 'failed',
    				'message' => 'Periksa Kembali Koneksi Internet Anda',
    			],
    		];

        $this->data= $res_failed;
        return $this;
    }
}