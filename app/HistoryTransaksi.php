<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryTransaksi extends Model
{
	protected $fillable = [
		'id','user_id', 'no_trx','data_harga_ppob_id','code','provider_sub','mutasi','nomor','idcust','harga','untung','untung_agen','ket','sn_token','id_time','status_ppob_id','saldo'
	];
	public function productId(){
			return $this->belongsTo('App\DataHargaPpob','data_harga_ppob_id');
	}
	public function productAgneId(){
		return $this->belongsTo('App\DataHargaPpobAgent','data_harga_ppob_id');
}
	public function statusPpob(){
			return $this->belongsTo('App\StatusPpob','status_ppob_id');
	}
	public function userId(){
		return $this->belongsTo('App\User','user_id');
	}
}
