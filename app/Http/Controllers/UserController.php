<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Notifications\PendaftaranAgen;
use Jenssegers\Agent\Agent;
use App\Mail\PengajuanDeposit;
use Illuminate\Support\Str;
use App\DataHargaPpob;
use App\HistoryTransaksi;
use App\BukuSaldo;
use App\User;
use App\Bank;
use App\Deposit;
use App\Toko;
use App\Produk;
use App\Propinsi;
use App\Kota;
use App\Category;
use App\DataHargaPpobAgent;
use DB;
use Log;
use JavaScript;
use Mail;
use Image;
use Cookie;
use App\Helpers\Req;

class UserController extends Controller
{
				public function __construct()
				{
								$this->middleware('user');
								$this->middleware(['auth','verified']);
								$this->url=config('api-config')['api_url'];
				}
				public function user(){
					flash()->overlay('Login Berhasil', 'Berhasil');
					return redirect()->route('index');
				}
    public function transaksi_pulsa(){
					JavaScript::put([
						'url_provider'=>route('provider'),
						'url_operator'=>route('operator'),
						'url_proses'=>route('proses')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_pulsa');
				}
				public function provider(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}else {
						$datas = DataHargaPpob::where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}
					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				
				public function operator(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request['code'])->where('status','normal')->first();
					}else {
								$datas = DataHargaPpob::where('code',$request['code'])->where('status','normal')->first();
					}
								return response()->json([
												'code'=>200,
												'data'=>$datas
								]);
				}
				public function proses(Request $request){
					$message = [
									'code.required'=>'URL tdk boleh kosong',
									'no_hp.required'=>'Nama Product tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'no_hp'  => 'required|min:10',
									'code'  => 'required',
									'token'  => 'required',
					],$message);
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Pulsa :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Proses Pulsa',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					
					if ($request->user()->uplineId->agen =='yes') {
						$product = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request->code)->where('status','normal')->first();
						$untung_upline = $product['markup'];

						$markup_admins = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $markup_admins['markup_agen'];
					}else {
						$untung_upline = 0;

						$product = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $product['markup'];
					}
					if ($product) {
						if ($request->user()->saldo < ($product['price']+$product['markup'])) {
										return response()->json([
														'code'=>401,
														'title'=>'Saldo Kamu Kurang',
														'message'=>'Apakah ingin tambah saldo sekarang ?',
														'textBtnYa'=>'Ya, Tambah Sekarang',
														'textBtnTidak'=>'Tambah Nanti',
										]);
						}else {
										DB::beginTransaction();
										try {
														$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
														$params['code'] = $request->code;
														$params['no_hp'] = $request->no_hp;
														$params['no_trx'] = $no_trx;
														$saldo_akhir = (int)($request->user()->saldo - ($product['price']+$product['markup']));
														HistoryTransaksi::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'data_harga_ppob_id'=>$product['id'],
																		'code'=>$product['code'],
																		'provider_sub'=>$product['provider_sub'],
																		'mutasi'=>'Debet',
																		'harga'=>(int)($product['price']+$product['markup']),
																		'untung'=>(int)($untung_admin),
																		'untung_agen'=>(int)($untung_upline),
																		'nomor'=>$request->no_hp,
																		'ket'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')',
																		'status_ppob_id'=>1,
																		'id_time'=>$request->user()->id.$product->id.date('Ymdhi'),
																		'saldo'=>(int)($saldo_akhir)
														]);
														BukuSaldo::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'mutasi'=>'Debet',
																		'nominal'=>(int)($product['price']+$product['markup']),
																		'saldo_akhir'=>(int)($saldo_akhir),
																		'keterangan'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')'
														]);
														$anggota = User::find($request->user()->id)->update([
																		'saldo'=>(int)($saldo_akhir)
														]);
														$response = Req::post($params, $this->url.'proses-pulsa')->get();
										} catch (\Throwable $th) {
														Log::info('Gagal proses Transaksi:'.$th->getMessage());
														DB::rollback();
														return response()->json([
																		'code'=>400,
																		'message'=>'Ulangi kembali 1 Menit lagi',
																		'data'=>$th
														]);
										}
										if ($response['code'] == 200) {
														DB::commit();
														return response()->json([
																		'code'=>200,
																		'message'=>'Respon transaksi pembelian',
																		'data'=>$response['data']
														]);
										}
										DB::rollback();
										return response()->json([
														'code'=>400,
														'message'=>$response['message'],
														'data'=>$response['data']
										]);
						}
					}
					return response()->json([
									'code'=>400,
									'message'=>'Product tidak ditemukan',
					]);

					
				}
				public function transaksi_pln(){
					JavaScript::put([
						'url_provider'=>route('provider-pln'),
						'url_komisi'=>route('operator'),
						'url_proses'=>route('proses-pln')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_pln');
				}
				public function provider_pln(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}else {
						$datas = DataHargaPpob::where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}

					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				public function proses_pln(Request $request){
								$message = [
												'code.required'=>'Code tdk boleh kosong',
												'no_hp.required'=>'No HP tdk boleh kosong',
												'no_meteran.required'=>'No meteran tdk boleh kosong',
								];
								$response = array('response' => '', 'success'=>false);
								$validator = Validator::make($request->all(), [
												'code'  => 'required',
												'token'  => 'required',
												'no_hp'  => 'required|min:10',
												'no_meteran'  => 'required|min:10',
								],$message);
								
								if ($validator->fails()) {
												$response['response'] = $validator->messages();
												Log::info('Gagal Proses Token Listrik :'.$validator->messages());
												$respon = [
																'code'=>401,
																'message'=>'Gagal Proses Token Listrik',
																'status'=>'gagal',
																'error'=>$response
												];
												return response()->json($respon);
								}
								
								if ($request->user()->uplineId->agen =='yes') {
									$product = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request->code)->where('status','normal')->first();
									$untung_upline = $product['markup'];
			
									$markup_admins = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
									$untung_admin = $markup_admins['markup_agen'];
								}else {
									$product = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
									$untung_admin = $product['markup'];
									$untung_upline = 0;
								}
								if ($product) {
									if ((int)($request->user()->saldo) < (int)($product['price']+$product['markup'])) {
										return response()->json([
														'code'=>401,
														'title'=>'Saldo Kamu Kurang',
														'message'=>'Apakah ingin tambah saldo sekarang ?',
														'textBtnYa'=>'Ya, Tambah Sekarang',
														'textBtnTidak'=>'Tambah Nanti',
										]);
									}else {
													DB::beginTransaction();
													try {
																	$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
																	$params['code'] = $request->code;
																	$params['no_hp'] = $request->no_hp;
																	$params['no_meteran'] = $request->no_meteran;
																	$params['no_trx'] = $no_trx;

																	$response = Req::post($params, $this->url.'proses-pln')->get();
																	$saldo_akhir = (int)($request->user()->saldo - ($product['price']+$product['markup']));
																	HistoryTransaksi::create([
																					'user_id'=>$request->user()->id,
																					'no_trx'=>$no_trx,
																					'data_harga_ppob_id'=>$product['id'],
																					'code'=>$product['code'],
																					'provider_sub'=>$product['provider_sub'],
																					'mutasi'=>'Debet',
																					'harga'=>(int)($product['price']+$product['markup']),
																					'untung'=>(int)($untung_admin),
																					'untung_agen'=>(int)($untung_upline),
																					'nomor'=>$request->no_hp,
																					'idcust'=>$request->no_meteran,
																					'ket'=>'Transaksi '.$product['description'].' ('.$request->no_meteran.')',
																					'status_ppob_id'=>1,
																					'id_time'=>$request->user()->id.$product->id.date('Ymdhi'),
																					'saldo'=>(int)($saldo_akhir)
																	]);
																	BukuSaldo::create([
																					'user_id'=>$request->user()->id,
																					'no_trx'=>$no_trx,
																					'mutasi'=>'Debet',
																					'nominal'=>(int)($product['price']+$product['markup']),
																					'saldo_akhir'=>(int)($saldo_akhir),
																					'keterangan'=>'Transaksi '.$product['description'].' ('.$request->no_meteran.')'
																	]);
																	User::find($request->user()->id)->update([
																					'saldo'=>(int)($saldo_akhir)
																	]);
																	
													} catch (\Throwable $th) {
																	Log::info('Gagal proses Transaksi:'.$th->getMessage());
																	DB::rollback();
																	return response()->json([
																					'code'=>400,
																					'message'=>'Ulangi kembali 1 Menit lagi',
																					'data'=>$th
																	]);
													}
													if ($response['code'] == 200) {
																	DB::commit();
																	return response()->json([
																					'code'=>200,
																					'message'=>'Respon transaksi pembelian',
																					'data'=>$response['data']
																	]);
													}
													DB::rollback();
													return response()->json([
																	'code'=>400,
																	'message'=>$response['message'],
																	'data'=>$response['data']
													]);
									}
								}
								return response()->json([
												'code'=>400,
												'message'=>'Product tidak ditemukan',
								]);

								
				}
					//PAKET DATA
				public function transaksi_paket_data(){
					JavaScript::put([
						'url_provider'=>route('provider-paket-data'),
						'url_operator'=>route('operator-paket-data'),
						'url_proses'=>route('proses-paket-data')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_paket_data');
				}
				public function provider_paket_data(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider',$request['provider'])->where('provider_sub','INTERNET')->where('status','normal')->orderBy('code','ASC')->get();
					}else {
						$datas = DataHargaPpob::where('provider',$request['provider'])->where('provider_sub','INTERNET')->where('status','normal')->orderBy('code','ASC')->get();
					}
					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				
				public function operator_paket_data(Request $request){
								$datas = DataHargaPpob::where('code',$request['code'])->where('status','normal')->first();
								return response()->json([
												'code'=>200,
												'data'=>$datas
								]);
				}
				public function proses_paket_data(Request $request){
					$message = [
									'code.required'=>'URL tdk boleh kosong',
									'no_hp.required'=>'Nama Product tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'no_hp'  => 'required|min:10',
									'code'  => 'required',
									'token'  => 'required',
					],$message);
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Paket data :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Proses Paket data',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					
					if ($request->user()->uplineId->agen =='yes') {
						$product = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request->code)->where('status','normal')->first();
						$untung_upline = $product['markup'];

						$markup_admins = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $markup_admins['markup_agen'];
					}else {
						$product = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $product['markup'];
						$untung_upline = 0;
					}
					// $product = DataHargaPpob::where('code',$params['code'])->where('status','normal')->first();
					if ($product) {
						if ((int)($request->user()->saldo) < (int)($product['price']+$product['markup'])) {
							return response()->json([
											'code'=>401,
											'title'=>'Saldo Kamu Kurang',
											'message'=>'Apakah ingin tambah saldo sekarang ?',
											'textBtnYa'=>'Ya, Tambah Sekarang',
											'textBtnTidak'=>'Tambah Nanti',
							]);
						}else {
										DB::beginTransaction();
										try {
														$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
														$params['code'] = $request->code;
														$params['no_hp'] = $request->no_hp;
														$params['no_trx'] = $no_trx;

														$saldo_akhir = (int)($request->user()->saldo - ($product['price']+$product['markup']));
														HistoryTransaksi::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'data_harga_ppob_id'=>$product['id'],
																		'code'=>$product['code'],
																		'provider_sub'=>$product['provider_sub'],
																		'mutasi'=>'Debet',
																		'harga'=>(int)($product['price']+$product['markup']),
																		'untung'=>(int)($untung_admin),
																		'untung_agen'=>(int)($untung_upline),
																		'nomor'=>$request->no_hp,
																		'ket'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')',
																		'status_ppob_id'=>1,
																		'id_time'=>$request->user()->id.$product->id.date('Ymdhi'),
																		'saldo'=>(int)($saldo_akhir)
														]);
														BukuSaldo::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'mutasi'=>'Debet',
																		'nominal'=>(int)($product['price']+$product['markup']),
																		'saldo_akhir'=>(int)($saldo_akhir),
																		'keterangan'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')'
														]);
														$anggota = User::find($request->user()->id)->update([
																		'saldo'=>(int)($saldo_akhir)
														]);
														$response = Req::post($params, $this->url.'proses-pulsa')->get();
										} catch (\Throwable $th) {
														Log::info('Gagal proses Transaksi:'.$th->getMessage());
														DB::rollback();
														return response()->json([
																		'code'=>400,
																		'message'=>'Ulangi kembali 1 Menit lagi',
																		'data'=>$th
														]);
										}
										if ($response['code'] == 200) {
														DB::commit();
														return response()->json([
																		'code'=>200,
																		'message'=>'Respon transaksi pembelian',
																		'data'=>$response['data']
														]);
										}
										DB::rollback();
										return response()->json([
														'code'=>400,
														'message'=>$response['message'],
														'data'=>$response['data']
										]);
						}
					}
					return response()->json([
									'code'=>400,
									'message'=>'Product tidak ditemukan',
					]);

					
				}
				//END PAKET DATA
				//VOUCHER GMAE
				public function transaksi_voucher_game(Request $request){
					JavaScript::put([
						'url_provider'=>route('provider-game'),
						'url_proses'=>route('proses-game')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_voucher_game');

				}
				public function provider_game(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider',$request['provider_sub'])->where('operator_sub',$request->provider)->where('status','normal')->orderBy('price','ASC')->get();
					}else {
						$datas = DataHargaPpob::where('provider',$request['provider_sub'])->where('operator_sub',$request->provider)->where('status','normal')->orderBy('price','ASC')->get();
					}
					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				public function proses_game(Request $request){
					$message = [
									'code.required'=>'URL tdk boleh kosong',
									'no_hp.required'=>'Nama Product tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'no_hp'  => 'required|min:5',
									'code'  => 'required',
									'token'  => 'required',
					],$message);
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Paket data :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Proses Game',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					
					if ($request->user()->uplineId->agen =='yes') {
						$product = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request->code)->where('status','normal')->first();
						$untung_upline = $product['markup'];

						$markup_admins = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $markup_admins['markup_agen'];
					}else {
						$product = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $product['markup'];
						$untung_upline = 0;
					}
					// $product = DataHargaPpob::where('code',$params['code'])->where('status','normal')->first();
					if ($product) {
						if ((int)($request->user()->saldo) < (int)($product['price']+$product['markup'])) {
							return response()->json([
											'code'=>401,
											'title'=>'Saldo Kamu Kurang',
											'message'=>'Apakah ingin tambah saldo sekarang ?',
											'textBtnYa'=>'Ya, Tambah Sekarang',
											'textBtnTidak'=>'Tambah Nanti',
							]);
						}else {
										DB::beginTransaction();
										try {
														$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
														$params['code'] = $request->code;
														$params['no_hp'] = $request->no_hp;
														$params['idcust'] = $request->idcust;
														$params['no_trx'] = $no_trx;

														$saldo_akhir = (int)($request->user()->saldo - ($product['price']+$product['markup']));
														HistoryTransaksi::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'data_harga_ppob_id'=>$product['id'],
																		'code'=>$product['code'],
																		'provider_sub'=>'GAME',
																		'mutasi'=>'Debet',
																		'harga'=>(int)($product['price']+$product['markup']),
																		'untung'=>(int)($untung_admin),
																		'untung_agen'=>(int)($untung_upline),
																		'nomor'=>$request->no_hp,
																		'idcust'=>$request->idcust,
																		'ket'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')',
																		'status_ppob_id'=>1,
																		'id_time'=>$request->user()->id.$product->id.date('Ymdhi'),
																		'saldo'=>(int)($saldo_akhir)
														]);
														BukuSaldo::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'mutasi'=>'Debet',
																		'nominal'=>(int)($product['price']+$product['markup']),
																		'saldo_akhir'=>(int)($saldo_akhir),
																		'keterangan'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')'
														]);
														$anggota = User::find($request->user()->id)->update([
																		'saldo'=>(int)($saldo_akhir)
														]);
														$response = Req::post($params, $this->url.'proses-game')->get();
										} catch (\Throwable $th) {
														Log::info('Gagal proses Transaksi game:'.$th->getMessage());
														DB::rollback();
														return response()->json([
																		'code'=>400,
																		'message'=>'Ulangi kembali 1 Menit lagi',
																		'data'=>$th
														]);
										}
										if ($response['code'] == 200) {
														DB::commit();
														return response()->json([
																		'code'=>200,
																		'message'=>'Respon transaksi pembelian',
																		'data'=>$response['data']
														]);
										}
										DB::rollback();
										return response()->json([
														'code'=>400,
														'message'=>$response['message'],
														'data'=>$response['data']
										]);
						}
					}
					return response()->json([
									'code'=>400,
									'message'=>'Product tidak ditemukan',
					]);
				}
				public function transaksi_e_money(Request $request){
					JavaScript::put([
						'url_provider'=>route('provider-e-money'),
						'url_proses'=>route('proses-e-money')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_e_money');

				}
				public function provider_e_money(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub',$request['provider_sub'])->where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}else {
						$datas = DataHargaPpob::where('provider_sub',$request['provider_sub'])->where('operator_sub',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}
					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				public function proses_e_money(Request $request){
					$message = [
									'code.required'=>'URL tdk boleh kosong',
									'no_hp.required'=>'Nama Product tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'no_hp'  => 'required|min:5',
									'code'  => 'required',
									'token'  => 'required',
					],$message);
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Paket data :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Proses Game',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					
					if ($request->user()->uplineId->agen =='yes') {
						$product = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request->code)->where('status','normal')->first();
						$untung_upline = $product['markup'];

						$markup_admins = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $markup_admins['markup_agen'];
					}else {
						$product = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $product['markup'];
						$untung_upline = 0;
					}

					if ($product) {
						if ((int)($request->user()->saldo) < (int)($product['price']+$product['markup'])) {
							return response()->json([
											'code'=>401,
											'title'=>'Saldo Kamu Kurang',
											'message'=>'Apakah ingin tambah saldo sekarang ?',
											'textBtnYa'=>'Ya, Tambah Sekarang',
											'textBtnTidak'=>'Tambah Nanti',
							]);
						}else {
										DB::beginTransaction();
										try {
														$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
														$params['code'] = $request->code;
														$params['no_hp'] = $request->no_hp;
														$params['no_trx'] = $no_trx;

														$saldo_akhir = (int)($request->user()->saldo - ($product['price']+$product['markup']));
														HistoryTransaksi::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'data_harga_ppob_id'=>$product['id'],
																		'code'=>$product['code'],
																		'provider_sub'=>'LAIN',
																		'mutasi'=>'Debet',
																		'harga'=>(int)($product['price']+$product['markup']),
																		'untung'=>(int)($untung_admin),
																		'untung_agen'=>(int)($untung_upline),
																		'nomor'=>$request->no_hp,
																		'id_time'=>$request->user()->id.$product->id.date('Ymdhi'),
																		'ket'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')',
																		'status_ppob_id'=>1,
																		'saldo'=>(int)($saldo_akhir)
														]);
														BukuSaldo::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'mutasi'=>'Debet',
																		'nominal'=>(int)($product['price']+$product['markup']),
																		'saldo_akhir'=>(int)($saldo_akhir),
																		'keterangan'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')'
														]);
														$anggota = User::find($request->user()->id)->update([
																		'saldo'=>(int)($saldo_akhir)
														]);
														$response = Req::post($params, $this->url.'proses-e-money')->get();
										} catch (\Throwable $th) {
														Log::info('Gagal proses Transaksi E-Money:'.$th->getMessage());
														DB::rollback();
														return response()->json([
																		'code'=>400,
																		'message'=>'Ulangi kembali 1 Menit lagi',
																		'data'=>$th
														]);
										}
										if ($response['code'] == 200) {
														DB::commit();
														return response()->json([
																		'code'=>200,
																		'message'=>'Respon transaksi pembelian',
																		'data'=>$response['data']
														]);
										}
										DB::rollback();
										return response()->json([
														'code'=>400,
														'message'=>$response['message'],
														'data'=>$response['data']
										]);
						}
					}
					return response()->json([
									'code'=>400,
									'message'=>'Product tidak ditemukan',
					]);
				}
				public function transaksi_sms(Request $request){
					JavaScript::put([
						'url_provider'=>route('provider-sms'),
						'url_proses'=>route('proses-sms')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_paket_sms');

				}
				public function provider_sms(Request $request){
					if ($request->user()->uplineId->agen =='yes') {
						$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub',$request['provider_sub'])->where('provider',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}else {
						$datas = DataHargaPpob::where('provider_sub',$request['provider_sub'])->where('provider',$request['provider'])->where('status','normal')->orderBy('price','ASC')->get();
					}
					return response()->json([
									'code'=>200,
									'data'=>$datas
					]);
				}
				public function proses_sms(Request $request){
					$message = [
									'code.required'=>'URL tdk boleh kosong',
									'no_hp.required'=>'Nama Product tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'no_hp'  => 'required|min:5',
									'code'  => 'required',
									'token'  => 'required',
					],$message);
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Paket data :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Proses SMS',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					
					if ($request->user()->uplineId->agen =='yes') {
						$product = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('code',$request->code)->where('status','normal')->first();
						$untung_upline = $product['markup'];

						$markup_admins = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $markup_admins['markup_agen'];
					}else {
						$product = DataHargaPpob::where('code',$request->code)->where('status','normal')->first();
						$untung_admin = $product['markup'];
						$untung_upline = 0;
					}
					// $product = DataHargaPpob::where('code',$params['code'])->where('status','normal')->first();
					if ($product) {
						if ((int)($request->user()->saldo) < (int)($product['price']+$product['markup'])) {
							return response()->json([
											'code'=>401,
											'title'=>'Saldo Kamu Kurang',
											'message'=>'Apakah ingin tambah saldo sekarang ?',
											'textBtnYa'=>'Ya, Tambah Sekarang',
											'textBtnTidak'=>'Tambah Nanti',
							]);
						}else {
										DB::beginTransaction();
										try {
														$no_trx = date('ymdhis').$request->user()->id.rand(100,999);
														$params['code'] = $request->code;
														$params['no_hp'] = $request->no_hp;
														$params['no_trx'] = $no_trx;

														$saldo_akhir = (int)($request->user()->saldo - ($product['price']+$product['markup']));
														HistoryTransaksi::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'data_harga_ppob_id'=>$product['id'],
																		'code'=>$product['code'],
																		'provider_sub'=>$product['provider_sub'],
																		'mutasi'=>'Debet',
																		'harga'=>(int)($product['price']+$product['markup']),
																		'untung'=>(int)($untung_admin),
																		'untung_agen'=>(int)($untung_upline),
																		'nomor'=>$request->no_hp,
																		'ket'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')',
																		'status_ppob_id'=>1,
																		'id_time'=>$request->user()->id.$product->id.date('Ymdhi'),
																		'saldo'=>(int)($saldo_akhir)
														]);
														BukuSaldo::create([
																		'user_id'=>$request->user()->id,
																		'no_trx'=>$no_trx,
																		'mutasi'=>'Debet',
																		'nominal'=>(int)($product['price']+$product['markup']),
																		'saldo_akhir'=>(int)($saldo_akhir),
																		'keterangan'=>'Transaksi '.$product['description'].' ('.$request->no_hp.')'
														]);
														$anggota = User::find($request->user()->id)->update([
																		'saldo'=>(int)($saldo_akhir)
														]);
														$response = Req::post($params, $this->url.'proses-e-money')->get();
										} catch (\Throwable $th) {
														Log::info('Gagal proses Transaksi E-Money:'.$th->getMessage());
														DB::rollback();
														return response()->json([
																		'code'=>400,
																		'message'=>'Ulangi kembali 1 Menit lagi',
																		'data'=>$th
														]);
										}
										if ($response['code'] == 200) {
														DB::commit();
														return response()->json([
																		'code'=>200,
																		'message'=>'Respon transaksi pembelian',
																		'data'=>$response['data']
														]);
										}
										DB::rollback();
										return response()->json([
														'code'=>400,
														'message'=>$response['message'],
														'data'=>$response['data']
										]);
						}
					}
					return response()->json([
									'code'=>400,
									'message'=>'Product tidak ditemukan',
					]);
				}
				public function transaksi_telpon(Request $request){
					JavaScript::put([
						'url_provider'=>route('provider-sms'),
						'url_proses'=>route('proses-sms')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.transaksi_paket_telpon');
				}
				public function tambah_saldo(Request $request){
					JavaScript::put([
						'url_bank'=>route('bank'),
						'url_proses_deposit'=>route('proses-deposit')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.tambah_saldo');
				}
				public function bank(Request $request){
					$banks = Bank::where('aktif','yes')->get();
					return response()->json([
						'code'=>200,
						'bank'=>$banks
					]);
				}
				public function proses_deposit(Request $request){
					$message = [
									'bank_id.required'=>'Bank tdk boleh kosong',
									'nominal.required'=>'Nominal tdk boleh kosong',
									'token.required'=>'Token tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'bank_id'  => 'required',
									'nominal'  => 'required',
									'token'  => 'required|min:10',
					],$message);
					
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Request Saldo :'.$validator->messages());
									$respon = [
													'code'=>401,
													'message'=>'Gagal Request Saldo',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					$total = (int)(rand(1,500)+$request->user()->id + $request->nominal);
					$bank = Bank::find($request->bank_id);
					DB::beginTransaction();
					try {
						if ($cek_pending = Deposit::where('status_deposit_id',1)->where('user_id',$request->user()->id)->where('tgl_trx',date('Y-m-d'))->first()) {
							DB::rollback();
							return response()->json([
											'code'=>401,
											'error_code'=>401,
											'title'=>'Gagal Tambah Saldo',
											'message'=>'Deposit kamu Rp '.number_format($cek_pending->transfer).' statusnya masih MENUNGGU TRANSFER.'
							]);
						}
						$cek_cancel = Deposit::where('status_deposit_id',3)->where('user_id',$request->user()->id)->where('tgl_trx',date('Y-m-d'))->get();
						if (count($cek_cancel) > 3) {
							DB::rollback();
							return response()->json([
											'code'=>401,
											'error_code'=>402,
											'title'=>'Gagal Tambah Saldo',
											'message'=>'Hari ini Kamu sudah 3 kali tidak melakukan transfer, Coba kembali besok.'
							]);
						}
						$add = Deposit::create([
							'user_id'=>$request->user()->id,
							'no_trx'=>date('ymdhis').$request->user()->id,
							'tgl_trx'=>date('Y-m-d'),
							'nominal'=>(int)($request->nominal),
							'transfer'=>(int)($total),
							'bank_id'=>$request->bank_id,
							'status_deposit_id'=>1,
							'admin_id'=>0
						]);
						// $details = [
						// 				'user_id' => $request->user()->id,
						// 				'username' => $request->user()->username,
						// 				'bank'=>$bank->nama,
						// 				'rekening'=>$bank->no_rek,
						// 				'total'=>(int)($total)
						// ];
						// $client = new \GuzzleHttp\Client();
						// $response = $client->request('GET', config("services")["telegram-bot-api"]["all_api"].'Deposit : '.$request->user()->username.', ke bank : '.$bank->nama.', Nominal Rp '.number_format($total));
					} catch (\Throwable $th) {
						Log::info('Gagal proses deposit:'.$th->getMessage());
						DB::rollback();
						return response()->json([
										'code'=>400,
										'message'=>'Gagal proses deposit',
										'data'=>$th
						]);
					}
					DB::commit();
						return response()->json([
							'code'=>200,
							'message'=>'Berhasil',
							'nominal'=>number_format($total),
							'bank'=>Bank::find($request->bank_id)
						]);
				}
				public function mutasi_saldo(Request $request){
					$datas = BukuSaldo::where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.buku_saldo',compact('datas'));
				}
				public function data_topup(Request $request){
					$datas = Deposit::where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.data_topup',compact('datas'));
				}
				public function sudah_transfer(Request $request, $id){
					DB::beginTransaction();
					try {
						$ids_deposit = decrypt($id);
						$cari = Deposit::where('id',$ids_deposit)->where('user_id',$request->user()->id)->where('status_deposit_id',1)->first();
						$cari->status_deposit_id = 4;
						$cari->update();
						$client = new \GuzzleHttp\Client();
						$response = $client->request('GET', config("services")["telegram-bot-api"]["all_api"].'Deposit : '.$cari->userId->username.', ke bank : '.$cari->bankId->nama.', Nominal Rp '.number_format($cari->transfer));
					} catch (\Throwable $th) {
						Log::info('Gagal proses deposit:'.$th->getMessage());
						DB::rollback();
						flash('Silahkan diulangi kembali')->error();
						return redirect()->back();
					}
					DB::commit();
					flash('Mohon menunggu, deposit sedang di cek.')->success();
					return redirect()->back();
				}
				public function batalkan_deposit(Request $request,$id){
					DB::beginTransaction();
					try {
						$ids_deposit = decrypt($id);
						$cari = Deposit::where('id',$ids_deposit)->where('user_id',$request->user()->id)->where('status_deposit_id',1)->first();
						$cari->status_deposit_id = 3;
						$cari->update();
					} catch (\Throwable $th) {
						Log::info('Gagal proses deposit:'.$th->getMessage());
						DB::rollback();
						flash('Deposit gagal dibatalkan.')->error();
						return redirect()->back();
					}
					DB::commit();
					flash('Deposit berhasil dibatalkan.')->success();
					return redirect()->back();
				}
				public function laporan_pulsa(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','REGULER')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_pulsa',compact('datas'));
				}
				public function laporan_voucher_pln(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','PLN')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_voucher_pln',compact('datas'));
				}
				public function laporan_paket_data(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','INTERNET')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_paket_data',compact('datas'));
				}
				public function laporan_paket_sms(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','SMS')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_paket_sms',compact('datas'));
				}
				public function laporan_paket_telpon(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','TELPON')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_paket_telpon',compact('datas'));
				}
				public function laporan_game(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','GAME')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_game',compact('datas'));
				}
				public function laporan_e_money(Request $request){
					$datas = HistoryTransaksi::where('provider_sub','LAIN')->where('user_id',$request->user()->id)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_e_money',compact('datas'));
				}
				public function tambah_untung(Request $request){
					if ($request->user()->agen == 'yes') {
						if ($request->action == 'add') {
							if (User::where('email',$request->email)->first()) {
								flash()->overlay('Email sudah digunakan', 'GAGAL');
								return redirect()->back();
							}
							$mail = explode('@',$request->email);
							if ($mail[1] != 'gmail.com') {
								flash()->overlay('Email wajib menggunakan Gmail', 'GAGAL');
								return redirect()->back();
							}
							DB::beginTransaction();
							$random = Str::random(6);
							$password = Str::random(6);
							try {
								$user = new User;
								$user->username = $mail[0];
								$user->name = $request->name;
								$user->email = $request->email;
								$user->password = Hash::make($password);
								$user->saldo = 0;
								$user->kode = $random;
								$user->email_verified_at = date('Y-m-d H:i:s');
								$user->upline = $request->user()->id;
								$user->type = 'user';
								$user->agen = 'no';
								$user->save();
								$user['password'] = $password;
								$user->notify(new PendaftaranAgen($user));
							} catch (\Throwable $th) {
								Log::info('Gagal proses daftar:'.$th->getMessage());
								DB::rollback();
								flash()->overlay('Silahkan dicoba lagi', 'GAGAL');
								return redirect()->back();
							}
							DB::commit();
							flash()->overlay('Pendaftaran Berhasil, Username dan Password telah dikirim ke email.', 'INFO');
							// return redirect()->back();
						}
						$datas = User::where('upline',$request->user()->id)->where('email_verified_at','<>',null)->get();
						$agent = new Agent();
						return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.tambah_untung',compact('datas'));	
					}else {
						flash('Kamu tidak terdaftar sebagai Agen')->error();
						return redirect()->route('index');
					}
				}
				//TOKO ONLINE
				public function produk($id){
					$decrypted = decrypt($id);
					$categories = Category::find($decrypted);
					$produks = Produk::where('categories_id',$decrypted)->where('aktif','yes')->get();
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.produk',compact('produks','categories'));
				}
				public function detail_produk($id){
					$decrypted = decrypt($id);
					$agent = new Agent();
					$produks = Produk::where('id',$decrypted)->where('aktif','yes')->first();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.detail_produk',compact('produks')); 
				}
				public function cek_ongkir(Request $request){
					$data = [
						'asal'=>$request->asal,
						'tujuan'=>$request->tujuan,
						'berat'=>$request->berat,
						'kurir'=>$request->kurir
					];
					$curl = curl_init();
					curl_setopt_array($curl, array(
					CURLOPT_URL => config('api-config')['url_rajaongkir'].'cost',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => "origin=".$data['asal']."&destination=".$data['tujuan']."&weight=".$data['berat']."&courier=".$data['kurir'],
					CURLOPT_HTTPHEADER => array(
									"content-type: application/x-www-form-urlencoded",
									"key:".config('api-config')['key_rajaongkir']
					),
					));
					$response = curl_exec($curl);
					$err = curl_error($curl);
					curl_close($curl);
					$resarr =json_decode($response,true); 
					return response()->json([
						'code'=>200,
						'message'=>'Data Ongkir',
						'data'=>$resarr
					]);
				}
				public function toko($id){
					$decrypted = decrypt($id);
					$agent = new Agent();
					$tokos = Toko::where('id',$decrypted)->where('aktif','yes')->first();
					$produks = Produk::where('toko_id',$tokos->id)->where('aktif','yes')->get();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.toko',compact('tokos','produks')); 
				}
				public function toko_saya(Request $request){
					$agent = new Agent();
					$tokos = Toko::where('user_id',$request->user()->id)->where('aktif','yes')->get();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.toko_saya',compact('tokos')); 
				}
				public function form_toko(Request $request){
					JavaScript::put([
						'url_propinsi'=>route('propinsi'),
						'url_kota'=>route('kota'),
						'url_proses'=>route('simpan-toko')
					]);
						if ($request->token) {
							DB::beginTransaction();
							try {
								$toko = "toko-".time().$request->user()->id.".jpg";
								$path = public_path().'/images/tokos/' . $toko;
								Image::make(file_get_contents($request->foto_toko))->save($path);
								Toko::create([
									'user_id'=>$request->user()->id,
									'nama'=>$request->nama,
									'nohp'=>$request->nohp,
									'propinsi_id'=>$request->propinsi,
									'kota_id'=>$request->kota,
									'alamat_pengiriman'=>$request->alamat,
									'foto'=>$toko,
									'aktif'=>'yes'
								]);
							} catch (\Throwable $th) {
								Log::info('Gagal proses topup manual:'.$th->getMessage());
								DB::rollback();
								return response()->json([
												'code'=>400,
												'title'=>'Gagal',
												'message'=>'Gagal tambah toko'
								]);
							}
							DB::commit();
							return response()->json([
								'code'=>200,
								'title'=>'Berhasil',
								'message'=>'Berhasil tambah toko',
							]);
						}
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.form_toko'); 
				}
				public function propinsi(){
					$propinsis = Propinsi::get();
					return response()->json($propinsis);
				}
				public function kota(Request $request){
					$kotas = Kota::where('propinsi',$request->propinsi)->get();
					return response()->json($kotas);
				}
				public function detail_saya(Request $request,$id){
					$agent = new Agent();
					$tokos = Toko::where('user_id',$request->user()->id)->where('id',$id)->where('aktif','yes')->first();
					$produks = Produk::where('user_id',$request->user()->id)->where('toko_id',$id)->where('aktif','yes')->get();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.detail_toko',compact('tokos','produks')); 
				}
				public function form_produk(Request $request,$id){
					JavaScript::put([
						'url_kategori'=>route('kategori'),
						'url_proses'=>route('simpan-produk')
					]);
					if (!$tokos = Toko::where('user_id',$request->user()->id)->where('id',$id)->first()) {
						return redirect()->route('index');
					}
					Cookie::queue('toko', $id, 36000);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.tambah_produk'); 
				}
				public function simpan_produk(Request $request){
						DB::beginTransaction();
						try {
							$depan = "depan-".time().$request->user()->id.rand(100,999).".jpg";
							$pathdepan = public_path().'/images/produks/' . $depan;
							Image::make(file_get_contents($request->gambardepan))->save($pathdepan);
							$samping = "samping-".time().$request->user()->id.rand(100,999).".jpg";
							$pathsamping = public_path().'/images/produks/' . $samping;
							Image::make(file_get_contents($request->gambarsamping))->save($pathsamping);
							$belakang = "belakang-".time().$request->user()->id.rand(100,999).".jpg";
							$pathbelakang = public_path().'/images/produks/' . $belakang;
							Image::make(file_get_contents($request->gambarbelakang))->save($pathbelakang);
							$random = Str::random(6);
							if (!$tokos = Toko::where('user_id',$request->user()->id)->where('id',Cookie::get('toko'))->first()) {
								DB::rollback();
								return response()->json([
									'code'=>400,
									'title'=>'Gagal tambah barang',
									'message'=>'Salah Toko'
								]);
							}
							Produk::create([
								'toko_id'=> $tokos->id,
								'categories_id'=>$request->kategori_id,
								'nama_barang'=>$request->nama,
								'kode_barang'=>strtoupper($random),
								'gambar_depan'=>$depan,
								'gambar_samping'=>$samping,
								'gambar_belakang'=>$belakang,
								'user_id'=>$request->user()->id,
								'stok'=>$request->stok,
								'harga'=>$request->harga,
								'berat'=>$request->berat,
								'penjelasan'=>$request->penjelasan,
								'aktif'=>'yes'
							]);
						} catch (\Throwable $th) {
							Log::info('Gagal tambah barang:'.$th->getMessage());
							DB::rollback();
							return response()->json([
								'code'=>400,
								'title'=>'Gagal tambah barang',
								'message'=>'Mohon dicek kembali.'
							]);
						}
						DB::commit();
						return response()->json([
							'code'=>200,
							'title'=>'Berhasil',
							'message'=>'Berhasil tambah barang'
						]);
				}
				public function detail_produk_saya(Request $request, $id){
					$agent = new Agent();
					$produks = Produk::where('user_id',$request->user()->id)->where('id',$id)->where('aktif','yes')->first();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.detail_produk_saya',compact('produks')); 
				}

				public function jadi_agen(Request $request){
					Log::info('User klik jadi_agen : '.$request->user()->email);
					return response()->json([
						'code'=>200,
						'title'=>'Info Pendaftaran Agen',
						'message'=>'Hanya dengan Rp 50.000, kamu akan mendapatkan harga murah dari supplier dan kamu juga dapat menaikan harga untuk dijual ke agen kamu. Klik "Lanjutkan Daftar" jika setuju saldo kamu akan di potong otomatis Rp 50,000.',
						'textBtnYa'=>'Lanjutkan Daftar',
						'textBtnTidak'=>'Nanti',
					]);
				}
				public function proses_jadi_agen(Request $request){
					if ($request->user()->saldo < 50000) {
						return response()->json([
										'code'=>401,
										'title'=>'Saldo Kamu Kurang',
										'message'=>'Apakah ingin tambah saldo sekarang ?',
										'textBtnYa'=>'Ya, Tambah Sekarang',
										'textBtnTidak'=>'Tambah Nanti',
						]);
					}else {
						DB::beginTransaction();
						try {
							$saldo_akhir = (int)($request->user()->saldo - 50000);
							$saldo = User::find($request->user()->id);
							$saldo->saldo = $saldo_akhir;
							$saldo->agen = 'yes';
							$saldo->update();
							$add = BukuSaldo::create([
									'user_id'=>$request->user()->id,
									'no_trx'=>date('ymdhis').$request->user()->id.rand(100,999),
									'mutasi'=>'Debet',
									'nominal'=>50000,
									'saldo_akhir'=>(int)($saldo_akhir),
									'keterangan'=>'Biaya Pendaftaran jadi kepala agen'
							]);
							$upline = User::find($request->user()->upline);
							$saldo_akhir_upline = (int)($upline->saldo + 10000);
							$upline->saldo = $saldo_akhir_upline;
							$upline->update();
							$adds = BukuSaldo::create([
								'user_id'=>$request->user()->upline,
								'no_trx'=>date('ymdhis').$request->user()->upline.rand(100,999),
								'mutasi'=>'Kredit',
								'nominal'=>10000,
								'saldo_akhir'=>(int)($saldo_akhir_upline),
								'keterangan'=>'Biaya 50% dari Pendaftaran agen : '.$saldo->name
						]);
						} catch (\Throwable $th) {
							Log::info('Gagal proses topup:'.$th->getMessage());
								DB::rollback();
								return response()->json([
												'code'=>400,
												'title'=>'Gagal',
												'message'=>'Silahkan dicoba kembali',
												'textBtnYa'=>'OK !!!',
								]);
						}
						DB::commit();
						return response()->json([
										'code'=>200,
										'title'=>'Selamat !',
										'message'=>'Kamu sudah terdaftar sebagai kepala agen',
										'textBtnYa'=>'OK !!!',
										'textBtnTidak'=>'Tambah Nanti',
						]);
						
					}
					
				}
				public function harga_downline(Request $request,$id){
					if ($request->user()->agen =='yes') {
						if ($request->action == 'update') {
							DB::beginTransaction();
							try {
											foreach ($request->code as $key => $data) {
												if ($request->markup[$key] <= 500) {
													DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('code',$data)->first()->update([
																	'markup'=>str_replace(",","",$request->markup[$key])
													]);
												}
											}
							} catch (\Throwable $th) {
											Log::info('Gagal Update untung:'.$th->getMessage());
											DB::rollback();
											flash('Gagal Update untung.')->error();
											return redirect()->back();
							}
							DB::commit();
							flash('Berhasil update untung.')->success();
							return redirect()->back();	
						}
						$datas = DataHargaPpob::get();
						foreach ($datas as $key => $data) {
							DataHargaPpobAgent::updateOrCreate([
								'user_upline'=>$request->user()->id,
								'provider'=>$data->provider,
								'provider_sub'=>$data->provider_sub,
								'operator'=>$data->operator,
								'operator_sub'=>$data->operator_sub,
								'code'=>$data->code, 
							],[
								'description'=>$data->description,
								'price'=>$data->price+$data->markup_agen,
								'status'=>$data->status
							]);
						}
						if ($id == 'pulsa') {
										$product = 'Pulsa';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider_sub','REGULER')->get();
						}elseif ($id == 'pln') {
										$product = 'Token Listrik';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider_sub','PLN')->get();
						}elseif ($id == 'game') {
										$product = 'Voucher Game';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','GAME')->get();
						}elseif ($id == 'paket-internet') {
										$product = 'Paket Internet';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider_sub','INTERNET')->get();
						}elseif ($id == 'paket-sms') {
										$product = 'Paket SMS';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider_sub','SMS')->get();
						}elseif ($id == 'paket-telpon') {
										$product = 'Paket Telpon';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider_sub','TELPON')->get();
						}elseif ($id == 'saldo-OVO') {
										$product = 'Saldo OVO';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','OVO')->orderBy('price','ASC')->get();
						}elseif ($id == 'saldo-Gojek') {
										$product = 'Saldo Gojek';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','GOJEK')->orderBy('price','ASC')->get();
						}elseif ($id == 'saldo-Gojek-Driver') {
										$product = 'Saldo Gojek Driver';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','GOJEK DRIVER')->orderBy('price','ASC')->get();
						}elseif ($id == 'saldo-Grab') {
										$product = 'Saldo Grab';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','GRAB')->orderBy('price','ASC')->get();
						}elseif ($id == 'saldo-Grab-Driver') {
										$product = 'Saldo Grab Driver';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','GRAB DRIVER')->orderBy('price','ASC')->get();
						}elseif ($id == 'saldo-Linkaja') {
										$product = 'Saldo LinkAja';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','LINKAJA')->orderBy('price','ASC')->get();
						}elseif ($id == 'saldo-Shopeepay') {
										$product = 'Saldo Shopeepay';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','SHOPEEPAY')->orderBy('price','ASC')->get();
						}elseif ($id == 'wifi-id') {
										$product = 'Wifi ID';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','SPEEDY')->orderBy('price','ASC')->get();
						}elseif ($id == 'google-play') {
										$product = 'Google Play';
										$hargas = DataHargaPpobAgent::where('user_upline',$request->user()->id)->where('provider','GOOGLE PLAY')->orderBy('price','ASC')->get();
						}
						$ids = $id;
						$agent = new Agent();
						return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.data_harga_ppob',compact('hargas','product','ids')); 	
					}
					else{
						flash()->overlay('Kamu belum terdaftar sebagai Agen.', 'INFO');
						return redirect()->route('index');
					}
				}
}
