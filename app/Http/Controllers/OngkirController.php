<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Propinsi;
use App\Kota;

class OngkirController extends Controller
{
	public function all_propinsi(){
		$data = [
			'asal'=>151,
			'tujuan'=>68,
			'berat'=>1000,
			'kurir'=>'jne'
		];
		$curl = curl_init();
						curl_setopt_array($curl, array(
						CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_POSTFIELDS => "origin=".$data['asal']."&destination=".$data['tujuan']."&weight=".$data['berat']."&courier=".$data['kurir'],
						CURLOPT_HTTPHEADER => array(
										"content-type: application/x-www-form-urlencoded",
										"key:da08d414a043e0001fd33b5b3ad29ad0"
						),
						));
						$response = curl_exec($curl);
						$err = curl_error($curl);
						curl_close($curl);
						$resarr =json_decode($response,true); 
						
						return $resarr;
}
public function kota($propinsi_id){
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=".$propinsi_id,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
	// CURLOPT_POSTFIELDS => "origin=".$data['asal']."&destination=".$data['tujuan']."&weight=".$data['berat']."&courier=".$data['kurir'],
			CURLOPT_HTTPHEADER => array(
							"content-type: application/x-www-form-urlencoded",
							"key:da08d414a043e0001fd33b5b3ad29ad0"
			),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);
			$resarr =json_decode($response,true); 
			$detail =  $resarr['rajaongkir']['results'];
						foreach ($detail as $key => $value) {
							$add = new Kota;
							$add->id = $value['city_id'];
							$add->propinsi = $value['province_id'];
							$add->kota = $value['city_name'];
							$add->kode_pos = $value['postal_code'];
							$add->save();
						}
			return $response;
	}
	public function city(){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.rajaongkir.com/starter/city",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		// CURLOPT_POSTFIELDS => "origin=$asal&destination=$tujuan&weight=$berat&courier=jne",
		CURLOPT_HTTPHEADER => array(
						"content-type: application/x-www-form-urlencoded",
						"key:da08d414a043e0001fd33b5b3ad29ad0"
		),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		$resarr =json_decode($response,true); 
		
		return $response;
}
public function cek_ongkir(Request $request){
	$data = [
		'asal'=>151,
		'tujuan'=>68,
		'berat'=>1000,
		'kurir'=>'jne'
	];
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => "origin=".$data['asal']."&destination=".$data['tujuan']."&weight=".$data['berat']."&courier=".$data['kurir'],
	CURLOPT_HTTPHEADER => array(
					"content-type: application/x-www-form-urlencoded",
					"key:da08d414a043e0001fd33b5b3ad29ad0"
	),
	));
	$response = curl_exec($curl);
	$err = curl_error($curl);
	curl_close($curl);
	$resarr =json_decode($response,true); 
	
	return $response;
}
}
