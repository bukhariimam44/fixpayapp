<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Verified;
use App\Notifications\VerifikasiEmail;
use App\DataHargaPpob;
use App\User;
use App\Post;
use DB;
use Log; 
use JavaScript;
use Cookie;
use Illuminate\Support\Str;
use App\Youtube;
use App\Category;
use App\DataHargaPpobAgent;

class UmumController extends Controller
{
				public function __construct()
				{
								$this->url=config('api-config')['api_url'];
				}
				public function share($kode){
					Cookie::queue(Cookie::forever('kode_fixpay', $kode));
					return redirect()->route('index');
				}
				public function channels(Request $request){
					$agent = new Agent();
					$yout = Youtube::where('aktif','yes')->get();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.channel',compact('yout'));
				}
				public function download(){
					$path = storage_path();
					if (config('app.name') == 'FIXPAY') {
						return response()->file($path.'/app/public/fixpay.apk' ,[
										'Content-Type'=>'application/vnd.android.package-archive',
										'Content-Disposition'=> 'attachment; filename="fixpay.apk"',
						]) ;
					}else {
						return response()->file($path.'/app/public/wip.apk' ,[
										'Content-Type'=>'application/vnd.android.package-archive',
										'Content-Disposition'=> 'attachment; filename="wip.apk"',
						]) ;
					}
					
				}
				public function index(){
					$agent = new Agent();
					if (config('app.name') == 'FIXPAY') {
						return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.welcome_fixpay');
					}elseif (config('app.name') == 'WELIMENA') {
						return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.welimena');
					}else {
						$yout = Youtube::where('aktif','yes')->get();
						return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.welcome_imbuh',compact('yout'));
					}
					
				}
				public function kirim_kode(Request $request){
					if ($request->kode == $request->user()->kode) {
						$user = User::find($request->user()->id)->update([
							'email_verified_at'=>date('Y-m-d H:i:s')
						]);
						return response()->json([
							'code'=>200,
							'title'=>'Verifikasi Berhasil',
							'message'=>'Selamat bertransaksi murah di '.config('app.name')
						]);
					}
					return response()->json([
						'code'=>400,
						'title'=>'Verifikasi Gagal',
						'message'=>'Kode yang dimasukan SALAH.'
					]);
				}
				public function kirim_ulang_kode(Request $request){
						$user = User::find($request->user()->id);
						try {
							$user->notify(new VerifikasiEmail($user));
						} catch (\Throwable $th) {
							return response()->json([
								'code'=>400,
								'title'=>'Pengiriman Gagal',
								'message'=>'Kode gagal di kirim ke Email.'
							]);
						}
						return response()->json([
							'code'=>200,
							'title'=>'Pengiriman Berhasil',
							'message'=>'Kode telah kami kirim ke Email '.$user->email
						]);
				}
				public function ganti_email(Request $request){
					DB::beginTransaction();
					try {
						$user = User::find($request->user()->id);
						$user->email = $request->email;
						$user->notify(new VerifikasiEmail($user));
						$user->update();
					} catch (\Throwable $th) {
						Log::info('Gagal ganti email:'.$th->getMessage());
						DB::rollback();
						return response()->json([
							'code'=>400,
							'title'=>'Gagal ganti email',
							'message'=>'Mohon gunakan email lain.'
						]);
					}
					DB::commit();
					return response()->json([
						'code'=>200,
						'title'=>'Berhasil ganti email',
						'message'=>'Kode telah kami kirim ke Email '.$request->email
					]);
					
				}
    public function harga_pulsa(Request $request,$name){
					if ($name =='pulsa') {
						if ($request->user()->uplineId->agen == 'yes') {
							$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub','REGULER')->get();
						}else {
							$datas = DataHargaPpob::where('provider_sub','REGULER')->get();
						}
					}elseif ($name =='Voucher PLN') {
						if ($request->user()->uplineId->agen == 'yes') {
							$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub','PLN')->get();
						}else {
							$datas = DataHargaPpob::where('provider_sub','PLN')->get();
						}
					}elseif ($name =='Paket Data') {
						if ($request->user()->uplineId->agen == 'yes') {
							$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub','INTERNET')->get();
						}else {
							$datas = DataHargaPpob::where('provider_sub','INTERNET')->get();
						}
					}elseif ($name =='Voucher Game') {
						if ($request->user()->uplineId->agen == 'yes') {
							$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub','GAME')->get();
						}else {
							$datas = DataHargaPpob::where('provider_sub','GAME')->get();
						}
					}else {
						if ($request->user()->uplineId->agen == 'yes') {
							$datas = DataHargaPpobAgent::where('user_upline',$request->user()->upline)->where('provider_sub','LAIN')->get();
						}else {
							$datas = DataHargaPpob::where('provider_sub','LAIN')->get();
						}
					}
					
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.harga',compact('datas','name'));
				}
				public function masuk(Request $request){
					$userdata = array(
						'username'  => $request->username,
						'password'  => $request->password
					);
					$remember = ($request->has('remember')) ? true : false;
					$message = [
									'CaptchaCode.required'=>'Kode tdk boleh kosong',
									'CaptchaCode.valid_captcha'=>'Kode Salah',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
						'CaptchaCode' => 'required|valid_captcha'
					],$message);
					if ($validator->fails())
					{
						flash()->overlay('Kode Captcha Salah', 'Gagal');
							return redirect()
													->back()
													->withInput()
													->withErrors($validator->errors());
					}
					if (Auth::attempt($userdata,$remember)) {
						if ($request->user()->uplineId->agen == 'yes') {
							Cookie::queue(Cookie::forever('kode_fixpay', $request->user()->uplineId->kode));
						}
						return redirect()->route($request->user()->type);
						// return response()->json([
						// 	'code'=>200,
						// 	'title'=>'Login Berhasil',
						// 	'message'=>'Selamat bertransaksi murah di '.config('app.name').'.',
						// ]);
					}
					flash()->overlay('Username ata Password Salah!', 'Gagal');
					return redirect()->back();
					// return response()->json([
					// 	'code'=>401,
					// 	'title'=>'Login Gagal',
					// 	'message'=>'Username atau Password Salah.'
					// ]);
				}
				public function daftar(Request $request){
					if (User::where('username',$request->username)->first()) {
						return response()->json([
							'code'=>401,
							'title'=>'Pendaftaran Gagal',
							'message'=>'Username Sudah digunakan.'
						]);
					}
					if (User::where('email',$request->email)->first()) {
						return response()->json([
							'code'=>401,
							'title'=>'Pendaftaran Gagal',
							'message'=>'Email Sudah digunakan.'
						]);
					}
					$mail = explode('@',$request->email);
					if ($mail[1] != 'gmail.com') {
						return response()->json([
							'code'=>401,
							'title'=>'Pendaftaran Gagal',
							'message'=>'Gunakan Gmail yang valid.'
						]);
					}
					$message = [
									'name.required'=>'name tdk boleh kosong',
									'email.required'=>'email tdk boleh kosong',
									'email.unique'=>'unique sudah digunakan',
									'username.required'=>'username tdk boleh kosong',
									'username.unique'=>'username sudah digunakan',
									'username.min'=>'username minimal 6 karakter',
								];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'name'  => 'required|min:3',
									'username'  => 'required|min:6|unique:users',
									'email'  => 'required|min:7|unique:users|email',
									// 'password' => 'required|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$/',
									'password'  => 'required|min:6',
									'token'  => 'required',
					],$message);
					
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Daftar :'.$validator->messages());
									$respon = [
													'code'=>401,
													'title'=>'Gagal',
													'message'=>'Silahkan di coba lagi',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					$kode = Cookie::get('kode_fixpay');
					if ($upline = User::where('kode',$kode)->first()) {
						$upline = $upline->id;
					}else {
						$upline = 1;
					}
					
					DB::beginTransaction();
					$random = Str::random(6);
					try {
						$user = new User;
						$user->username = $request->username;
						$user->name = $request->name;
						$user->email = $request->email;
						$user->password = Hash::make($request->password);
						$user->saldo = 0;
						$user->kode = $random;
						$user->upline = $upline;
						$user->type = 'user';
						$user->save();
						$user->notify(new VerifikasiEmail($user));
					} catch (\Throwable $th) {
						Log::info('Gagal proses daftar:'.$th->getMessage());
						DB::rollback();
						return response()->json([
							'code'=>401,
							'title'=>'Pendaftaran Gagal',
							'message'=>'Silahkan di Ulangi kembali.'
						]);
					}
					DB::commit();
					$userdata = array(
						'username'  => $request->username,
						'password'  => $request->password
					);
					Auth::attempt($userdata,true);
					if ($request->user()->uplineId->agen == 'yes') {
						Cookie::queue(Cookie::forever('kode_fixpay', $request->user()->uplineId->kode));
					}
					return response()->json([
						'code'=>200,
						'title'=>'Pendaftaran Berhasil',
						'message'=>'Kami telah mengirim Kode Verifikasi di email kamu.'
					]);
				}
				public function kontak(Request $request){
					JavaScript::put([
						'url_kirim'=>route('kontak')
					]);
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.kontak');
				}
				public function ganti_password(Request $request){
					JavaScript::put([
						'url_simpan_password'=>route('edit-password')
					]);
					if ($request->action == 'edit') {
						$user = User::find($request->user()->id);
						if (!Hash::check($request->password_lama,$user->password)) {
							return response()->json([
								'code'=>400,
								'title'=>'Gagal Ganti',
								'message'=>'Password Lama Salah'
							]);
						}
						$user->password = Hash::make($request->password);
						$user->update();
						return response()->json([
							'code'=>200,
							'title'=>'Berhasil Ganti',
							'message'=>'Apakah ingin keluar ?'
						]);
					}
					$agent = new Agent();
					return view(($agent->isMobile() ? 'mobile' : 'desktop') .'.edit_password');

				}
				public function keluar(){
					Auth::logout();
					return redirect()->route('index');
				}
				public function kategori(){
					$datas = Category::get();
					return response()->json($datas);
				}
}
