<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Validator;

use App\Deposit;
use App\BukuSaldo;
use App\User;
use App\Bank;
use App\DataHargaPpob;
use App\HistoryTransaksi;
use App\DataHargaPpobAgent;

use DB;
use Log;
use View;
use JavaScript;
use Session;
use App\Helpers\Req;

class AdminController extends Controller
{
				public function __construct()
				{
								$this->middleware('admin');
								$this->middleware(['auth','verified']);
								$this->url=config('api-config')['api_url'];
        $this->hsl = '';
				}
				public function admin(){
					flash()->overlay('Login Berhasil', 'Berhasil');
					return redirect()->route('index');
				}
				public function data_harga(Request $request,$id){
								if ($request->action == 'update') {
												// if ($request->user()->email != 'b') {
												// 				flash('Belum ada ijin Update.')->error();
												// 				return redirect()->back();
												// }
												DB::beginTransaction();
												try {
																foreach ($request->code as $key => $data) {
																				DataHargaPpob::where('code',$data)->first()->update([
																								'markup'=>str_replace(",","",$request->markup[$key]),
																								'markup_agen'=>str_replace(",","",$request->markup_agen[$key])
																				]);
																}
																$update = DataHargaPpobAgent::get();
																foreach ($update as $key => $value) {
																	$agen = DataHargaPpob::where('code',$value->code)->first();
																	$dd = DataHargaPpobAgent::where('code',$value->code)->first();
																	$dd->description = $agen->description;
																	$dd->price = $agen->price+$agen->markup_agen;
																	$dd->status = $agen->status;
																	$dd->update();
																}
												} catch (\Throwable $th) {
																Log::info('Gagal Update Jual:'.$th->getMessage());
																DB::rollback();
																flash('Gagal Update Markup.')->error();
																$this->hsl = 'gagal';
												}
												if ($this->hsl == '') {
																DB::commit();
																flash('Berhasil Update Markup')->success();
												}
												
								}
								$params = [];
								$response = Req::get($params, $this->url.'data-harga')->get();
								
								foreach ($response['data'] as $key => $value) {
											DataHargaPpob::updateOrCreate([
												'provider'=>$value['provider'],
												'provider_sub'=>$value['provider_sub'],
												'operator'=>$value['operator'],
												'operator_sub'=>$value['operator_sub'],
												'code'=>$value['code'], 
											],[
												'description'=>$value['description'],
												'price'=>$value['price'],
												'status'=>$value['status']
											]);
												// DataHargaPpob::where('code',$value['code'])->where('operator_sub',$value['operator_sub'])->first()->update([
												// 				'price'=>$value['price'],
												// 				'status'=>$value['status']
												// ]);
								}
								if ($id == 'pulsa') {
												$product = 'Pulsa';
												$datas = DataHargaPpob::where('provider_sub','REGULER')->get();
								}elseif ($id == 'pln') {
												$product = 'Token Listrik';
												$datas = DataHargaPpob::where('provider_sub','PLN')->get();
								}elseif ($id == 'game') {
												$product = 'Voucher Game';
												$datas = DataHargaPpob::where('provider','GAME')->get();
								}elseif ($id == 'paket-internet') {
												$product = 'Paket Internet';
												$datas = DataHargaPpob::where('provider_sub','INTERNET')->get();
								}elseif ($id == 'paket-sms') {
												$product = 'Paket SMS';
												$datas = DataHargaPpob::where('provider_sub','SMS')->get();
								}elseif ($id == 'paket-telpon') {
												$product = 'Paket Telpon';
												$datas = DataHargaPpob::where('provider_sub','TELPON')->get();
								}elseif ($id == 'saldo-OVO') {
												$product = 'Saldo OVO';
												$datas = DataHargaPpob::where('provider','OVO')->orderBy('price','ASC')->get();
								}elseif ($id == 'saldo-Gojek') {
												$product = 'Saldo Gojek';
												$datas = DataHargaPpob::where('provider','GOJEK')->orderBy('price','ASC')->get();
								}elseif ($id == 'saldo-Gojek-Driver') {
												$product = 'Saldo Gojek Driver';
												$datas = DataHargaPpob::where('provider','GOJEK DRIVER')->orderBy('price','ASC')->get();
								}elseif ($id == 'saldo-Grab') {
												$product = 'Saldo Grab';
												$datas = DataHargaPpob::where('provider','GRAB')->orderBy('price','ASC')->get();
								}elseif ($id == 'saldo-Grab-Driver') {
												$product = 'Saldo Grab Driver';
												$datas = DataHargaPpob::where('provider','GRAB DRIVER')->orderBy('price','ASC')->get();
								}elseif ($id == 'saldo-Linkaja') {
												$product = 'Saldo LinkAja';
												$datas = DataHargaPpob::where('provider','LINKAJA')->orderBy('price','ASC')->get();
								}elseif ($id == 'saldo-Shopeepay') {
												$product = 'Saldo Shopeepay';
												$datas = DataHargaPpob::where('provider','SHOPEEPAY')->orderBy('price','ASC')->get();
								}elseif ($id == 'wifi-id') {
												$product = 'Wifi ID';
												$datas = DataHargaPpob::where('provider','SPEEDY')->orderBy('price','ASC')->get();
								}elseif ($id == 'google-play') {
												$product = 'Google Play';
												$datas = DataHargaPpob::where('provider','GOOGLE PLAY')->orderBy('price','ASC')->get();
								}
								$ids = $id;
								$agent = new Agent();
								return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.data_harga_ppob',compact('datas','ids','product'));	
				}
    public function permintaan_topup(Request $request){
					$datas = Deposit::orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.req_topup',compact('datas') );
				}
				public function proses_topup(Request $request){
					DB::beginTransaction();
					try {
						$proses = Deposit::where('id',$request->ids)->where('status_deposit_id',4)->first();
						$proses->status_deposit_id = 2;
						$proses->update();
						$user = User::find($proses->user_id);
						$no_trx = date('ymdhis').$proses->user_id;
						$saldo_akhir = (int)($user->saldo + $proses->transfer);
						$user->saldo = (int)($saldo_akhir);
						BukuSaldo::create([
										'user_id'=>$proses->user_id,
										'no_trx'=>$no_trx,
										'mutasi'=>'Kredit',
										'nominal'=>(int)($proses->transfer),
										'saldo_akhir'=>(int)($saldo_akhir),
										'keterangan'=>'Topup Saldo '.$proses->bankId->nama.' ('.$proses->bankId->no_rek.')'
						]);
							$user->update();
					} catch (\Throwable $th) {
								Log::info('Gagal proses topup:'.$th->getMessage());
								DB::rollback();
								flash('Gagal proses topup')->error();
								return redirect()->back();
					}
					DB::commit();
					flash('Berhasil proses topup')->success();
					return redirect()->back();
				}
				public function cancel_topup(Request $request){
						$proses = Deposit::where('id',$request->ids)->where('status_deposit_id',4)->first();
						$proses->status_deposit_id = 3;
						$proses->update();
					return redirect()->back();
				}
				public function mutasi_saldo(Request $request){
					if ($request->action == 'cari') {
						$user = User::where('email',$request->email)->first();
						$id_user = $user->id;
					}else {
						$id_user = '';
					}
					
					$datas = BukuSaldo::where('user_id',$id_user)->orderBy('created_at','DESC')->get();
					$agent = new Agent();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.mutasi_saldo',compact('datas') );
				}
				public function topup_manual(Request $request){
					JavaScript::put([
						'url_bank'=>route('admin-bank'),
						'url_topup_manual'=>route('admin-topup-manual')
					]);
					$agent = new Agent();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.topup_manual');
				}
				public function bank(Request $request){
					$banks = Bank::where('aktif','yes')->get();
					return response()->json([
						'code'=>200,
						'bank'=>$banks
					]);
				}
				public function proses_topup_manual(Request $request){
					$message = [
						'nominal.required'=>'Nominal tdk boleh kosong',
						'email.required'=>'Nama Product tdk boleh kosong',
					];
					$response = array('response' => '', 'success'=>false);
					$validator = Validator::make($request->all(), [
									'nominal'  => 'required',
									'email'  => 'required|email',
									'token'  => 'required',
					],$message);
					if ($validator->fails()) {
									$response['response'] = $validator->messages();
									Log::info('Gagal Proses Pulsa :'.$validator->messages());
									$respon = [
													'code'=>401,
													'title'=>'Topup Gagal',
													'message'=>'Proses Topup Manual Gagal',
													'status'=>'gagal',
													'error'=>$response
									];
									return response()->json($respon);
					}
					$no_trx = date('ymdhis').$request->user()->id;
					DB::beginTransaction();
					try {
						$banks = Bank::find($request->bank_id);
						$user = User::where('email',$request->email)->first();
						$saldo_akhir = (int)($user->saldo + $request->nominal);
						$user->saldo = (int)($saldo_akhir);
						$add = Deposit::create([
							'user_id'=>$user->id,
							'no_trx'=>date('ymdhis').$user->id,
							'tgl_trx'=>date('Y-m-d'),
							'nominal'=>(int)($request->nominal),
							'transfer'=>(int)($request->nominal),
							'bank_id'=>$banks->id,
							'status_deposit_id'=>2,
							'admin_id'=>$request->user()->id
						]);
						BukuSaldo::create([
							'user_id'=>$user->id,
							'no_trx'=>$no_trx,
							'mutasi'=>'Kredit',
							'nominal'=>(int)($request->nominal),
							'saldo_akhir'=>(int)($saldo_akhir),
							'keterangan'=>'Topup Manual '.$banks->nama.' ('.$banks->no_rek.')'
						]);
						$user->update();
					} catch (\Throwable $th) {
						Log::info('Gagal proses topup manual:'.$th->getMessage());
						DB::rollback();
						return response()->json([
										'code'=>400,
										'message'=>'Gagal proses deposit',
										'data'=>$th
						]);
					}
					DB::commit();
					return response()->json([
						'code'=>200,
						'title'=>'Berhasil',
						'message'=>'Topup Manual Berhasil',
						'url'=>url('admin/mutasi-saldo?email='.$request->email.'&action=cari')
					]);
				}
				public function laporan_ppob(Request $request){
					$agent = new Agent();
					$datas = HistoryTransaksi::orderBy('created_at','DESC')->get();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.laporan_ppob',compact('datas'));
				}
				//SUPPLIER
				public function mutasi_saldo_supplier(Request $request){
					$params = [];
					$response = Req::post($params, $this->url.'buku-saldo')->get();
					$datas = $response['data'];
					$agent = new Agent();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.mutasi_saldo_supplier',compact('datas'));

				}
				public function data_topup_supplier(Request $request){
					$params = [];
					$response = Req::post($params, $this->url.'buku-saldo')->get();
					$datas = $response['data'];
					$agent = new Agent();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.mutasi_saldo_supplier',compact('datas'));

				}
				public function topup_supplier(Request $request){
					if ($request->token) {
						$params['bank'] = $request->bank_id;
						$params['nominal'] = $request->nominal;
						$respon = Req::post($params, $this->url.'proses-topup')->get();
						return response()->json($respon);
					}
					$agent = new Agent();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.topup_supplier');
				}
				public function bank_supplier(){
					$params = [];
					$banks = Req::get($params, $this->url.'data-bank')->get();
					return response()->json($banks);
				}
				public function data_user(Request $request){
					$agent = new Agent();
					if ($request->action == 'update') {
						DB::beginTransaction();
						try {
							$email = decrypt($request->ids);
							$user = User::where('email',$email)->first();
							$user->agen = 'yes';
							$user->update();
							$datas = DataHargaPpob::get();
							foreach ($datas as $key => $data) {
								DataHargaPpobAgent::updateOrCreate([
									'user_upline'=>$user->id,
									'provider'=>$data->provider,
									'provider_sub'=>$data->provider_sub,
									'operator'=>$data->operator,
									'operator_sub'=>$data->operator_sub,
									'code'=>$data->code, 
								],[
									'description'=>$data->description,
									'price'=>$data->price+$data->markup_agen,
									'status'=>$data->status
								]);
							}
						} catch (\Throwable $th) {
							Log::info('Gagal daftar agen:'.$th->getMessage());
							DB::rollback();
							flash()->overlay('Gagal daftar Agen.', 'GAGAL');
							return redirect()->back();
						}
						DB::commit();
						flash()->overlay('Berhasil daftar Agen.', 'INFO');
					}
					$datas = User::where('type','user')->get();
					return view('admin.'.($agent->isMobile() ? 'mobile' : 'desktop') .'.data_user',compact('datas'));
				}
}
