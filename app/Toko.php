<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Toko extends Model
{
	protected $fillable = [
			'user_id','nama','nohp','propinsi_id','kota_id','alamat_pengiriman','foto','aktif'
	];
		public function propinsiId(){
			return $this->belongsTo('App\Propinsi','propinsi_id');
		}
		public function kotaId(){
			return $this->belongsTo('App\Kota','kota_id');
		}
		public function produkId(){
			return $this->hasMany('App\Produk','toko_id');
		}
		
}
