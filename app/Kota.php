<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
	protected $fillable = [
		'id','propinsi','kota','kode_pos'
];
	public function propinsiId(){
		return $this->belongsTo('App\Propinsi','propinsi');
	}
}
