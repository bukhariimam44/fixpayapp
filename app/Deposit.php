<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
	protected $fillable = [
		'user_id','no_trx','tgl_trx','nominal','transfer','bank_id','status','status_deposit_id','admin_id','created_at','updated_at'
	];
	public function userId(){
		return $this->belongsTo('App\User','user_id');
	}
	public function bankId(){
		return $this->belongsTo('App\Bank','bank_id');
	}
	public function adminId(){
		return $this->belongsTo('App\Admin','admin_id');
	}
	public function statusDepositId(){
		return $this->belongsTo('App\StatusDeposit','status_deposit_id');
	}
}
