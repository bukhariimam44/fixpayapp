<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
			protected $fillable = [
				'id','telegram_user_id','chat_id'
		];
	use Notifiable;
}
