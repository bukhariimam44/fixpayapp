<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusDeposit extends Model
{
	protected $fillable = [
		'id','status','created_at','updated_at'
	];
}
