<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataHargaPpob extends Model
{
	protected $table = 'data_harga_ppobs';
	protected $fillable = [
		'id','provider','provider_sub','operator','operator_sub','code', 'description','price','jual','markup','markup_agen','status'
];
}
