<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PendaftaranAgen extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
					$greeting = sprintf('Halo %s!', $notifiable->name);
								return (new MailMessage)
																				->greeting($greeting)
																				->subject('Data User Kamu')
																				->line('Data Akun '.config('app.name').' kamu :')
																				->line('____________________')
																				->line('Nama : '.$notifiable->name)
																				->line('Username : '.$notifiable->username)
																				->line('Email : '.$notifiable->email)
																				->line('Kata Sandi : '.$notifiable->password)
																				->line('____________________')
                    ->line('Terima kasih telah menggunakan aplikasi '.config('app.name').'.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
